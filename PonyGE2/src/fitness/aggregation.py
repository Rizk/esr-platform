from algorithm.parameters import params
from fitness.base_ff_classes.base_ff import base_ff
from os import path, getcwd, pardir, chdir
import subprocess


class aggregation(base_ff):
    """Calculates fitness according to the function defined in the ARGoS Loop Functions"""

    def __init__(self):
        # Initialise base fitness function class.
        super().__init__()

    def escape_characters(self, escape_string):
        escape_string = escape_string.replace(";","\;")
        return escape_string


    def evaluate(self, ind, **kwargs):
        originalDir = getcwd()
        chdir('../../argos-code/')
        process = subprocess.Popen(['build/embedding/ge_aggregation/ge_aggregation ' + self.escape_characters(ind.phenotype)], stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
        output = process.communicate()
        output_string = str(output[0])
        start_index = output_string.find("PERFORMANCE")
        end_index = output_string.find("PERFORMANCE", start_index+1)
        chdir(originalDir)
        fitness = float(output_string[start_index+len("PERFORMANCE")+1 : end_index-1])
        return fitness
