from algorithm.parameters import params
from fitness.base_ff_classes.base_ff import base_ff
from os import path, getcwd, pardir, chdir
import subprocess
import time


class task_specialisation(base_ff):
    """Calculates fitness according to the function defined in the ARGoS Loop Functions"""

    maximise = True
    dots = None

    def __init__(self):
        # Initialise base fitness function class.
        super().__init__()

    def escape_characters(self, escape_string):
        escape_string = escape_string.replace(";","\;")
        return escape_string

    def evaluate(self, ind, **kwargs):
        originalDir = getcwd()
        chdir('../../argos-code/')
        #//Arguments: visualisation_flag logging_flag slope_angle num_trials rules
        #print(ind.name)
        #print(ind.phenotype)
        #print("Problem is " + str(ind.name) + "?")
        t1 = time.time()
        process = subprocess.Popen(['build/embedding/ge_task_specialisation/ge_task_specialisation 0 0 ' + str(params['SLOPE']) + " 3 " + self.escape_characters(ind.phenotype)], stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
        output = process.communicate()
        #t2 = time.time()
        #print(t2-t1)
        output_string = str(output[0])
        error_string = str(output[1])
        start_index = output_string.find("PERFORMANCE")
        end_index = output_string.find("PERFORMANCE", start_index+1)
        chdir(originalDir)
        fitness = float(output_string[start_index+len("PERFORMANCE")+1 : end_index-1])
        start_index_dots = output_string.find("DOTS")
        end_index_dots = output_string.find("DOTS", start_index_dots+1)
        chdir(originalDir)
        dots = float(output_string[start_index_dots+len("DOTS")+1 : end_index_dots-1])
        return fitness,dots
