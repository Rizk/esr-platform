import os
from shutil import copyfile

list_of_seeds = []
list_of_rules = []

if not os.path.exists(os.getcwd()+"/ALL_RULES/"):
	os.mkdir(os.getcwd()+"/ALL_RULES/")

for directory in os.listdir(os.getcwd()):
	if(os.path.exists(os.getcwd()+"/"+directory+"/foraging_best_200.dat")):
		if not os.path.exists(os.getcwd()+"/ALL_RULES/"+directory.split("_")[-1]+"/"):
			os.mkdir(os.getcwd()+"/ALL_RULES/"+directory.split("_")[-1]+"/")
		copyfile(os.getcwd()+"/"+directory+"/foraging_best_200.dat", os.getcwd()+"/ALL_RULES/"+directory.split("_")[-1]+"/"+"foraging_best_200.dat")
		