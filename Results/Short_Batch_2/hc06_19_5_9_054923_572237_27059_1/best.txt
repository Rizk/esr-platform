Generation:
1

Phenotype:
*:*:IS_DROP_OBJECT,true,59;B_RANDOM_WALK,57#P_MOTIVATION_1_LT,false;*:*:IS_WANT_OBJECT,true,69#

Genotype:
[29538, 11119, 78935, 38292, 35175, 19637, 36921, 72637, 12762, 56763, 47014, 40273, 59137, 41954, 34533, 56419, 91154, 17246, 44429, 38399, 2313, 55098, 84495, 13923, 50293, 28155, 53706, 33096, 16663, 32841, 19781, 57122, 10236, 55537, 20171, 45997, 25975, 10849, 59821, 47416, 14522, 77653, 79426, 43413, 86541, 13215, 26675, 78948, 41636]
Tree:
(<rule_set> (<rule> (<precondition_set> (<epsilon> *)) : (<behaviour_set> (<epsilon> *)) : (<action_set> (<action> (<A_IS> (<IS_name> IS_DROP_OBJECT) , (<boolean> true) , (<probability> 59))) ; (<action_set> (<action> (<A_B> (<behaviour> B_RANDOM_WALK) , (<probability> 57)))))) # (<rule_set> (<rule> (<precondition_set> (<precondition> (<precondition_terminal> P_MOTIVATION_1_LT) , (<boolean> false)) ; (<precondition_set> (<epsilon> *))) : (<behaviour_set> (<epsilon> *)) : (<action_set> (<action> (<A_IS> (<IS_name> IS_WANT_OBJECT) , (<boolean> true) , (<probability> 69))))) #))

Fitness:
5.33333