Generation:
40

Phenotype:
P_MOTIVATION_2_GT,false;P_MOTIVATION_2_GT,false;*:*:B_PHOTOTAXIS,1#

Genotype:
[33435, 99741, 85407, 12303, 51095, 91206, 94574, 41608, 3336, 97163, 55328, 32705, 72467, 30263, 99739, 2817, 94724, 58275, 94569, 93217, 65640, 55326, 66547, 87858, 24883, 39763, 37245, 77015]
Tree:
(<rule_set> (<rule> (<precondition_set> (<precondition> (<precondition_terminal> P_MOTIVATION_2_GT) , (<boolean> false)) ; (<precondition_set> (<precondition> (<precondition_terminal> P_MOTIVATION_2_GT) , (<boolean> false)) ; (<precondition_set> (<epsilon> *)))) : (<behaviour_set> (<epsilon> *)) : (<action_set> (<action> (<A_B> (<behaviour> B_PHOTOTAXIS) , (<probability> 1))))) #)

Fitness:
0.0