import os

list_of_seeds = []
list_of_rules = []

for filename in os.listdir(os.getcwd()):
	f = open(filename)
	content = f.read().split('\n')
	if(len(content) == 41):#41 rules because 200 generations and logging 0th and every 5th
		list_of_seeds += [filename.split('_')[3].strip('.txt')]
		list_of_rules += [content[-2]]

#print(list_of_seeds)
#print(list_of_rules)
f = open("list_of_seeds.txt","w+")
g = open("list_of_rules.txt", "w+")
for i in range(len(list_of_rules)):
	f.write(list_of_seeds[i])
	f.write('\n')
	g.write(list_of_rules[i])
	g.write('\n')
f.close()
g.close()
