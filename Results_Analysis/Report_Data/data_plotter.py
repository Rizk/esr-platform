import subprocess
import os
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import numpy as np

'''
TS controllers are controllers that exhibit task specialisation, evolved in a sloped environment
Non-TS controllers are controllers that do not exhibit task specialisation, evolved in a flat environment with a grammar that doesn't support specialisation

The code here compares the performance of both of these in an environment with an 8° slope
'''

'''
Plot fitness at each generation for TS controllers

def plot_TS_evo():
	#directories = ["/hs07_18_9_28_190619_321167_29226_1/","/hs08_18_10_1_224323_421998_15881_2/"]
	stat_name = "best_fitness"
	fig = plt.figure()
	ax1 = fig.add_subplot(1, 1, 1)

	#for i in range(len(directories)):
	for directory in os.listdir(os.getcwd()+ "/TS_GE_batch1"):
		if directory != "200_gen":
			filename = os.getcwd()+"/TS_GE_batch1/"+directory+"/stats.tsv"
			data = pd.read_csv(filename, sep="\t")
			stat = list(data[stat_name])
			ax1.plot(stat)

	plt.title("Evolution of TS Controllers")
	plt.xlabel("Generation")
	plt.ylabel("Fitness")
	#plt.show()
	plt.savefig("TS_evolution.png")
'''	

'''
Plot fitness at each generation for Non-TS controllers

def plot_Non_TS_evo():
	stat_name = "best_fitness"
	fig = plt.figure()
	ax1 = fig.add_subplot(1, 1, 1)

	for directory in os.listdir(os.getcwd()+ "/Foraging_GE_batch1/foraging"):
		filename = os.getcwd()+"/Foraging_GE_batch1/foraging/"+directory+"/stats.tsv"
		data = pd.read_csv(filename, sep="\t")
		stat = list(data[stat_name])
		ax1.plot(stat)

	plt.title("Evolution of Non-TS Controllers")
	plt.xlabel("Generation")
	plt.ylabel("Fitness")
	#plt.show()
	plt.savefig("Non-TS_evolution.png")
'''
'''
Plot mean fitness (and variance) of all TS controllers
def plot_TS_fitness():
	filename = os.getcwd()+"/TS_slope_analysis.csv"
	data = pd.read_csv(filename, sep=",")
	N = len(data.index)
	means = data.mean(axis=1)
	std = data.std(axis=1)
	ind = np.arange(N)
	width = 0.35
	p1 = plt.bar(ind, means, width, yerr=std)
	plt.title("Performance of TS Controllers on 8° slope")
	plt.xlabel("Evolved Controller")
	plt.ylabel("Fitness")
	#plt.show()
	plt.savefig("TS_performance.png")
'''
'''
Plot mean fitness (and variance) of all Non-TS controllers
def plot_Non_TS_fitness():
	filename = os.getcwd()+"/Non-TS_slope_analysis.csv"
	data = pd.read_csv(filename, sep=",")
	N = len(data.index)
	means = data.mean(axis=1)
	std = data.std(axis=1)
	ind = np.arange(N)
	width = 0.35
	p1 = plt.bar(ind, means, width, yerr=std)
	plt.title("Performance of Non-TS Controllers on 8° slope")
	plt.xlabel("Evolved Controller")
	plt.ylabel("Fitness")
	#plt.show()
	plt.savefig("Non-TS_performance.png")
'''

'''
Plot fitness at each generation for TS and Non-TS controllers
'''
def plot_evolution():
	#directories = ["/hs07_18_9_28_190619_321167_29226_1/","/hs08_18_10_1_224323_421998_15881_2/"]
	stat_name = "best_fitness"
	fig = plt.figure()
	ax1 = fig.add_subplot(1, 2, 1)
	plt.xlabel("Generation")
	plt.ylabel("Fitness")
	plt.title("TS Controllers")
	#ax1.set_xlim([0,2000])
	ax1.set_ylim([0,15])

	for directory in os.listdir(os.getcwd()+ "/TS_GE_batch1"):
		if directory != "200_gen":
			filename = os.getcwd()+"/TS_GE_batch1/"+directory+"/stats.tsv"
			data = pd.read_csv(filename, sep="\t")
			stat = list(data[stat_name])
			ax1.plot(stat)

	ax2 = fig.add_subplot(1, 2, 2)
	plt.xlabel("Generation")
	plt.ylabel("Fitness")
	plt.title("Non-TS Controllers")
	ax2.set_xlim([0,2000])
	ax2.set_ylim([0,15])

	for directory in os.listdir(os.getcwd()+ "/Foraging_GE_batch1/foraging"):
		filename = os.getcwd()+"/Foraging_GE_batch1/foraging/"+directory+"/stats.tsv"
		data = pd.read_csv(filename, sep="\t")
		stat = list(data[stat_name])
		ax2.plot(stat)

	plt.suptitle("Evolution of TS vs Non-TS Controllers")
	#plt.show()
	plt.savefig("evolution.png")

'''
Plot mean fitness (and variance) of both TS and Non-TS controllers
'''
def plot_fitness():
	filename1 = os.getcwd()+"/TS_slope_analysis.csv"
	filename2 = os.getcwd()+"/Non-TS_slope_analysis.csv"
	data1 = pd.read_csv(filename1, sep=",")
	data1 = data1.drop(['Run'], axis=1)
	data2 = pd.read_csv(filename2, sep=",")
	data2 = data2.drop(['Run'], axis=1)
	N1 = len(data1.index)
	N2 = len(data2.index)
	#means1 = data1.mean(axis=1)
	#means2 = data2.mean(axis=1)
	#std1 = data1.std(axis=1)
	#std2 = data2.std(axis=1)
	#ind1 = np.arange(N1)
	#ind2 = np.arange(N2)
	#width = 0.35
	ax1 = plt.subplot(1,2,1)
	#p1 = plt.bar(ind1, means1, width, yerr=std1)
	p1 = plt.boxplot(data1)
	plt.xlabel("Evolved Controller")
	plt.ylabel("Fitness")
	plt.title("TS Controllers")
	ax2 =plt.subplot(1,2,2)
	#p2 = plt.bar(ind2, means2, width, yerr=std2)
	p2 = plt.boxplot(data2)
	plt.xlabel("Evolved Controller")
	plt.ylabel("Fitness")
	plt.title("Non-TS Controllers")
	ax1.set_xlim([0,max(N1,N2)+1])
	ax2.set_xlim([0,max(N1,N2)+1])
	ax1.set_ylim([-50,50])
	ax2.set_ylim([-50,50])
	plt.suptitle("Performance of TS vs Non-TS Controllers on 8° slope")
	plt.savefig("fitness.png")

'''
Plot change in trajectory of all 4 robots and the number of cylinders in 
the cache over the course of the experiment
TODO: Update to do mean of all runs and compare TS vs Non-TS
'''
def plot_trajectory():
	filename1 = os.getcwd()+"/trajectory_logs.csv"
	#filename2 = os.getcwd()+"/TS_cache_analysis.csv"
	filename2 = os.getcwd()+"/cache_logs.csv"
	data1 = pd.read_csv(filename1, sep=',')
	data2 = pd.read_csv(filename2, sep=',')
	#data2 = data2.drop(['Run'], axis=1)
	#data2 = data2.mean(axis=0)
	#print(data2)
	#data1 = data1.drop([0], axis=0)
	#data2 = data2.drop(['Time'], axis=1)
	#print(data.shape)
	#print(data.iloc[0])
	ax1 = plt.subplot(2,1,1)
	#data1.plot()
	plt.plot(data1.iloc[0])
	plt.plot(data1.iloc[1])
	plt.plot(data1.iloc[2])
	plt.plot(data1.iloc[3])
	plt.xlabel("Time")
	plt.ylabel("Robot Trajectory")
	ax2 = plt.subplot(2,1,2)
	#print(data2.iloc[0][0:101])
	#print(data2.iloc[-1])
	#plt.plot(data2)
	plt.plot(data2.iloc[0])
	#print(data2.iloc[0])
	plt.xlabel("Time")
	plt.ylabel("Cache Size")
	plt.suptitle("Change in Robot Trajectory and Cache Size")
	plt.savefig("trajectory.png")

'''
Plot mean&variance of degree of task specialisation of last generation of each 
evolved controller
TODO: Update to have boxplots (do the same for fitness one) and show fitness as
colour.
'''
def plot_dots():
	filename1 = os.getcwd()+"/TS_dots_analysis.csv"
	filename2 = os.getcwd()+"/Non-TS_dots_analysis.csv"
	data1 = pd.read_csv(filename1, sep=",")
	data1 = data1.drop(['Run'], axis=1)
	data2 = pd.read_csv(filename2, sep=",")
	data2 = data2.drop(['Run'], axis=1)
	#labels1 = data1# TODO: Create x labels matching the run IDs.
	N1 = len(data1.index)
	N2 = len(data2.index)
	fitness_file_TS = os.getcwd()+"/TS_slope_analysis.csv"
	fitness_file_non_TS = os.getcwd()+"/Non-TS_slope_analysis.csv"
	TS_fitness_data = pd.read_csv(fitness_file_TS, sep=",")
	non_TS_fitness_data = pd.read_csv(fitness_file_non_TS, sep=",")
	#means1 = data1.mean(axis=1)
	#means2 = data2.mean(axis=1)
	#std1 = data1.std(axis=1)
	#std2 = data2.std(axis=1)
	#ind1 = np.arange(N1)
	#ind2 = np.arange(N2)
	#width = 0.35
	''' '''
	ax1 = plt.subplot(1,2,1)
	#p1 = plt.bar(ind1, means1, width)#, yerr=std1)#plt.boxplot(means1)#
	cmap = matplotlib.cm.get_cmap('coolwarm')
	#cmap2 = 
	normalize = matplotlib.colors.Normalize(vmin=min( min(TS_fitness_data.mean(axis=1)), min(non_TS_fitness_data.mean(axis=1)) ), vmax=max( max(TS_fitness_data.mean(axis=1)), max(non_TS_fitness_data.mean(axis=1)) ))
	colors1 = [cmap(normalize(value)) for value in TS_fitness_data.mean(axis=1)]
	p1 = plt.boxplot(data1, patch_artist=True)
	for patch, color in zip(p1['boxes'], colors1):
		patch.set_facecolor(color)
	plt.xlabel("Evolved Controller")
	plt.ylabel("Degree of Task Specialisation")
	plt.title("TS Controllers")
	ax2 =plt.subplot(1,2,2)
	#p2 = plt.bar(ind2, means2, width)#, yerr=std2)#plt.boxplot(means2)#
	colors2 = [cmap(normalize(value)) for value in non_TS_fitness_data.mean(axis=1)]
	p2 = plt.boxplot(data2, patch_artist=True)
	for patch, color in zip(p2['boxes'], colors2):
		patch.set_facecolor(color)
	plt.xlabel("Evolved Controller")
	plt.ylabel("Degree of Task Specialisation")
	plt.title("Non-TS Controllers")
	ax1.set_xlim([0,max(N1,N2)+1])
	ax2.set_xlim([0,max(N1,N2)+1])
	#ax1.set_ylim([-50,50])
	#ax2.set_ylim([-50,50])
	plt.suptitle("Degree of Task Specialisation of TS vs Non-TS Controllers on 8° slope")
	cax, _ = matplotlib.colorbar.make_axes(ax2)
	cbar = matplotlib.colorbar.ColorbarBase(cax, cmap=cmap, norm=normalize)
	plt.savefig("dots.png")
	#plt.show()
	
	#print(means1)

#plot_fitness()
#plot_evolution()
plot_trajectory()
#plot_dots()

