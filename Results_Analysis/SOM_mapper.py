from mvpa2.suite import *
import os
'''
behaviour_characterisations = np.array(
     [[0., 0., 0.],
      [0., 0., 1.],
      [0., 0., 0.5],
      [0.125, 0.529, 1.0],
      [0.33, 0.4, 0.67],
      [0.6, 0.5, 1.0],
      [0., 1., 0.],
      [1., 0., 0.],
      [0., 1., 1.],
      [1., 0., 1.],
      [1., 1., 0.],
      [1., 1., 1.],
      [.33, .33, .33],
      [.5, .5, .5],
      [.66, .66, .66]])
'''

# Visualization names
controller_names_NN = []
controller_names_GE = []

#behaviour_characterisations = np.array([])

f = open("BC_LOGS_GE.csv")
f_read = f.read()
g = open("list_of_seeds_GE.txt")
g_read = g.read()
seeds_ge = [int(x) for x in g_read.strip().split('\n')]
bc_ge = []

for i in range(len(f_read.strip().split('\n'))):
  bc_ge += [f_read.strip().split('\n')[i].strip().split(',')]
for i in range(len(bc_ge)):
  del bc_ge[i][-1]
bc_ge = [[float(x) for x in y] for y in bc_ge]

f.close()
g.close()

f = open("BC_LOGS_NN.csv")
f_read = f.read()
g = open("list_of_seeds_NN.txt")
g_read = g.read()
seeds_nn = [int(x) for x in g_read.strip().split('\n')]
bc_nn = []

for i in range(len(f_read.strip().split('\n'))):
  bc_nn += [f_read.strip().split('\n')[i].strip().split(',')]
for i in range(len(bc_nn)):
  del bc_nn[i][-1]
bc_nn = [[float(x) for x in y] for y in bc_nn]

f.close()
g.close()

#bc_nn = bc_nn[:5]
#bc_ge = bc_ge[:5]
#seeds_nn = seeds_nn[:5]
#seeds_ge = seeds_ge[:5]

controller_names_GE = ["GE_"+str(x) for x in seeds_ge]
controller_names_NN = ["NN_"+str(x) for x in seeds_nn]

behaviour_characterisations = np.array(bc_nn + bc_ge)
print(behaviour_characterisations)




#print(behaviour_characterisations)
#arr_to_append = np.zeros((behaviour_characterisations.shape[0],1))
#for i in range(10):
#  behaviour_characterisations = np.append(behaviour_characterisations,arr_to_append, axis=1)
#print(arr_to_append)
#print(behaviour_characterisations)
  

#for i in range(len(behaviour_characterisations)):
#  controller_names_NN += ['NN_'+str(i)]
#  controller_names_GE += ['GE_'+str(i)]

#print(behaviour_characterisations)

som = SimpleSOMMapper((25, 25), 400, learning_rate=0.05)

som.train(behaviour_characterisations)
#pl.imshow(som.K, origin='lower')
mapped_NN = som(np.array(bc_nn))
mapped_GE = som(np.array(bc_ge))
#print(som.K)
print(mapped_NN)
print(mapped_GE)

X = np.linspace(0, 25, len(mapped_NN), endpoint=True)
Y = np.linspace(0, 25, len(mapped_NN), endpoint=True)
pl.title('Behaviour SOM')

pl.subplot(121)
pl.plot(X,Y, c='white')

# SOM's kshape is (rows x columns), while matplotlib wants (X x Y)
for i, m in enumerate(mapped_NN):
    pl.text(m[1], m[0], controller_names_NN[i], ha='center', va='center',
           bbox=dict(facecolor='gray', alpha=0.5, lw=0))

pl.subplot(122)
pl.plot(X,Y, c='white')

# SOM's kshape is (rows x columns), while matplotlib wants (X x Y)
for i, m in enumerate(mapped_GE):
    pl.text(m[1], m[0], controller_names_GE[i], ha='center', va='center',
           bbox=dict(facecolor='gray', alpha=0.5, lw=0))

# show the figure
pl.show()