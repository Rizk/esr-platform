import subprocess
import os
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from scipy import spatial

slope_list = [0,5,10,15,20]
slope_length = len(slope_list)
experiment_length = 1000
step_size = 10

'''
Plot fitness at each generation for controllers evolved in each slope environment
'''
def plot_evolution():
	stat_name = "best_fitness"
	fig = plt.figure(figsize=(12,10))
	
	for i in range(len(slope_list)):
		ax = fig.add_subplot(1,slope_length,i+1)#, sharex=True)
		plt.xlabel("Generation")
		if i == 0:
			plt.ylabel("Fitness")
		plt.title("Slope =" + str(slope_list[i]) + "°")
		ax.set_ylim([0,30])

		#Normal version
		parent_directory = os.getcwd()+ "/Data/pony_logs/" + str(slope_list[i])
		
		#Population 100 version
		#parent_directory = os.getcwd()+ "/Data/population_100/pony_logs/" + str(slope_list[i])
		
		for directory in os.listdir(parent_directory):
			filename = parent_directory + "/" + directory + "/stats.tsv"
			data = pd.read_csv(filename, sep="\t")
			stat = list(data[stat_name])
			ax.plot(stat)
	
	plt.xlabel("Generation")
	plt.suptitle("Evolution of Controllers w/ Different Slope Angles")
	
	#Normal version
	plt.savefig("evolution.png")

	#Population 100 version
	#os.chdir(os.getcwd()+'/Data/population_100/')
	#plt.savefig("evolution_100.png")

'''
Plot mean fitness (and variance) of controllers evolved in each slope environment
TODO: Update file reading to include random seed
'''
def plot_fitness():
	fig = plt.figure(figsize=(14,14))
	plt.subplots_adjust(hspace=0.3,wspace=0.3)
	parent_directory = os.getcwd()+"/Data/post_evolution_logs/"
	performance_data = [[[] for i in range(slope_length)] for j in range(slope_length)]
	
	for file in os.listdir(parent_directory):
		slope_train_index = slope_list.index(int(file.split('_')[0]))#get og slope
		slope_test_index = slope_list.index(int(file.split('_')[1]))#get destination slope
		random_seed = int(file.split('_')[2])
		log_type = file.split('_')[3]#get cache or trajectory or performance
		filename = os.path.join(parent_directory,file)

		if log_type == "performance":
			data = pd.read_csv(filename, sep="\n")
			data = data.transpose()
			N = len(performance_data[slope_train_index][slope_test_index])
			
			if random_seed-1 >= N:
				performance_data[slope_train_index][slope_test_index] += [pd.DataFrame()]*(random_seed-N)

			performance_data[slope_train_index][slope_test_index][random_seed-1] = data
		
	for i in range(slope_length):
		for j in range(slope_length):
			ax = fig.add_subplot(slope_length,slope_length,i*slope_length+j+1)
			if performance_data[i][j] != []:
				performance_data[i][j] = pd.concat(performance_data[i][j])
				p = plt.boxplot(performance_data[i][j])
			if i%slope_length==(slope_length-1):
				plt.xlabel("Evolved Controller")
			plt.title(str(slope_list[i])+"° Ctrlr, "+"Slope=" + str(slope_list[j]) + "°")
			if j%slope_length==0:
				plt.ylabel("Fitness")
			#ax.set_xlim([0,5])#ax.set_xlim([0,N+1])
			ax.set_ylim([0,50])
			plt.suptitle("Performance of Evolved Controllers with Different Slopes")
	
	plt.savefig("fitness.png")

	'''for i in range(slope_length):
		for j in range(slope_length):
			filename = os.getcwd()+"/Data/post_evolution_logs/"+str(slope_list[i])+"_"+str(slope_list[j])+"_performance_logs.csv"
			data = []
			N=0
			if(os.path.isfile(filename)):
				data = pd.read_csv(filename, sep="\n")
				data = data.transpose()
				N = len(data.index)
			ax = fig.add_subplot(slope_length,slope_length,i*slope_length+j+slope_length)
			p = plt.boxplot(data)
			if i%slope_length==(slope_length-1):
				plt.xlabel("Evolved Controller")
			plt.title(str(slope_list[i])+"° Ctrlr, "+"Slope=" + str(slope_list[j]) + "°")
			if j%slope_length==0:
				plt.ylabel("Fitness")
			ax.set_xlim([0,N+1])
			ax.set_ylim([-50,50])
			plt.suptitle("Performance of Evolved Controllers with Different Slopes")
	
	plt.savefig("fitness.png")'''

'''
Plot change in the number of cylinders in the cache over the course of the experiment
'''
def plot_cache():
	fig_cache = plt.figure(figsize=(14,14))
	plt.subplots_adjust(hspace=0.5,wspace=0.5)
	parent_directory = os.getcwd()+"/Data/post_evolution_logs/"
	cache_data = [[pd.DataFrame() for i in range(slope_length)] for j in range(slope_length)]
	cache_tally = [[0 for i in range(slope_length)] for j in range(slope_length)]#slope_length*[ slope_length*[0] ] 
	cols = [i*step_size for i in range(experiment_length+1)]
	
	for file in os.listdir(parent_directory):
		slope_train_index = slope_list.index(int(file.split('_')[0]))#get og slope
		slope_test_index = slope_list.index(int(file.split('_')[1]))#get destination slope
		log_type = file.split('_')[3]#get cache or trajectory or performance
		filename = os.path.join(parent_directory,file)
		
		if log_type == "cache":
			data = pd.read_csv(filename)
			data.columns = cols
			
			if cache_data[slope_train_index][slope_test_index].empty:
				cache_data[slope_train_index][slope_test_index] = data
				cache_tally[slope_train_index][slope_test_index] += 1
			else:
				cache_data[slope_train_index][slope_test_index] = cache_data[slope_train_index][slope_test_index] + data #add to cache list
				cache_tally[slope_train_index][slope_test_index] += 1 #increment cache tally

	for i in range(slope_length):
		cache_incrementer = 1
		for j in range(slope_length):
			if cache_tally[i][j] != 0:
				cache_data[i][j] /= cache_tally[i][j]
			#Plot cache
			ax = fig_cache.add_subplot(slope_length,slope_length,i*slope_length+j+1)
			if not cache_data[i][j].empty:
				plt.plot(cache_data[i][j].mean(axis=0))
				#plt.plot(cache_data[i][j].iloc[0])
			#Title and label
			plt.title(str(slope_list[i])+"° Ctrlr, "+"Slope=" + str(slope_list[j]) + "°")
			plt.xlabel("Time")
			plt.ylabel("Cache size")
			#ax.set_xlim([0,experiment_length])
			ax.set_ylim([0,75])
			cache_incrementer += 1

	plt.suptitle("Change in Cache Size")
	plt.savefig("cache.png")

'''
Plot change in robot trajectory over the course of the experiment
'''
def plot_trajectory():
	fig_traj = plt.figure(figsize=(14,14))
	plt.subplots_adjust(hspace=0.5,wspace=0.5)
	parent_directory = os.getcwd()+"/Data/post_evolution_logs/"
	trajectory_data = [[pd.DataFrame() for i in range(slope_length)] for j in range(slope_length)]
	trajectory_tally = [[0 for i in range(slope_length)] for j in range(slope_length)]
	cols = [i*step_size for i in range(experiment_length+1)]
	
	for file in os.listdir(parent_directory):
		slope_train_index = slope_list.index(int(file.split('_')[0]))#get og slope
		slope_test_index = slope_list.index(int(file.split('_')[1]))#get destination slope
		log_type = file.split('_')[3]#get cache or trajectory or performance
		filename = os.path.join(parent_directory,file)
		
		if log_type == "trajectory":
			data = pd.read_csv(filename)
			#data = data.transpose()
			data.columns = cols

			if trajectory_data[slope_train_index][slope_test_index].empty:
				trajectory_data[slope_train_index][slope_test_index] = data
				trajectory_tally[slope_train_index][slope_test_index] += 1
			else:
				trajectory_data[slope_train_index][slope_test_index] = trajectory_data[slope_train_index][slope_test_index] + data #add to trajectory list
				trajectory_tally[slope_train_index][slope_test_index] += 1 #increment trajectory tally

	for i in range(slope_length):
		traj_incrementer = 1
		for j in range(slope_length):
			if trajectory_tally[i][j] != 0:
				trajectory_data[i][j] /= trajectory_tally[i][j]
			#Plot trajectory
			ax = fig_traj.add_subplot(slope_length,slope_length,i*slope_length+j+1)
			if not trajectory_data[i][j].empty:
				#print(trajectory_data[i][j].shape)
				#plt.plot(trajectory_data[i][j])
				k=0
				plt.plot(trajectory_data[i][j].iloc[(4*k)+0])#mean(axis=0))
				plt.plot(trajectory_data[i][j].iloc[(4*k)+1])
				plt.plot(trajectory_data[i][j].iloc[(4*k)+2])
				plt.plot(trajectory_data[i][j].iloc[(4*k)+3])
				
				distance = spatial.distance.cdist(trajectory_data[i][j].iloc[ (4*k)+0 : (4*k)+4 , : ], 
				trajectory_data[i][j].iloc[ (4*k)+0 : (4*k)+4 , : ], metric='canberra')
				print(str(slope_list[i])+"° Ctrlr, "+"Slope=" + str(slope_list[j]) + "°")
				print(pd.DataFrame(distance))
			#Title and label
			plt.title(str(slope_list[i])+"° Ctrlr, "+"Slope=" + str(slope_list[j]) + "°")
			plt.xlabel("Time")
			plt.ylabel("Robot Trajectory")
			#ax.set_xlim([0,experiment_length])
			ax.set_ylim([0,10])
			traj_incrementer += 1

	plt.suptitle("Change in Robot Trajectory")
	plt.savefig("trajectory.png")

'''
Plot mean&variance of degree of task specialisation of every 5th generation for controllers 
evolved in each slope environment
'''
def plot_dots():
	fig = plt.figure(figsize=(12,10))
	parent_directory = os.getcwd()+ "/Data/argos_logs/"
	data = len(slope_list)*[[]]
	N = len(slope_list)*[0]
	
	for i in range(len(slope_list)):
		for file in os.listdir(parent_directory):
			slope = file.split('_')[4]

			if file.split('_')[6] == "DoTS.txt" and slope == str(slope_list[i]):
				filename = os.path.join(parent_directory,file)
				temp_data = pd.read_csv(filename)
				#temp_data = temp_data.transpose()
				if data[i] == []:
					data[i] = temp_data
				else:
					data[i].append(temp_data)

	for i in range(len(slope_list)):
		ax = fig.add_subplot(1,len(slope_list),i+1)
		#data[i] = data[i].transpose()
		#fitness_file = os.getcwd() + "/Data/pony_logs/" + str(slope)
		#fitness_data = pd.read_csv(fitness_file)
		plt.xlabel("Generation")
		if i==0:
			plt.ylabel("Degree of Task Specialisation")
		plt.title("Slope =" + str(slope_list[i]) + "°")
		ax.set_ylim([0,1.05])
		#cmap = matplotlib.cm.get_cmap('coolwarm')
		p = plt.plot(data[i],'o-')
	
	plt.suptitle("Degree of Task Specialisation During Evolution")
	plt.savefig("dots.png")

	'''
	filename1 = os.getcwd()+"/TS_dots_analysis.csv"
	filename2 = os.getcwd()+"/Non-TS_dots_analysis.csv"
	data1 = pd.read_csv(filename1, sep=",")
	data1 = data1.drop(['Run'], axis=1)
	data2 = pd.read_csv(filename2, sep=",")
	data2 = data2.drop(['Run'], axis=1)
	#labels1 = data1# TODO: Create x labels matching the run IDs.
	N1 = len(data1.index)
	N2 = len(data2.index)
	fitness_file_TS = os.getcwd()+"/TS_slope_analysis.csv"
	fitness_file_non_TS = os.getcwd()+"/Non-TS_slope_analysis.csv"
	TS_fitness_data = pd.read_csv(fitness_file_TS, sep=",")
	non_TS_fitness_data = pd.read_csv(fitness_file_non_TS, sep=",")
	#means1 = data1.mean(axis=1)
	#means2 = data2.mean(axis=1)
	#std1 = data1.std(axis=1)
	#std2 = data2.std(axis=1)
	#ind1 = np.arange(N1)
	#ind2 = np.arange(N2)
	#width = 0.35
	ax1 = plt.subplot(1,2,1)
	#p1 = plt.bar(ind1, means1, width)#, yerr=std1)#plt.boxplot(means1)#
	cmap = matplotlib.cm.get_cmap('coolwarm')
	#cmap2 = 
	normalize = matplotlib.colors.Normalize(vmin=min( min(TS_fitness_data.mean(axis=1)), min(non_TS_fitness_data.mean(axis=1)) ), vmax=max( max(TS_fitness_data.mean(axis=1)), max(non_TS_fitness_data.mean(axis=1)) ))
	colors1 = [cmap(normalize(value)) for value in TS_fitness_data.mean(axis=1)]
	p1 = plt.boxplot(data1, patch_artist=True)
	for patch, color in zip(p1['boxes'], colors1):
		patch.set_facecolor(color)
	plt.xlabel("Evolved Controller")
	plt.ylabel("Degree of Task Specialisation")
	plt.title("TS Controllers")
	ax2 =plt.subplot(1,2,2)
	#p2 = plt.bar(ind2, means2, width)#, yerr=std2)#plt.boxplot(means2)#
	colors2 = [cmap(normalize(value)) for value in non_TS_fitness_data.mean(axis=1)]
	p2 = plt.boxplot(data2, patch_artist=True)
	for patch, color in zip(p2['boxes'], colors2):
		patch.set_facecolor(color)
	plt.xlabel("Evolved Controller")
	plt.ylabel("Degree of Task Specialisation")
	plt.title("Non-TS Controllers")
	ax1.set_xlim([0,max(N1,N2)+1])
	ax2.set_xlim([0,max(N1,N2)+1])
	#ax1.set_ylim([-50,50])
	#ax2.set_ylim([-50,50])
	plt.suptitle("Degree of Task Specialisation of TS vs Non-TS Controllers on 8° slope")
	cax, _ = matplotlib.colorbar.make_axes(ax2)
	cbar = matplotlib.colorbar.ColorbarBase(cax, cmap=cmap, norm=normalize)
	plt.savefig("dots.png")
	#plt.show()
	
	#print(means1)
	'''

plot_evolution()
plot_fitness()
#plot_dots()
plot_cache()
plot_trajectory()

