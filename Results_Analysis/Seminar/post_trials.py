import os
import subprocess

def do_trials(slope_list, num_trials):
	#Create directory paths
	original_directory = os.getcwd()
	argos_directory = os.path.join(original_directory, '../../argos-code/')
	os.chdir(argos_directory)
	data_directory = original_directory+"/Data/pony_logs/"

	#For each slope environment
	for slope_directory in os.listdir(data_directory):
		#For each evolutionary run done in that slope environment
		for run_directory in os.listdir(os.path.join(data_directory,slope_directory)):
			#Path to the evolved controller that is going to be evaluated
			controller_path = os.path.join(os.path.join(data_directory,slope_directory) , run_directory)

			seed = run_directory.split('_')[3]

			#Get the final rules from that run
			f = open(controller_path+"/best.txt")
			best_file = f.read().split("\n")
			rules = best_file[4]

			#For every slope environment
			for slope_test in slope_list:
				#build/embedding/ge_task_specialisation/ge_task_specialisation 0 1 5 1 "*:*:IS_WANT_OBJECT,false,41;IS_DROP_OBJECT,false,97;B_PHOTOTAXIS,62;B_RANDOM_WALK,90;IS_MOTIVATION_1_DEC,false,21#*:*:IS_MOTIVATION_2_INC,false,51#P_ON_NEST,false;P_HAS_OBJECT,true;*:*:B_ANTI_PHOTOTAXIS,94#"

				#Argos command with arguments: visualisation_flag logging_flag slope_angle num_trials rules
				argos_command = "build/embedding/ge_task_specialisation/ge_task_specialisation 0 1 " + str(slope_test) + " " + str(num_trials) + " \"" + rules + "\""
				
				#Evaluate controller
				process = subprocess.Popen([argos_command], stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
				output = process.communicate()
				print("Moving results of "+slope_directory+"_"+str(slope_test))
				os.rename(argos_directory+"performance_logs.csv",original_directory+"/Data/post_evolution_logs/"+slope_directory+"_"+str(slope_test)+"_"+seed+"_performance_logs.csv")
				os.rename(argos_directory+"trajectory_logs.csv",original_directory+"/Data/post_evolution_logs/"+slope_directory+"_"+str(slope_test)+"_"+seed+"_trajectory_logs.csv")
				os.rename(argos_directory+"cache_logs.csv",original_directory+"/Data/post_evolution_logs/"+slope_directory+"_"+str(slope_test)+"_"+seed+"_cache_logs.csv")

	os.chdir(original_directory)
	print(os.getcwd())

do_trials([0,5,10,15,20], 30)