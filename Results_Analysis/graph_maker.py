import subprocess
import os
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

#Create graph from TS evolution data
def plot_TS_GE_evo():
	#directories = ["/hs07_18_9_28_190619_321167_29226_1/","/hs08_18_10_1_224323_421998_15881_2/"]
	stat_name = "best_fitness"
	fig = plt.figure()
	ax1 = fig.add_subplot(1, 1, 1)

	#for i in range(len(directories)):
	for directory in os.listdir(os.getcwd()+ "/TS_GE_batch1"):
		if directory != "200_gen":
			filename = os.getcwd()+"/TS_GE_batch1/"+directory+"/stats.tsv"
			data = pd.read_csv(filename, sep="\t")
			stat = list(data[stat_name])
			ax1.plot(stat)

	plt.title("Best Fitness- Task Specialisation with Grammars")
	plt.xlabel("Generation")
	plt.ylabel("Fitness")
	plt.show()
	

#Create graph from GE evolution data
def plot_foraging_GE_evo():
	stat_name = "best_fitness"
	fig = plt.figure()
	ax1 = fig.add_subplot(1, 1, 1)

	for directory in os.listdir(os.getcwd()+ "/Foraging_GE_batch1/foraging"):
		filename = os.getcwd()+"/Foraging_GE_batch1/foraging/"+directory+"/stats.tsv"
		data = pd.read_csv(filename, sep="\t")
		stat = list(data[stat_name])
		ax1.plot(stat)

	plt.title("Best Fitness- Foraging with Grammars")
	plt.xlabel("Generation")
	plt.ylabel("Fitness")
	plt.show()

#Create graph from NN evolution data
def plot_foraging_NN_evo():
	stat_name = "best_fitness"
	fig = plt.figure()
	ax1 = fig.add_subplot(1, 1, 1)
	
	for directory in os.listdir(os.getcwd()+ "/Foraging_NN_batch2/results"):
		print(directory)
		if directory != "ALL_RULES" and os.path.isdir(os.getcwd()+"/Foraging_NN_batch2/results/"+directory):
			filename = os.getcwd()+"/Foraging_NN_batch2/results/"+directory+"/foraging_stats.tsv"
			data = pd.read_csv(filename, sep="\t")
			#print(data)
			stat = list(data[stat_name])
			ax1.plot(stat)
	
	plt.title("Best Fitness- Foraging with NNs")
	plt.xlabel("Generation")
	plt.ylabel("Fitness")
	plt.show()	

#Create graph from TS post analysis
def plot_TS_post():
	filename = os.getcwd()+"/TS-GE_post_analysis.csv"
	data = pd.read_csv(filename, sep=",")
	N = len(data.index)
	means = data.mean(axis=1)
	std = data.std(axis=1)
	ind = np.arange(N)
	width = 0.35
	p1 = plt.bar(ind, means, width, yerr=std)
	plt.title("Performance of Evolved TS Controller with Grammars")
	plt.xlabel("Evolved Controller")
	plt.ylabel("Fitness")
	plt.show()

#Create graph from GE post analysis
def plot_foraging_GE_post():
	filename = os.getcwd()+"/GE_post_analysis.csv"
	data = pd.read_csv(filename, sep=",")
	N = len(data.index)
	means = data.mean(axis=1)
	std = data.std(axis=1)
	ind = np.arange(N)
	width = 0.35
	p1 = plt.bar(ind, means, width, yerr=std)
	plt.title("Performance of Evolved Foraging Controller with Grammars")
	plt.xlabel("Evolved Controller")
	plt.ylabel("Fitness")
	plt.show()

#Create graph from NN post analysis
def plot_foraging_NN_post():
	filename = os.getcwd()+"/NN_post_analysis.csv"
	data = pd.read_csv(filename, sep=",")
	N = len(data.index)
	means = data.mean(axis=1)
	std = data.std(axis=1)
	ind = np.arange(N)
	width = 0.35
	p1 = plt.bar(ind, means, width, yerr=std)
	plt.title("Performance of Evolved Foraging Controller with NNs")
	plt.xlabel("Evolved Controller")
	plt.ylabel("Fitness")
	plt.show()

#plot_TS_GE_evo()
#plot_foraging_GE_evo()
#plot_foraging_NN_evo()
plot_TS_post()
plot_foraging_GE_post()
plot_foraging_NN_post()
