from os import path, getcwd, chdir
import subprocess

originalDir = getcwd()
chdir(originalDir + '/PonyGE2/src')
for i in range(3):
	process = subprocess.Popen(['time python3 ponyge.py --parameters aggregation.txt --random_seed ' + str(i)], shell=True)
process.communicate()
