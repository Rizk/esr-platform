import subprocess

f = open("list_of_seeds.txt")
list_of_seeds = f.read().split('\n')
f = open("list_of_rules.txt")
list_of_rules = f.read().split('\n')

for i in range(len(list_of_seeds)):
	if list_of_rules[i] != '':
		f = open("foraging_rules.txt","w")
		f.write(list_of_rules[i])
		process = subprocess.Popen(['argos3 -c experiments/ge_foraging.argos'], stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
		output = process.communicate()
