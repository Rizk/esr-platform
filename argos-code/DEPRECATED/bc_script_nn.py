import os
from shutil import copyfile
import subprocess

list_of_seeds = []
list_of_rules = []
f = open("list_of_seeds_NN.txt","w+")

for directory in os.listdir(os.getcwd()+"/ALL_RULES_NN/"):
	copyfile(os.getcwd()+"/ALL_RULES_NN/"+directory+"/foraging_best_200.dat", os.getcwd()+"/foraging_best_200.dat")
	process = subprocess.Popen(['argos3 -c experiments/mpga_foraging-trial.argos'], stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
	output = process.communicate()
	f.write(directory)
	f.write("\n")

f.close()
		