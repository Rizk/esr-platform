import subprocess
import xml.etree.ElementTree

f = open("list_of_seeds.txt")
list_of_seeds = f.read().split('\n')
f = open("list_of_rules.txt")
list_of_rules = f.read().split('\n')
results_file = open("Non-TS_dots_analysis.csv","w")
num_seeds = 50
results_file.write("Run")
for i in range(num_seeds):
	results_file.write(",Seed "+str(i))
results_file.write("\n")

for i in range(len(list_of_seeds)):
	if list_of_rules[i] != '':
		f = open("task_specialisation_rules.txt","w")
		f.write(list_of_rules[i])
		#print("Controller ",list_of_seeds[i])
		results_file.write(list_of_seeds[i])
		for j in range(num_seeds):
			#Change seed in xml file
			argosTree = xml.etree.ElementTree.parse('experiments/ge_task_specialisation.argos')
			root = argosTree.getroot()
			experimentElement = root.find('framework').find('experiment')
			experimentElement.set('random_seed',str(j))
			with open('experiments/ge_task_specialisation.argos','w') as h:
				h.write(xml.etree.ElementTree.tostring(root, encoding="unicode"))
			
			process = subprocess.Popen(['argos3 -c experiments/ge_task_specialisation.argos'], stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
			output = process.communicate()
			
			#This segment logs fitness
			'''
			output_string = str(output[0])
			start_index = output_string.find("PERFORMANCE")
			end_index = output_string.find("PERFORMANCE", start_index+1)
			fitness = float(output_string[start_index+len("PERFORMANCE")+1 : end_index-1])
			results_file.write(","+str(fitness))#Uncomment to add fitne
			'''

			#This segment logs degree of TS
			dots_file = open("degree_of_TS_logs.csv")
			dots = dots_file.read().split('\n')
			results_file.write(","+str(dots[0]))

		results_file.write("\n")
