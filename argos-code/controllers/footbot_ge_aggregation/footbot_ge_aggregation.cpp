/* Include the controller definition */
#include "footbot_ge_aggregation.h"
/* Function definitions for XML parsing */
#include <argos3/core/utility/configuration/argos_configuration.h>
/* 2D vector definition */
#include <argos3/core/utility/math/vector2.h>
/* Logging */
#include <argos3/core/utility/logging/argos_log.h>

/* File reading */
#include <fstream>
#include <unistd.h>

/****************************************/
/****************************************/

/****************************************/
/****************************************/

void CFootBotGEAggregationController::SWheelTurningParams::Init(TConfigurationNode& t_node) {
   try {
      TurningMechanism = NO_TURN;
      CDegrees cAngle;
      GetNodeAttribute(t_node, "hard_turn_angle_threshold", cAngle);
      HardTurnOnAngleThreshold = ToRadians(cAngle);
      GetNodeAttribute(t_node, "soft_turn_angle_threshold", cAngle);
      SoftTurnOnAngleThreshold = ToRadians(cAngle);
      GetNodeAttribute(t_node, "no_turn_angle_threshold", cAngle);
      NoTurnAngleThreshold = ToRadians(cAngle);
      GetNodeAttribute(t_node, "max_speed", MaxSpeed);
   }
   catch(CARGoSException& ex) {
      THROW_ARGOSEXCEPTION_NESTED("Error initializing controller wheel turning parameters.", ex);
   }
}

/****************************************/
/****************************************/

void CFootBotGEAggregationController::SFlockingInteractionParams::Init(TConfigurationNode& t_node) {
   try {
      GetNodeAttribute(t_node, "target_distance", TargetDistance);
      GetNodeAttribute(t_node, "gain", Gain);
      GetNodeAttribute(t_node, "exponent", Exponent);
   }
   catch(CARGoSException& ex) {
      THROW_ARGOSEXCEPTION_NESTED("Error initializing controller flocking parameters.", ex);
   }
}

/****************************************/
/****************************************/

/*
 * This function is a generalization of the Lennard-Jones potential
 */
Real CFootBotGEAggregationController::SFlockingInteractionParams::GeneralizedLennardJones(Real f_distance) {
   Real fNormDistExp = ::pow(TargetDistance / f_distance, Exponent);
   return -Gain / f_distance * (fNormDistExp * fNormDistExp - fNormDistExp);
}

/****************************************/
/****************************************/

CFootBotGEAggregationController::CFootBotGEAggregationController() :
   m_pcWheelsAct(NULL),
   m_pcRABAct(NULL),
   m_pcProximity(NULL),
   m_pcRABSens(NULL),
   m_pcWheelsSens(NULL),
   m_pcRNG(NULL),
   m_Rules(0) {}

/****************************************/
/****************************************/

void CFootBotGEAggregationController::Init(TConfigurationNode& t_node) {
   try {
      /*
       * Initialize sensors/actuators
       */
      m_pcWheelsAct    = GetActuator<CCI_DifferentialSteeringActuator>("differential_steering");
      m_pcRABAct    = GetActuator<CCI_RangeAndBearingActuator     >("range_and_bearing"    );
      m_pcProximity = GetSensor  <CCI_FootBotProximitySensor      >("footbot_proximity"    );
      m_pcRABSens   = GetSensor  <CCI_RangeAndBearingSensor       >("range_and_bearing"    );
      m_pcWheelsSens = GetSensor <CCI_DifferentialSteeringSensor	>("differential_steering");
      /*
       * Parse XML parameters
       */
      /* Wheel turning */
      m_sWheelTurningParams.Init(GetNode(t_node, "wheel_turning"));
      /* Flocking-related */
      m_sFlockingParams.Init(GetNode(t_node, "flocking"));
   }
   catch(CARGoSException& ex) {
      THROW_ARGOSEXCEPTION_NESTED("Error initializing the foot-bot aggregation controller for robot \"" << GetId() << "\"", ex);
   }
   /*
    * Initialize other stuff
    */
   /* Create a random number generator. We use the 'argos' category so
      that creation, reset, seeding and cleanup are managed by ARGoS. */
   m_pcRNG = CRandom::CreateRNG("argos");
   //m_Rules = SetRules("P_NEAR_NEIGHBOURS,false:*:B_RANDOM_WALK,100\nP_NEAR_NEIGHBOURS,true:*:B_FLOCK,100");//ReadRules();
   
   RULES_ASSIGNED = false;
   
   P_NEAR_NEIGHBOURS = false;
   
   B_RANDOM_WALK = false;
   B_FLOCK = false;
   
   m_RandomWalkTime = 0;
   m_CurrentRandomWalk = 0;
   
   //Reset();
   
   
}

void CFootBotGEAggregationController::SetRulesFromInput(std::string rulesString) {
	std::cout<<"Controller: Setting rules"<<std::endl;
	m_Rules = SetRules(rulesString);
	RULES_ASSIGNED = true;
}

/****************************************/
/****************************************/

void CFootBotGEAggregationController::ControlStep() {
	/*
	LOG<<GetId()<<": "<<std::endl;
	LOG<<"P_NEAR_NEIGHBOURS = "<<P_NEAR_NEIGHBOURS<<std::endl;
	LOG<<"B_RANDOM_WALK = "<<B_RANDOM_WALK<<std::endl;
	LOG<<"B_FLOCK = "<<B_FLOCK<<std::endl;
	*/
	
	if(!RULES_ASSIGNED) {
		//std::cout<<"About to read from file"<<std::endl;
		m_Rules = ReadRules();//LOGERR<<"Rules have not been set for this controller"<<std::endl;
	}
	
	for(int i=0; i<m_Rules.size(); i++) {
		if(CheckPreconditions(m_Rules[i].preconditions) &&
			CheckBehaviours(m_Rules[i].behaviours))
			ExecuteActions(m_Rules[i].actions);
	}
	
	if(B_RANDOM_WALK == false)
		m_CurrentRandomWalk = 0;
	
	UpdateState();
}

/****************************************/
/****************************************/

void CFootBotGEAggregationController::Reset() {

	RULES_ASSIGNED = false;
	
	P_NEAR_NEIGHBOURS = false;
	
	B_RANDOM_WALK = false;
	B_FLOCK = false;
	
	m_RandomWalkTime = 0;
	m_CurrentRandomWalk = 0;
}

/****************************************/
/****************************************/

CVector2 CFootBotGEAggregationController::ObstacleAvoidanceVector() {
   
   /* Add vector to detected obstacles */
   const CCI_FootBotProximitySensor::TReadings& tProxReads = m_pcProximity->GetReadings();
   CVector2 cObstacleVector = CVector2();
   for(size_t i = 0; i < tProxReads.size(); ++i) {
	  cObstacleVector += CVector2(tProxReads[i].Value, tProxReads[i].Angle);
   }
   
   if(cObstacleVector.Length() > 0.0f)
		cObstacleVector = CVector2(m_sWheelTurningParams.MaxSpeed, cObstacleVector.Angle());
   
   return cObstacleVector;
	
}

CVector2 CFootBotGEAggregationController::VectorToNeighbours() {
	const CCI_RangeAndBearingSensor::TReadings& tRABReads = m_pcRABSens->GetReadings();
	if(!tRABReads.empty()) {
		CVector2 cNeighbourVector = CVector2();
		
		for(size_t i = 0; i < tRABReads.size(); ++i) {
			cNeighbourVector += CVector2(tRABReads[i].Range, tRABReads[i].HorizontalBearing);
		}
		
		if(cNeighbourVector.Length() > m_sWheelTurningParams.MaxSpeed) {
            cNeighbourVector.Normalize();
            cNeighbourVector *= m_sWheelTurningParams.MaxSpeed;
         }
         
         return cNeighbourVector;
	}
	
	else 
		return CVector2();
}

CVector2 CFootBotGEAggregationController::FlockingVector() {
   /* Get the RAB readings */
   const CCI_RangeAndBearingSensor::TReadings& tRABReads = m_pcRABSens->GetReadings();
   /* Go through the RAB readings to calculate the flocking interaction vector */
   if(! tRABReads.empty()) {
      CVector2 cAccum;
      Real fLJ;
      size_t unNeighboursSensed = 0;

      for(size_t i = 0; i < tRABReads.size(); ++i) {

         /*
          * Consider only the closest neighbors, to avoid
          * attraction to the farthest ones. Taking 180% of the target
          * distance is a good rule of thumb.
          */
         if(tRABReads[i].Range < m_sFlockingParams.TargetDistance * 1.80f) {
            /*
             * Calculate the Lennard-Jones interaction force
             * Form a 2D vector with the interaction force and the angle
             * Sum such vector to the accumulator
             */
            /* Calculate LJ */
            fLJ = m_sFlockingParams.GeneralizedLennardJones(tRABReads[i].Range);
            /* Sum to accumulator */
            cAccum += CVector2(fLJ,
                               tRABReads[i].HorizontalBearing);
            /* Increment the neighbours seen counter */
            ++unNeighboursSensed;
         }
      }
      
      if(unNeighboursSensed > 0) {
         /* Divide the accumulator by the number of blobs seen */
         cAccum /= unNeighboursSensed;
         /* Clamp the length of the vector to the max speed */
         if(cAccum.Length() > m_sWheelTurningParams.MaxSpeed) {
            cAccum.Normalize();
            cAccum *= m_sWheelTurningParams.MaxSpeed;
         }
         return cAccum;
      }
      else
         return CVector2();
   }
   else {
      return CVector2();
   }
}

CVector2 CFootBotGEAggregationController::RandomVector() {

	CRadians orientation;
	orientation = m_pcRNG->Uniform(CRadians::UNSIGNED_RANGE);
	CVector2 randomVector = CVector2(m_sWheelTurningParams.MaxSpeed, orientation);
	
	return randomVector;

}

/****************************************/
/****************************************/

void CFootBotGEAggregationController::SetWheelSpeedsFromVector(const CVector2& c_heading) {
   /* Get the heading angle */
   CRadians cHeadingAngle = c_heading.Angle().SignedNormalize();
   /* Get the length of the heading vector */
   Real fHeadingLength = c_heading.Length();
   /* Clamp the speed so that it's not greater than MaxSpeed */
   Real fBaseAngularWheelSpeed = Min<Real>(fHeadingLength, m_sWheelTurningParams.MaxSpeed);
   /* State transition logic */
   if(m_sWheelTurningParams.TurningMechanism == SWheelTurningParams::HARD_TURN) {
      if(Abs(cHeadingAngle) <= m_sWheelTurningParams.SoftTurnOnAngleThreshold) {
         m_sWheelTurningParams.TurningMechanism = SWheelTurningParams::SOFT_TURN;
      }
   }
   if(m_sWheelTurningParams.TurningMechanism == SWheelTurningParams::SOFT_TURN) {
      if(Abs(cHeadingAngle) > m_sWheelTurningParams.HardTurnOnAngleThreshold) {
         m_sWheelTurningParams.TurningMechanism = SWheelTurningParams::HARD_TURN;
      }
      else if(Abs(cHeadingAngle) <= m_sWheelTurningParams.NoTurnAngleThreshold) {
         m_sWheelTurningParams.TurningMechanism = SWheelTurningParams::NO_TURN;
      }
   }
   if(m_sWheelTurningParams.TurningMechanism == SWheelTurningParams::NO_TURN) {
      if(Abs(cHeadingAngle) > m_sWheelTurningParams.HardTurnOnAngleThreshold) {
         m_sWheelTurningParams.TurningMechanism = SWheelTurningParams::HARD_TURN;
      }
      else if(Abs(cHeadingAngle) > m_sWheelTurningParams.NoTurnAngleThreshold) {
         m_sWheelTurningParams.TurningMechanism = SWheelTurningParams::SOFT_TURN;
      }
   }
   /* Wheel speeds based on current turning state */
   Real fSpeed1, fSpeed2;
   switch(m_sWheelTurningParams.TurningMechanism) {
      case SWheelTurningParams::NO_TURN: {
         /* Just go straight */
         fSpeed1 = fBaseAngularWheelSpeed;
         fSpeed2 = fBaseAngularWheelSpeed;
         break;
      }
      case SWheelTurningParams::SOFT_TURN: {
         /* Both wheels go straight, but one is faster than the other */
         Real fSpeedFactor = (m_sWheelTurningParams.HardTurnOnAngleThreshold - Abs(cHeadingAngle)) / m_sWheelTurningParams.HardTurnOnAngleThreshold;
         fSpeed1 = fBaseAngularWheelSpeed - fBaseAngularWheelSpeed * (1.0 - fSpeedFactor);
         fSpeed2 = fBaseAngularWheelSpeed + fBaseAngularWheelSpeed * (1.0 - fSpeedFactor);
         break;
      }
      case SWheelTurningParams::HARD_TURN: {
         /* Opposite wheel speeds */
         fSpeed1 = -m_sWheelTurningParams.MaxSpeed;
         fSpeed2 =  m_sWheelTurningParams.MaxSpeed;
         break;
      }
   }
   /* Apply the calculated speeds to the appropriate wheels */
   Real fLeftWheelSpeed, fRightWheelSpeed;
   if(cHeadingAngle > CRadians::ZERO) {
      /* Turn Left */
      fLeftWheelSpeed  = fSpeed1;
      fRightWheelSpeed = fSpeed2;
   }
   else {
      /* Turn Right */
      fLeftWheelSpeed  = fSpeed2;
      fRightWheelSpeed = fSpeed1;
   }
   /* Finally, set the wheel speeds */
   m_pcWheelsAct->SetLinearVelocity(fLeftWheelSpeed, fRightWheelSpeed);
}

/****************************************/
/****************************************/

std::vector<CFootBotGEAggregationController::Rule> CFootBotGEAggregationController::ReadRules() {
	std::vector<Rule> rules;
	std::fstream ruleFile;
	std::string rulesString;
	
	ruleFile.open("aggregation_rules.txt");
	
	if (!ruleFile) {
		LOGERR<< "Could not read rule file";
		exit(1);
	}
	
	ruleFile >> rulesString;
	ruleFile.close();
		
	Rule rule;
	std::string preconditionString = "";
	std::string behaviourString = "";
	std::string actionString = "";
	int colonCount = 0;
	
	//std::cout<<"ARGoS: "<<rulesString<<std::endl;
	
	for(int i=0; i<rulesString.length(); i++) {
		
		//std::cout<<"***"<<rulesString.at(i)<<std::endl;
		
		if(rulesString.at(i) != '#') {
			if(rulesString.at(i) == ':')
				colonCount++;
			else if(colonCount == 0)
				preconditionString += rulesString.at(i);
			else if(colonCount == 1)
				behaviourString += rulesString.at(i);
			else if(colonCount == 2)
				actionString += rulesString.at(i);
		
		}
		
		else if(rulesString.at(i) == '#') {
			
			//std::cout<<"***RECONSTRUCTED***: "<<preconditionString<<";"<<behaviourString<<";"<<actionString<<std::endl;
			
			rule.preconditions = GetStringList(preconditionString, ';');
			rule.behaviours = GetStringList(behaviourString, ';');
			rule.actions = GetStringList(actionString, ';');
			rules.push_back(rule);
			
			preconditionString = "";
			behaviourString = "";
			actionString = "";
			colonCount = 0;
		}

	}
	
	return rules;
}

std::vector<CFootBotGEAggregationController::Rule> CFootBotGEAggregationController::SetRules(std::string rulesString) {
	std::vector<Rule> rules;
	
	Rule rule;
	std::string preconditionString = "";
	std::string behaviourString = "";
	std::string actionString = "";
	int colonCount = 0;
	
	//std::cout<<"ARGoS: "<<rulesString<<std::endl;
	
	for(int i=0; i<rulesString.length(); i++) {
		
		//std::cout<<"***"<<rulesString.at(i)<<std::endl;
		
		if(rulesString.at(i) != '#') {
			if(rulesString.at(i) == ':')
				colonCount++;
			else if(colonCount == 0)
				preconditionString += rulesString.at(i);
			else if(colonCount == 1)
				behaviourString += rulesString.at(i);
			else if(colonCount == 2)
				actionString += rulesString.at(i);
		
		}
		
		else if(rulesString.at(i) == '#') {
			
			//std::cout<<"***RECONSTRUCTED***: "<<preconditionString<<";"<<behaviourString<<";"<<actionString<<std::endl;
			
			rule.preconditions = GetStringList(preconditionString, ';');
			rule.behaviours = GetStringList(behaviourString, ';');
			rule.actions = GetStringList(actionString, ';');
			rules.push_back(rule);
			
			preconditionString = "";
			behaviourString = "";
			actionString = "";
			colonCount = 0;
		}

	}
	
	return rules;
}

std::vector<std::string> CFootBotGEAggregationController::GetStringList(std::string itemString, char separator) {
	std::vector<std::string> items;//Preconditions, behaviours or actions
	std::string item = "";//Individual precondition, behaviour or action
		
	/* Parses itemString (a string of preconditions/behaviours/actions) and splits it into individuals */
	for(int i=0; i<itemString.length(); i++) {
		if(itemString.at(i) != separator)
			item += itemString.at(i);
			
		if(itemString.at(i) == separator || i == itemString.length()-1){
			items.push_back(item);
			item = "";
		}
	}
	
	return items;
}
/****************************************/
/****************************************/

void CFootBotGEAggregationController::UpdateState() {
	const CCI_RangeAndBearingSensor::TReadings& tRABReads = m_pcRABSens->GetReadings();
	
	if(tRABReads.size() > 0)
		P_NEAR_NEIGHBOURS = true;
	else
		P_NEAR_NEIGHBOURS = false;
}


bool CFootBotGEAggregationController::CheckPreconditions(std::vector<std::string> preconditions){
	for(int i=0; i<preconditions.size(); i++){
		std::vector<std::string> preconditionParts = GetStringList(preconditions[i], ',');
		
		if(preconditionParts[0] != "*") {
			bool checkValue;
			
			if(preconditionParts[1] == "true")
				checkValue = true;
			else if(preconditionParts[1] == "false")
				checkValue = false;
			else
				LOGERR<<"Precondition contains an invalid boolean string"<<std::endl;
				
			/* If a precondition does not have the desired value, return false */
			if(preconditionParts[0] == "P_NEAR_NEIGHBOURS") {
				if(P_NEAR_NEIGHBOURS != checkValue)
					return false;
			}
		}
	}
	
	return true;
}
   
/****************************************/
/****************************************/

bool CFootBotGEAggregationController::CheckBehaviours(std::vector<std::string> behaviours){
	
	if(behaviours.size() == 1 && behaviours[0] == "*")
		return true;
		
	for(int i=0; i<behaviours.size(); i++) {
		if(behaviours[i] == "B_RANDOM_WALK")
			if(B_RANDOM_WALK == true)
				return true;
		else if(behaviours[i] == "B_FLOCK")
			if(B_FLOCK == true)
				return true;
	}
	
	return false;
}
   
/****************************************/
/****************************************/

void CFootBotGEAggregationController::ExecuteActions(std::vector<std::string> actions){
	for(int i=0; i<actions.size(); i++){
		std::vector<std::string> actionParts = GetStringList(actions[i], ',');
		
		if(actionParts.size() == 2){
			//Execute actionParts[0] with probability actionParts[1]/100
			Real selectionProbability = Real(std::stoi(actionParts[1])) / 100.0f;
			
			if(SelectWithProbability(selectionProbability)){
				if(actionParts[0] == "B_RANDOM_WALK")
					DoRandomWalk();
				else if(actionParts[0] == "B_FLOCK")
					DoFlocking();
				else 
					LOGERR<<"Invalid action selected"<<std::endl;
			}
		}
		else
			LOGERR<<"This action is composed of an incorrect number of parts"<<std::endl;
	}
}

bool CFootBotGEAggregationController::SelectWithProbability(Real probability){
	Real randomNumber = m_pcRNG->Uniform(CRange<Real>(0.0f, 1.0f));
	
	if(randomNumber <= probability)
		return true;
	else
		return false;
}

CRadians CFootBotGEAggregationController::GetAngleMoved() {
	const CCI_DifferentialSteeringSensor::SReading& tWheelReading = m_pcWheelsSens->GetReading();
	
	Real leftWheelDistance = tWheelReading.CoveredDistanceLeftWheel;
	Real rightWheelDistance = tWheelReading.CoveredDistanceRightWheel;
	
	CRadians angleMoved;
	
	if(leftWheelDistance < EPSILON)
		angleMoved = CRadians(rightWheelDistance/tWheelReading.WheelAxisLength);
	else if(rightWheelDistance < EPSILON)
		angleMoved = CRadians(-1.0f*leftWheelDistance/tWheelReading.WheelAxisLength);
	else
		angleMoved = CRadians::PI * rightWheelDistance / tWheelReading.WheelAxisLength;
	
	return angleMoved;
}

/****************************************/
/****************************************/

void CFootBotGEAggregationController::DoRandomWalk(){
	
	if(m_CurrentRandomWalk >= m_RandomWalkTime) {
		m_CurrentRandomWalk = 0;
		m_RandomWalkTime = m_pcRNG->Uniform(CRange<int>(50, 300));
		m_RandomWalkVector = RandomVector();
		
		SetWheelSpeedsFromVector(m_RandomWalkVector);
	}
	
	else {
		//LOG<<m_CurrentRandomWalk<<std::endl;
		
		CRadians angleMoved = GetAngleMoved();
		const CCI_DifferentialSteeringSensor::SReading& tWheelReading = m_pcWheelsSens->GetReading();
		Real leftWheelDistance = tWheelReading.CoveredDistanceLeftWheel;
		Real rightWheelDistance = tWheelReading.CoveredDistanceRightWheel;
		Real leftVelocity = tWheelReading.VelocityLeftWheel;
		Real rightVelocity = tWheelReading.VelocityRightWheel;
		/*
		LOG<<"Left wheel: "<<leftWheelDistance<<std::endl;
		LOG<<"Right wheel: "<<rightWheelDistance<<std::endl;
		LOG<<"Left speed: "<<leftVelocity<<std::endl;
		LOG<<"Right speed: "<<rightVelocity<<std::endl;
		
		LOG<<"Walk angle: "<<m_RandomWalkVector.Angle()<<std::endl;
		LOG<<"Angle moved: "<<angleMoved<<std::endl;
		LOG<<Abs(m_RandomWalkVector.Angle() - angleMoved)<<" < "<<TURN_THRESHOLD<<std::endl;
		LOG<<"-------"<<std::endl;
		*/
		
		if( Abs(m_RandomWalkVector.Angle() - angleMoved) < TURN_THRESHOLD )
			m_RandomWalkVector = CVector2(m_RandomWalkVector.Length(), CRadians::ZERO);
		else
			m_RandomWalkVector = CVector2(m_RandomWalkVector.Length(), m_RandomWalkVector.Angle() - angleMoved);
		
		SetWheelSpeedsFromVector(m_RandomWalkVector - ObstacleAvoidanceVector());
		m_CurrentRandomWalk++;
	}
	
	B_RANDOM_WALK = true;
	B_FLOCK = false;
}

void CFootBotGEAggregationController::DoFlocking(){
	
	//+ RandomVector()
	
	SetWheelSpeedsFromVector(FlockingVector());
	
	B_RANDOM_WALK = false;
	B_FLOCK = true;
}

/****************************************/
/****************************************/

/*
 * This statement notifies ARGoS of the existence of the controller.
 * It binds the class passed as first argument to the string passed as
 * second argument.
 * The string is then usable in the XML configuration file to refer to
 * this controller.
 * When ARGoS reads that string in the XML file, it knows which controller
 * class to instantiate.
 * See also the XML configuration files for an example of how this is used.
 */
REGISTER_CONTROLLER(CFootBotGEAggregationController, "footbot_ge_aggregation_controller")
