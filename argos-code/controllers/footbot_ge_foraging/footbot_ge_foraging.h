#ifndef FOOTBOT_GE_FORAGING_H
#define FOOTBOT_GE_FORAGING_H

/*
 * Include some necessary headers.
 */
/* Definition of the CCI_Controller class. */
#include <argos3/core/control_interface/ci_controller.h>
/* Definition of the differential steering actuator */
#include <argos3/plugins/robots/generic/control_interface/ci_differential_steering_actuator.h>
/* Definition of the range and bearing actuator */
#include <argos3/plugins/robots/generic/control_interface/ci_range_and_bearing_actuator.h>
/* Definition of the gripper actuator */
#include <argos3/plugins/robots/foot-bot/control_interface/ci_footbot_gripper_actuator.h>
/* Definition of the foot-bot proximity sensor */
#include <argos3/plugins/robots/foot-bot/control_interface/ci_footbot_proximity_sensor.h>
/* Definition of the foot-bot light sensor */
#include <argos3/plugins/robots/foot-bot/control_interface/ci_footbot_light_sensor.h>
/* Definition of the foot-bot motor ground sensor */
#include <argos3/plugins/robots/foot-bot/control_interface/ci_footbot_motor_ground_sensor.h>
/* Definition of the range and bearing sensor */
#include <argos3/plugins/robots/generic/control_interface/ci_range_and_bearing_sensor.h>
/* Definition of the differential steering sensor */
#include <argos3/plugins/robots/generic/control_interface/ci_differential_steering_sensor.h>
/* Definitions for random number generation */
#include <argos3/core/utility/math/rng.h>

/*
 * All the ARGoS stuff in the 'argos' namespace.
 * With this statement, you save typing argos:: every time.
 */
using namespace argos;

/*
 * A controller is simply an implementation of the CCI_Controller class.
 */
class CFootBotGEForagingController : public CCI_Controller {

public:
   
   /*
    * The following variables are used as parameters for the
    * diffusion algorithm. You can set their value in the <parameters>
    * section of the XML configuration file
    */
   struct SDiffusionParams {
      /*
       * Maximum tolerance for the proximity reading between
       * the robot and the closest obstacle.
       * The proximity reading is 0 when nothing is detected
       * and grows exponentially to 1 when the obstacle is
       * touching the robot.
       */
      Real Delta;
      /* Angle tolerance range to go straight. */
      CRange<CRadians> GoStraightAngleRange;

      /* Constructor */
      SDiffusionParams();

      /* Parses the XML section for diffusion */
      void Init(TConfigurationNode& t_tree);
   };
   
   /*
    * The following variables are used as parameters for
    * turning during navigation. You can set their value
    * in the <parameters> section of the XML configuration
    * file
    */
   struct SWheelTurningParams {
      /*
       * The turning mechanism.
       * The robot can be in three different turning states.
       */
      enum ETurningMechanism
      {
         NO_TURN = 0, // go straight
         SOFT_TURN,   // both wheels are turning forwards, but at different speeds
         HARD_TURN    // wheels are turning with opposite speeds
      } TurningMechanism;
      /*
       * Angular thresholds to change turning state.
       */
      CRadians HardTurnOnAngleThreshold;
      CRadians SoftTurnOnAngleThreshold;
      CRadians NoTurnAngleThreshold;
      /* Maximum wheel speed */
      Real MaxSpeed;

      void Init(TConfigurationNode& t_tree);
   };
   
   /* Rule that guides controller. Can have one or more */
   struct Rule {
	   std::vector<std::string> preconditions;
	   std::vector<std::string> behaviours;
	   std::vector<std::string> actions;
   };

   void SetRulesFromInput(std::string rulesString);
   
   /* Class constructor. */
   CFootBotGEForagingController();
   /* Class destructor. */
   virtual ~CFootBotGEForagingController() {}

   /*
    * This function initializes the controller.
    */
   virtual void Init(TConfigurationNode& t_node);

   /*
    * This function is called once every time step.
    * The length of the time step is set in the XML file.
    */
   virtual void ControlStep();

   /*
    * This function resets the controller to its state right after the
    * Init().
    * It is called when you press the reset button in the GUI.
    */
   virtual void Reset();

   /*
    * Called to cleanup what done by Init() when the experiment finishes.
    * In this example controller there is no need for clean anything up,
    * so the function could have been omitted. It's here just for
    * completeness.
    */
   virtual void Destroy() {}
   
   int GetCylindersGathered();
   
   bool CheckWantObject();
   bool CheckHasObject();
   bool CheckDropObject();
   void SetHasObjectFlag(bool flag);
   void IncrementCylindersGathered();

private:

   /*
    * Calculates the vector to the most intense light source.
    */
   CVector2 PhototaxisVector();
   
   /*
    * Calculates the vector to the least intense light source.
    */
   CVector2 AntiPhototaxisVector();
   
   /*
    * Calculates a vector to nearby obstacles
    */
   CVector2 ObstacleAvoidanceVector();
   
   /*
    * Calculates a random vector
    */
   CVector2 RandomVector();
   
   /*
    * Calculates a vector to the cylinders
    * DEPRECATED
    */
   //CVector2 CylinderAttractionVector();

   /*
    * Gets a direction vector as input and transforms it into wheel
    * actuation.
    */
   void SetWheelSpeedsFromVector(const CVector2& c_heading);
   
   /* Gets the rules generated by grammatical evolution from a file */
   std::vector<Rule> ReadRules();
   
   /* Sets the rules provided in the arguments */
   std::vector<Rule> SetRules(std::string rulesString);
   
   /* Gets a string of items e.g. preconditions, and turns it into a vector of those items */
   std::vector<std::string> GetStringList(std::string itemString, char separator);
   
   /* Updates preconditions and internal states */
   void UpdateState();
   
   /* Checks if preconditions are all true */
   bool CheckPreconditions(std::vector<std::string> preconditions);
   
   /* Checks if any of the behaviours are currently being performed */
   bool CheckBehaviours(std::vector<std::string> behaviours);
   
   /* Executes all actions in the vector */
   void ExecuteActions(std::vector<std::string> actions);
   
   /* If want a cylinder, don't have one and it's there, pick it up
    * DEPRECATED
    */
   //void CylinderCheck();
   
   
   /** Behaviours **/
   
   /* Move towards light */
   void DoPhototaxis();
   
   /* Move away from light */
   void DoAntiPhototaxis();
   
   /* Move randomly */
   void DoRandomWalk();
   
   /** Helpers **/
   
   /* Return true with probability*/
   bool SelectWithProbability(Real probability);
   
   /* Returns angle turned in the last time step */
   CRadians GetAngleMoved();

private:

	/*  
	 * Actuators
	 *  */
   /* Pointer to the differential steering actuator */
   CCI_DifferentialSteeringActuator* m_pcWheelsAct;
   /* Pointer to the range-and-bearing actuator */
   CCI_RangeAndBearingActuator* m_pcRABAct;
   /* Pointer to the foot-bot gripper actuator */
   //CCI_FootBotGripperActuator* m_pcGripper;
   
	/* 
	 * Sensors
	 *  */
   /* Pointer to the foot-bot proximity sensor */
   CCI_FootBotProximitySensor* m_pcProximity;
   /* Pointer to the foot-bot light sensor */
   CCI_FootBotLightSensor* m_pcLight;
   /* Pointer to the foot-bot motor ground sensor */
   CCI_FootBotMotorGroundSensor* m_pcGround;
   /* Pointer to the range-and-bearing sensor */
   CCI_RangeAndBearingSensor* m_pcRABSens;
   /* Pointer to the foot-bot steering sensor */
   CCI_DifferentialSteeringSensor* m_pcWheelsSens;

   /* The random number generator */
   CRandom::CRNG* m_pcRNG;

   /* The turning parameters */
   SWheelTurningParams m_sWheelTurningParams;
   /* The diffusion parameters */
   SDiffusionParams m_sDiffusionParams;
   
   /* List of rules */
   std::vector<Rule> m_Rules;
   bool RULES_ASSIGNED;
   
   /* Preconditions */
   bool P_ON_SOURCE;
   bool P_ON_NEST;
   bool P_HAS_OBJECT;
   
   /* Behaviours */
   bool B_PHOTOTAXIS;
   bool B_ANTI_PHOTOTAXIS;
   bool B_RANDOM_WALK;
   
   /* States */
   bool IS_WANT_OBJECT;
   bool IS_DROP_OBJECT;
   
   /* Random walk variables */
   int m_RandomWalkTime;
   int m_CurrentRandomWalk;
   CVector2 m_RandomWalkVector;
   const CRadians TURN_THRESHOLD = CRadians(0.8);
   const Real EPSILON = 1.0f;
   
   /*
   int m_RandomWalkTime;
   int m_CurrentRandomWalk;
   CVector2 m_RandomWalkVector;
   const CRadians TURN_THRESHOLD = CRadians(0.8);
   const Real EPSILON = 1.0f;
   */
   
   /* Number of cylinders returned to nest. Used to calculate fitness */
   int m_CylindersGathered;

};

#endif
