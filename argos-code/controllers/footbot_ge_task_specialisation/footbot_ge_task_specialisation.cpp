/* Include the controller definition */
#include "footbot_ge_task_specialisation.h"
/* Function definitions for XML parsing */
#include <argos3/core/utility/configuration/argos_configuration.h>
/* 2D vector definition */
#include <argos3/core/utility/math/vector2.h>
/* Logging */
#include <argos3/core/utility/logging/argos_log.h>

/* File reading */
#include <fstream>
#include <unistd.h>

/****************************************/
/****************************************/

CFootBotGETaskSpecialisationController::SDiffusionParams::SDiffusionParams() :
   GoStraightAngleRange(CRadians(-1.0f), CRadians(1.0f)) {}

void CFootBotGETaskSpecialisationController::SDiffusionParams::Init(TConfigurationNode& t_node) {
   try {
      CRange<CDegrees> cGoStraightAngleRangeDegrees(CDegrees(-10.0f), CDegrees(10.0f));
      GetNodeAttribute(t_node, "go_straight_angle_range", cGoStraightAngleRangeDegrees);
      GoStraightAngleRange.Set(ToRadians(cGoStraightAngleRangeDegrees.GetMin()),
                               ToRadians(cGoStraightAngleRangeDegrees.GetMax()));
      GetNodeAttribute(t_node, "delta", Delta);
   }
   catch(CARGoSException& ex) {
      THROW_ARGOSEXCEPTION_NESTED("Error initializing controller diffusion parameters.", ex);
   }
}

/****************************************/
/****************************************/

void CFootBotGETaskSpecialisationController::SWheelTurningParams::Init(TConfigurationNode& t_node) {
   try {
      TurningMechanism = NO_TURN;
      CDegrees cAngle;
      GetNodeAttribute(t_node, "hard_turn_angle_threshold", cAngle);
      HardTurnOnAngleThreshold = ToRadians(cAngle);
      GetNodeAttribute(t_node, "soft_turn_angle_threshold", cAngle);
      SoftTurnOnAngleThreshold = ToRadians(cAngle);
      GetNodeAttribute(t_node, "no_turn_angle_threshold", cAngle);
      NoTurnAngleThreshold = ToRadians(cAngle);
      GetNodeAttribute(t_node, "max_speed", MaxSpeed);
   }
   catch(CARGoSException& ex) {
      THROW_ARGOSEXCEPTION_NESTED("Error initializing controller wheel turning parameters.", ex);
   }
}

/****************************************/
/****************************************/

CFootBotGETaskSpecialisationController::CFootBotGETaskSpecialisationController() :
   m_pcWheelsAct(NULL),
   m_pcRABAct(NULL),
   m_pcLight(NULL),
   m_pcGround(NULL),
   m_pcProximity(NULL),
   m_pcRABSens(NULL),
   m_pcWheelsSens(NULL),
   m_pcRNG(NULL),
   m_Rules(0) {}

/****************************************/
/****************************************/

void CFootBotGETaskSpecialisationController::Init(TConfigurationNode& t_node) {
   try {
      /*
       * Initialize sensors/actuators
       */
      m_pcWheelsAct    = GetActuator<CCI_DifferentialSteeringActuator>("differential_steering");
      m_pcRABAct    = GetActuator<CCI_RangeAndBearingActuator     >("range_and_bearing"    );
      //m_pcGripper	= GetActuator<CCI_FootBotGripperActuator	  >("footbot_gripper"	   );
      m_pcLight     = GetSensor  <CCI_FootBotLightSensor          >("footbot_light"        );
      m_pcGround    = GetSensor  <CCI_FootBotMotorGroundSensor    >("footbot_motor_ground" );
      m_pcProximity = GetSensor  <CCI_FootBotProximitySensor      >("footbot_proximity"    );
      m_pcRABSens   = GetSensor  <CCI_RangeAndBearingSensor       >("range_and_bearing"    );
      m_pcWheelsSens = GetSensor <CCI_DifferentialSteeringSensor	>("differential_steering");
      //m_pcCamera 	= GetSensor	 <CCI_ColoredBlobOmnidirectionalCameraSensor>("colored_blob_omnidirectional_camera");
      /*
       * Parse XML parameters
       */
      /* Diffusion algorithm */
      m_sDiffusionParams.Init(GetNode(t_node, "diffusion"));
      /* Wheel turning */
      m_sWheelTurningParams.Init(GetNode(t_node, "wheel_turning"));
   }
   catch(CARGoSException& ex) {
      THROW_ARGOSEXCEPTION_NESTED("Error initializing the foot-bot foraging controller for robot \"" << GetId() << "\"", ex);
   }
   /*
    * Initialize other stuff
    */
   /* Create a random number generator. We use the 'argos' category so
      that creation, reset, seeding and cleanup are managed by ARGoS. */
   m_pcRNG = CRandom::CreateRNG("argos");
   //m_Rules = ReadRules();

   RULES_ASSIGNED = false;
   
   P_ON_SOURCE = false;
   P_ON_NEST = false;
   P_HAS_OBJECT = false;

   /* Unique to TS */
   motivation_1 = 0.5;
   motivation_2 = 0.5;
   P_ON_SLOPE = false;
   P_ON_CACHE = false;
   P_MOTIVATION_1_LT = false;
   P_MOTIVATION_1_GT = false;
   P_MOTIVATION_2_LT = false;
   P_MOTIVATION_2_GT = false;
   IS_MOTIVATION_1_INC = false;
   IS_MOTIVATION_1_DEC = false;
   IS_MOTIVATION_2_INC = false;
   IS_MOTIVATION_2_DEC = false;
   /****************/
   
   B_PHOTOTAXIS = false;
   B_ANTI_PHOTOTAXIS = false;
   B_RANDOM_WALK = false;
   
   IS_WANT_OBJECT = false;
   IS_DROP_OBJECT = false;
   
   m_RandomWalkTime = 0;
   m_CurrentRandomWalk = 0;
   
   m_CylindersGathered = 0;
   
   //m_distanceTravelled = 0.0f;
   
}

void CFootBotGETaskSpecialisationController::SetRulesFromInput(std::string rulesString) {
	//LOG<<"Rule string received: "<<rulesString<<std::endl;
	m_Rules = SetRules(rulesString);
	RULES_ASSIGNED = true;
}

/****************************************/
/****************************************/

void CFootBotGETaskSpecialisationController::ControlStep() {
	
	//argos::LOG<<"Start of control step"<<std::endl;
	
	/* */
	UpdateState();
	
	if(!RULES_ASSIGNED) {
		m_Rules = ReadRules();//LOGERR<<"Rules have not been set for this controller"<<std::endl;
		RULES_ASSIGNED = true;
	}
	
	//LOG<<"Number of rules: "<<m_Rules.size()<<std::endl;
	
	for(int i=0; i<m_Rules.size(); i++) {
		if(CheckPreconditions(m_Rules[i].preconditions) &&
			CheckBehaviours(m_Rules[i].behaviours))
			ExecuteActions(m_Rules[i].actions);
		
		//LOG<<"Precondition Check: "<<CheckPreconditions(m_Rules[i].preconditions)<<std::endl;
		//LOG<<"Behaviour Check: "<<CheckBehaviours(m_Rules[i].behaviours)<<std::endl;
	}
	
	if(B_RANDOM_WALK == false)
		m_CurrentRandomWalk = 0;
	
	if(B_PHOTOTAXIS)
		DoPhototaxis();
	else if(B_ANTI_PHOTOTAXIS)
		DoAntiPhototaxis();
	else if(B_RANDOM_WALK)
		DoRandomWalk();
	
	
	
	/*
	if(B_RANDOM_WALK)
		argos::LOG<<"RANDOM WALK- ctrl"<<std::endl;
	else if(B_PHOTOTAXIS)
		argos::LOG<<"PHOTOTAXIS- ctrl"<<std::endl;
	else if(B_ANTI_PHOTOTAXIS)
		argos::LOG<<"ANTIPHOTOTAXIS- ctrl"<<std::endl;
	
	
	if(P_HAS_OBJECT)
		argos::LOG<<"!!!HAS OBJECT!!!"<<std::endl;
	
	if(IS_WANT_OBJECT)
		argos::LOG<<"WANT OBJECT"<<std::endl;
	
	if(IS_DROP_OBJECT)
		argos::LOG<<"DROP OBJECT"<<std::endl;
	 */
	//argos::LOG<<"End of control step"<<std::endl;
	//argos::LOG<<"---"<<std::endl;
}

/****************************************/
/****************************************/

void CFootBotGETaskSpecialisationController::Reset() {
	
	RULES_ASSIGNED = false;
	
	P_ON_SOURCE = false;
	P_ON_NEST = false;
	P_HAS_OBJECT = false;
   
   /* Unique to TS */
   motivation_1 = 0.5;
   motivation_2 = 0.5;
   P_ON_SLOPE = false;
   P_ON_CACHE = false;
   P_MOTIVATION_1_LT = false;
   P_MOTIVATION_1_GT = false;
   P_MOTIVATION_2_LT = false;
   P_MOTIVATION_2_GT = false;
   IS_MOTIVATION_1_INC = false;
   IS_MOTIVATION_1_DEC = false;
   IS_MOTIVATION_2_INC = false;
   IS_MOTIVATION_2_DEC = false;
   /****************/
	
	B_PHOTOTAXIS = false;
	B_ANTI_PHOTOTAXIS = false;
	B_RANDOM_WALK = false;
	
	IS_WANT_OBJECT = false;
	IS_DROP_OBJECT = false;
	
	m_RandomWalkTime = 0;
	m_CurrentRandomWalk = 0;
	
	m_CylindersGathered = 0;
}

int CFootBotGETaskSpecialisationController::GetCylindersGathered() {
	return m_CylindersGathered;
}

void CFootBotGETaskSpecialisationController::IncrementCylindersGathered(){
	m_CylindersGathered++;
}

Real CFootBotGETaskSpecialisationController::GetMaxSpeed(){
	return m_sWheelTurningParams.MaxSpeed;
}

void CFootBotGETaskSpecialisationController::SetMaxSpeed(Real newMaxSpeed){
	m_sWheelTurningParams.MaxSpeed = newMaxSpeed;
}

bool CFootBotGETaskSpecialisationController::IsOnSlope(){
	return P_ON_SLOPE;
}

void CFootBotGETaskSpecialisationController::Freeze(){
   SetWheelSpeedsFromVector(CVector2());
}

/****************************************/
/****************************************/

CVector2 CFootBotGETaskSpecialisationController::PhototaxisVector() {
   /* Get readings from light sensor */
   const CCI_FootBotLightSensor::TReadings& tLightReads = m_pcLight->GetReadings();
   
   /* Calculate the most intense light */
   Real cMax = tLightReads[0].Value;
   CRadians cMaxAngle = tLightReads[0].Angle;
   
   /*
   for(size_t i = 1; i < tLightReads.size(); ++i) {
   std::cout<<"Num readings: "<<tLightReads.size()<<std::endl;
   std::cout<<"Light distance: "<<tLightReads[i].Value<<std::endl;
   std::cout<<"Light angle: "<<tLightReads[i].Angle<<std::endl;
}*/
   
   for(size_t i = 1; i < tLightReads.size(); ++i) {
      if(tLightReads[i].Value > cMax) {
			cMaxAngle = tLightReads[i].Angle;
			cMax = tLightReads[i].Value;
		}
	//LOG<<"Light Vector: "<<CVector2(tLightReads[i].Value, tLightReads[i].Angle)<<std::endl;
	
   }
   
   return CVector2(m_sWheelTurningParams.MaxSpeed, cMaxAngle);
}

CVector2 CFootBotGETaskSpecialisationController::AntiPhototaxisVector() {
   
   /* */
   const CCI_FootBotLightSensor::TReadings& tLightReads = m_pcLight->GetReadings();
   
   Real cMin = tLightReads[0].Value;
   CRadians cMinAngle = tLightReads[0].Angle;
   
   for(size_t i = 1; i < tLightReads.size(); ++i) {
      if(tLightReads[i].Value < cMin) {
		cMinAngle = tLightReads[i].Angle;
		cMin = tLightReads[i].Value;
	}
   }
   
   
   return CVector2(m_sWheelTurningParams.MaxSpeed, cMinAngle);
   
   
   //return PhototaxisVector().Rotate(CRadians::PI);
}

/****************************************/
/****************************************/

CVector2 CFootBotGETaskSpecialisationController::ObstacleAvoidanceVector() {
   
   /* Add vector to detected obstacles */
   const CCI_FootBotProximitySensor::TReadings& tProxReads = m_pcProximity->GetReadings();
   CVector2 cObstacleVector = CVector2();
   for(size_t i = 0; i < tProxReads.size(); ++i) {
	  cObstacleVector += CVector2(tProxReads[i].Value, tProxReads[i].Angle);
   }
   
   /* Add vector to robots */
   const CCI_RangeAndBearingSensor::TReadings& tRABReads = m_pcRABSens->GetReadings();
   for(size_t i =0; i < tRABReads.size(); ++i) {
		cObstacleVector += CVector2(tRABReads[i].Range, tRABReads[i].HorizontalBearing);
	}
   if(cObstacleVector.Length() > 0.0f)
		cObstacleVector = CVector2(m_sWheelTurningParams.MaxSpeed, cObstacleVector.Angle());
   
   return cObstacleVector;
	
}


CVector2 CFootBotGETaskSpecialisationController::RandomVector() {

	CRadians orientation;
	orientation = m_pcRNG->Uniform(CRadians::UNSIGNED_RANGE);
	CVector2 randomVector = CVector2(m_sWheelTurningParams.MaxSpeed, orientation);
	
	return randomVector;

}

/****************************************/
/****************************************/

void CFootBotGETaskSpecialisationController::SetWheelSpeedsFromVector(const CVector2& c_heading) {
   /* Get the heading angle */
   CRadians cHeadingAngle = c_heading.Angle().SignedNormalize();
   /* Get the length of the heading vector */
   Real fHeadingLength = c_heading.Length();
   /* Clamp the speed so that it's not greater than MaxSpeed */
   Real fBaseAngularWheelSpeed = Min<Real>(fHeadingLength, m_sWheelTurningParams.MaxSpeed);
   /* State transition logic */
   if(m_sWheelTurningParams.TurningMechanism == SWheelTurningParams::HARD_TURN) {
      if(Abs(cHeadingAngle) <= m_sWheelTurningParams.SoftTurnOnAngleThreshold) {
         m_sWheelTurningParams.TurningMechanism = SWheelTurningParams::SOFT_TURN;
      }
   }
   if(m_sWheelTurningParams.TurningMechanism == SWheelTurningParams::SOFT_TURN) {
      if(Abs(cHeadingAngle) > m_sWheelTurningParams.HardTurnOnAngleThreshold) {
         m_sWheelTurningParams.TurningMechanism = SWheelTurningParams::HARD_TURN;
      }
      else if(Abs(cHeadingAngle) <= m_sWheelTurningParams.NoTurnAngleThreshold) {
         m_sWheelTurningParams.TurningMechanism = SWheelTurningParams::NO_TURN;
      }
   }
   if(m_sWheelTurningParams.TurningMechanism == SWheelTurningParams::NO_TURN) {
      if(Abs(cHeadingAngle) > m_sWheelTurningParams.HardTurnOnAngleThreshold) {
         m_sWheelTurningParams.TurningMechanism = SWheelTurningParams::HARD_TURN;
      }
      else if(Abs(cHeadingAngle) > m_sWheelTurningParams.NoTurnAngleThreshold) {
         m_sWheelTurningParams.TurningMechanism = SWheelTurningParams::SOFT_TURN;
      }
   }
   /* Wheel speeds based on current turning state */
   Real fSpeed1, fSpeed2;
   switch(m_sWheelTurningParams.TurningMechanism) {
      case SWheelTurningParams::NO_TURN: {
         /* Just go straight */
         fSpeed1 = fBaseAngularWheelSpeed;
         fSpeed2 = fBaseAngularWheelSpeed;
         break;
      }
      case SWheelTurningParams::SOFT_TURN: {
         /* Both wheels go straight, but one is faster than the other */
         Real fSpeedFactor = (m_sWheelTurningParams.HardTurnOnAngleThreshold - Abs(cHeadingAngle)) / m_sWheelTurningParams.HardTurnOnAngleThreshold;
         fSpeed1 = fBaseAngularWheelSpeed - fBaseAngularWheelSpeed * (1.0 - fSpeedFactor);
         fSpeed2 = fBaseAngularWheelSpeed + fBaseAngularWheelSpeed * (1.0 - fSpeedFactor);
         break;
      }
      case SWheelTurningParams::HARD_TURN: {
         /* Opposite wheel speeds */
         fSpeed1 = -m_sWheelTurningParams.MaxSpeed;
         fSpeed2 =  m_sWheelTurningParams.MaxSpeed;
         break;
      }
   }
   /* Apply the calculated speeds to the appropriate wheels */
   Real fLeftWheelSpeed, fRightWheelSpeed;
   if(cHeadingAngle > CRadians::ZERO) {
      /* Turn Left */
      fLeftWheelSpeed  = fSpeed1;
      fRightWheelSpeed = fSpeed2;
   }
   else {
      /* Turn Right */
      fLeftWheelSpeed  = fSpeed2;
      fRightWheelSpeed = fSpeed1;
   }
   /* Finally, set the wheel speeds */
   m_pcWheelsAct->SetLinearVelocity(fLeftWheelSpeed, fRightWheelSpeed);
}

/****************************************/
/****************************************/

std::vector<CFootBotGETaskSpecialisationController::Rule> CFootBotGETaskSpecialisationController::ReadRules() {
	std::vector<Rule> rules;
	std::fstream ruleFile;
	std::string rulesString;
	
	ruleFile.open("task_specialisation_rules.txt");
	
	if (!ruleFile) {
		LOGERR<< "Could not read rule file";
		exit(1);
	}
	
	ruleFile >> rulesString;
	ruleFile.close();
		
	Rule rule;
	std::string preconditionString = "";
	std::string behaviourString = "";
	std::string actionString = "";
	int colonCount = 0;
	
	//std::cout<<"ARGoS: "<<rulesString<<std::endl;
	
	for(int i=0; i<rulesString.length(); i++) {
		
		//std::cout<<"***"<<rulesString.at(i)<<std::endl;
		
		if(rulesString.at(i) != '#') {
			if(rulesString.at(i) == ':')
				colonCount++;
			else if(colonCount == 0)
				preconditionString += rulesString.at(i);
			else if(colonCount == 1)
				behaviourString += rulesString.at(i);
			else if(colonCount == 2)
				actionString += rulesString.at(i);
		
		}
		
		else if(rulesString.at(i) == '#') {
			
			//std::cout<<"***RECONSTRUCTED***: "<<preconditionString<<";"<<behaviourString<<";"<<actionString<<std::endl;
			
			rule.preconditions = GetStringList(preconditionString, ';');
			rule.behaviours = GetStringList(behaviourString, ';');
			rule.actions = GetStringList(actionString, ';');
			rules.push_back(rule);
			
			preconditionString = "";
			behaviourString = "";
			actionString = "";
			colonCount = 0;
		}

	}
	
	return rules;
}

std::vector<CFootBotGETaskSpecialisationController::Rule> CFootBotGETaskSpecialisationController::SetRules(std::string rulesString) {
	std::vector<Rule> rules;
	
	Rule rule;
	std::string preconditionString = "";
	std::string behaviourString = "";
	std::string actionString = "";
	int colonCount = 0;
	
	//std::cout<<"Rule string: "<<rulesString<<std::endl;
	
	for(int i=0; i<rulesString.length(); i++) {
		
		//std::cout<<"***"<<rulesString.at(i)<<std::endl;
		
		if(rulesString.at(i) != '#') {
			if(rulesString.at(i) == ':')
				colonCount++;
			else if(colonCount == 0)
				preconditionString += rulesString.at(i);
			else if(colonCount == 1)
				behaviourString += rulesString.at(i);
			else if(colonCount == 2)
				actionString += rulesString.at(i);
		
		}
		
		else if(rulesString.at(i) == '#') {
			
			//std::cout<<"***RECONSTRUCTED***: "<<preconditionString<<";"<<behaviourString<<";"<<actionString<<std::endl;
			
			//LOG<<"Precondition string: "<<preconditionString<<std::endl;
			rule.preconditions = GetStringList(preconditionString, ';');
			rule.behaviours = GetStringList(behaviourString, ';');
			rule.actions = GetStringList(actionString, ';');
			rules.push_back(rule);
			
			preconditionString = "";
			behaviourString = "";
			actionString = "";
			colonCount = 0;
		}

	}
	
	return rules;
}

std::vector<std::string> CFootBotGETaskSpecialisationController::GetStringList(std::string itemString, char separator) {
	std::vector<std::string> items;//Preconditions, behaviours or actions
	std::string item = "";//Individual precondition, behaviour or action
		
	/* Parses itemString (a string of preconditions/behaviours/actions) and splits it into individuals */
	for(int i=0; i<itemString.length(); i++) {
		if(itemString.at(i) != separator)
			item += itemString.at(i);
			
		if(itemString.at(i) == separator || i == itemString.length()-1){
			items.push_back(item);
			item = "";
		}
	}
	
	return items;
}
/****************************************/
/****************************************/

/* WARNING: DEPENDS ON GROUND COLOURS SET IN LOOP FUNCTIONS */
void CFootBotGETaskSpecialisationController::UpdateState() {
	/* Determine if on source or nest */
	const CCI_FootBotMotorGroundSensor::TReadings& tGroundReads = m_pcGround->GetReadings();
	int nest_senses = 0;
	int source_senses = 0;
	float epsilon = 0.05f;

	//Values obtained from testing sensors in the environment with no noise
	float black = 0.0f;
	float white = 1.0f;
	float red = 0.299f;
	float gray = 0.498039f;

	int black_count = 0;
	int white_count = 0;
	int red_count = 0;
	int gray_count = 0;

	for(size_t i = 0; i < tGroundReads.size(); ++i) {
		//LOG<<"Reading: "<<tGroundReads[i].Value<<std::endl;
		if(fabs(tGroundReads[i].Value-black) < epsilon){
			//LOG<<"Black: "<<black<<std::endl;
			//LOG<<tGroundReads[i].Value-black<<std::endl;
			//LOG<<fabs(tGroundReads[i].Value-black)<<std::endl;
			black_count++;
			//LOG<<"Black count: "<<black_count<<std::endl;
		}
		else if(fabs(tGroundReads[i].Value-white) < epsilon){
			white_count++;
			//LOG<<"White count: "<<white_count<<std::endl;
		}
		else if(fabs(tGroundReads[i].Value-red) < epsilon){
			red_count++;
			//LOG<<"Red count: "<<red_count<<std::endl;
		}
		else if(fabs(tGroundReads[i].Value-gray) < epsilon){
			gray_count++;
			//LOG<<"Gray count: "<<gray_count<<std::endl;
		}
	}
	
	/* A robot is considered to be 'on' an area if 3 out of its 4 ground sensors place it there */
	if(black_count >= 3) {
		P_ON_SOURCE = true;
		P_ON_NEST = false;
		P_ON_SLOPE = false;
		P_ON_CACHE = false;
		//LOG<<"On Source"<<std::endl;
	}
	else if(white_count >= 3) {
		P_ON_SOURCE = false;
		P_ON_NEST = true;
		P_ON_SLOPE = false;
		P_ON_CACHE = false;
		//LOG<<"On Nest"<<std::endl;
	}
	else if(red_count >= 3) {
		P_ON_SOURCE = false;
		P_ON_NEST = false;
		P_ON_SLOPE = true;
		P_ON_CACHE = false;
		//LOG<<"On Slope"<<std::endl;
	}
	else if(gray_count >= 3) {
		P_ON_SOURCE = false;
		P_ON_NEST = false;
		P_ON_SLOPE = false;
		P_ON_CACHE = true;
		//LOG<<"On Cache"<<std::endl;
	}

	/* Motivation Variables */
	if(motivation_1 >= 0.5){
		P_MOTIVATION_1_GT = true;
		P_MOTIVATION_1_LT = false;
	}
	else {
		P_MOTIVATION_1_GT = false;
		P_MOTIVATION_1_LT = true;
	}

	if(motivation_2 >= 0.5){
		P_MOTIVATION_2_GT = true;
		P_MOTIVATION_2_LT = false;
	}
	else {
		P_MOTIVATION_2_GT = false;
		P_MOTIVATION_2_LT = true;
	}

	/* Increasing and decreasing motivation variables */
	if(IS_MOTIVATION_1_INC && motivation_1 < MOTIVATION_MAX)
		motivation_1 += MOTIVATION_INCREMENT;
	else if(IS_MOTIVATION_1_DEC && motivation_1 > MOTIVATION_MIN)
		motivation_1 -= MOTIVATION_INCREMENT;
	else if(IS_MOTIVATION_2_INC && motivation_2 < MOTIVATION_MAX)
		motivation_2 += MOTIVATION_INCREMENT;
	else if(IS_MOTIVATION_2_DEC && motivation_2 > MOTIVATION_MIN)
		motivation_2 -= MOTIVATION_INCREMENT;
	
	//LOG<<"Motivation 1: "<<motivation_1<<std::endl;
	//LOG<<"Motivation 2: "<<motivation_2<<std::endl;
}


bool CFootBotGETaskSpecialisationController::CheckPreconditions(std::vector<std::string> preconditions){
	for(int i=0; i<preconditions.size(); i++){
		std::vector<std::string> preconditionParts = GetStringList(preconditions[i], ',');
		//LOG<<"Precondition: "<<preconditions[i]<<std::endl;
		//for(int j=0; j<preconditionParts.size(); j++)
			//LOG<<"Part "<<j<<" of precondition is: "<<preconditionParts[j]<<std::endl;
		
		
		if(preconditionParts[0] != "*") {
			bool checkValue;
			
			if(preconditionParts[1] == "true")
				checkValue = true;
			else if(preconditionParts[1] == "false")
				checkValue = false;
			else
				LOGERR<<"Precondition contains an invalid boolean string"<<std::endl;
				
			/* If a precondition does not have the desired value, return false */
			if(preconditionParts[0] == "P_ON_SOURCE") {
				if(P_ON_SOURCE != checkValue)
					return false;
			}
			else if(preconditionParts[0] == "P_ON_NEST") {
				if(P_ON_NEST != checkValue)
					return false;
			}
			else if(preconditionParts[0] == "P_HAS_OBJECT") {
				if(P_HAS_OBJECT != checkValue)
					return false;
			}
			else if(preconditionParts[0] == "P_ON_SLOPE") {
				if(P_ON_SLOPE != checkValue)
					return false;
			}
			else if(preconditionParts[0] == "P_ON_CACHE") { 
				if(P_ON_CACHE != checkValue)
					return false;
			}
			else if(preconditionParts[0] == "P_MOTIVATION_1_LT") {
				if(P_MOTIVATION_1_LT != checkValue)
					return false;
			}
			else if(preconditionParts[0] == "P_MOTIVATION_1_GT") {
				if(P_MOTIVATION_1_GT != checkValue)
					return false;
			}
			else if(preconditionParts[0] == "P_MOTIVATION_2_LT") {
				if(P_MOTIVATION_2_LT != checkValue)
					return false;
			}
			else if(preconditionParts[0] == "P_MOTIVATION_2_GT") {
				if(P_MOTIVATION_2_GT != checkValue)
					return false;
			}
		}
	}
	
	return true;
}
   
/****************************************/
/****************************************/

bool CFootBotGETaskSpecialisationController::CheckBehaviours(std::vector<std::string> behaviours){
	
	//argos::LOG<<"???Behaviour Check ???"<<std::endl;
	
	if(behaviours.size() == 1 && behaviours[0] == "*")
		return true;
		
	for(int i=0; i<behaviours.size(); i++) {
		
		//argos::LOG<<"Behaviour: "<<behaviours[i]<<std::endl;
		/*
		argos::LOG<<(B_PHOTOTAXIS == true)<<std::endl;
		argos::LOG<<(B_ANTI_PHOTOTAXIS == true)<<std::endl;
		argos::LOG<<(B_RANDOM_WALK == true)<<std::endl;
		*/
				
		/////////////////////
		
		if(behaviours[i] == "B_PHOTOTAXIS"){
			if(B_PHOTOTAXIS == true)
				return true;
		}
		else if(behaviours[i] == "B_ANTI_PHOTOTAXIS"){
			if(B_ANTI_PHOTOTAXIS == true)
				return true;
		}
		else if(behaviours[i] == "B_RANDOM_WALK"){
			if(B_RANDOM_WALK == true)
				return true;
		}
	}
	
	return false;
}
   
/****************************************/
/****************************************/

void CFootBotGETaskSpecialisationController::ExecuteActions(std::vector<std::string> actions){
	for(int i=0; i<actions.size(); i++){
		//argos::LOG<<"Action: "<<actions[i]<<std::endl;
		
		std::vector<std::string> actionParts = GetStringList(actions[i], ',');
		
		if(actionParts.size() == 2){
			//Execute actionParts[0] with probability actionParts[1]/100
			Real selectionProbability = Real(std::stoi(actionParts[1])) / 100.0f;
			
			if(SelectWithProbability(selectionProbability)){
				if(actionParts[0] == "B_PHOTOTAXIS")
					DoPhototaxis();
				else if(actionParts[0] == "B_ANTI_PHOTOTAXIS")
					DoAntiPhototaxis();
				else if(actionParts[0] == "B_RANDOM_WALK")
					DoRandomWalk();
				else 
					LOGERR<<"Invalid action selected"<<std::endl;
			}
		}
		else if(actionParts.size() == 3){
			//Set state actionParts[0] to boolean value actionParts[1] with probability actionParts[2]
			Real selectionProbability = Real(std::stoi(actionParts[2])) / 100.0f;
			bool activationValue;
			
			if(SelectWithProbability(selectionProbability)){
				if(actionParts[1] == "true")
					activationValue = true;
				else if(actionParts[1] == "false")
					activationValue = false;
				else
					LOGERR<<"Action contains an invalid boolean string"<<std::endl;
				
				if(actionParts[0] == "IS_WANT_OBJECT") {
					IS_WANT_OBJECT = activationValue;
					IS_DROP_OBJECT = !IS_WANT_OBJECT;
				}
				else if(actionParts[0] == "IS_DROP_OBJECT") {
					IS_DROP_OBJECT = activationValue;
					IS_WANT_OBJECT = !IS_DROP_OBJECT;
				}
				else if(actionParts[0] == "IS_MOTIVATION_1_INC") {
					IS_MOTIVATION_1_INC = activationValue;
					if(IS_MOTIVATION_1_INC)
						IS_MOTIVATION_1_DEC = false;
				}
				else if(actionParts[0] == "IS_MOTIVATION_1_DEC") {
					IS_MOTIVATION_1_DEC = activationValue;
					if(IS_MOTIVATION_1_DEC)
						IS_MOTIVATION_1_INC = false;
				}
				else if(actionParts[0] == "IS_MOTIVATION_2_INC") {
					IS_MOTIVATION_2_INC = activationValue;
					if(IS_MOTIVATION_2_INC)
						IS_MOTIVATION_2_DEC = false;
				}
				else if(actionParts[0] == "IS_MOTIVATION_2_DEC") {
					IS_MOTIVATION_2_DEC = activationValue;
					if(IS_MOTIVATION_2_DEC)
						IS_MOTIVATION_2_INC = false;
				}
				else
					LOGERR<<"Invalid internal state selected"<<std::endl;
			}
		}
		else
			LOGERR<<"This action is composed of an incorrect number of parts"<<std::endl;
	}
}

bool CFootBotGETaskSpecialisationController::SelectWithProbability(Real probability){
	Real randomNumber = m_pcRNG->Uniform(CRange<Real>(0.0f, 1.0f));
	
	if(randomNumber <= probability)
		return true;
	else
		return false;
}

CRadians CFootBotGETaskSpecialisationController::GetAngleMoved() {
	const CCI_DifferentialSteeringSensor::SReading& tWheelReading = m_pcWheelsSens->GetReading();
	
	Real leftWheelDistance = tWheelReading.CoveredDistanceLeftWheel;
	Real rightWheelDistance = tWheelReading.CoveredDistanceRightWheel;
	
	CRadians angleMoved;
	
	if(leftWheelDistance < EPSILON)
		angleMoved = CRadians(rightWheelDistance/tWheelReading.WheelAxisLength);
	else if(rightWheelDistance < EPSILON)
		angleMoved = CRadians(-1.0f*leftWheelDistance/tWheelReading.WheelAxisLength);
	else
		angleMoved = CRadians::PI * rightWheelDistance / tWheelReading.WheelAxisLength;
	
	return angleMoved;
}

/****************************************/
/****************************************/

void CFootBotGETaskSpecialisationController::DoPhototaxis(){
	//argos::LOG<<"PHOTOTAXIS"<<std::endl;
	CVector2 lightVector = PhototaxisVector();
	CVector2 obstacleAvoidanceVector = ObstacleAvoidanceVector();
	
	SetWheelSpeedsFromVector(lightVector - obstacleAvoidanceVector);
	
	B_PHOTOTAXIS = true;
	B_ANTI_PHOTOTAXIS = false;
	B_RANDOM_WALK = false;
}

void CFootBotGETaskSpecialisationController::DoAntiPhototaxis(){
	//argos::LOG<<"ANTIPHOTOTAXIS"<<std::endl;
	CVector2 lightVector = AntiPhototaxisVector();
	CVector2 obstacleAvoidanceVector = ObstacleAvoidanceVector();
	
	SetWheelSpeedsFromVector(lightVector - obstacleAvoidanceVector);
	
	B_PHOTOTAXIS = false;
	B_ANTI_PHOTOTAXIS = true;
	B_RANDOM_WALK = false;
}

void CFootBotGETaskSpecialisationController::DoRandomWalk(){
	//argos::LOG<<"RANDOM WALK"<<std::endl;
	
	if(m_CurrentRandomWalk >= m_RandomWalkTime) {
		m_CurrentRandomWalk = 0;
		m_RandomWalkTime = m_pcRNG->Uniform(CRange<int>(50, 300));
		m_RandomWalkVector = RandomVector();
		
		SetWheelSpeedsFromVector(m_RandomWalkVector);
	}
	
	else {
		//LOG<<m_CurrentRandomWalk<<std::endl;
		
		CRadians angleMoved = GetAngleMoved();
		const CCI_DifferentialSteeringSensor::SReading& tWheelReading = m_pcWheelsSens->GetReading();
		Real leftWheelDistance = tWheelReading.CoveredDistanceLeftWheel;
		Real rightWheelDistance = tWheelReading.CoveredDistanceRightWheel;
		Real leftVelocity = tWheelReading.VelocityLeftWheel;
		Real rightVelocity = tWheelReading.VelocityRightWheel;
		
		if( Abs(m_RandomWalkVector.Angle() - angleMoved) < TURN_THRESHOLD )
			m_RandomWalkVector = CVector2(m_RandomWalkVector.Length(), CRadians::ZERO);
		else
			m_RandomWalkVector = CVector2(m_RandomWalkVector.Length(), m_RandomWalkVector.Angle() - angleMoved);
		
		SetWheelSpeedsFromVector(m_RandomWalkVector - ObstacleAvoidanceVector());
		m_CurrentRandomWalk++;
	}
	
	B_PHOTOTAXIS = false;
	B_ANTI_PHOTOTAXIS = false;
	B_RANDOM_WALK = true;
}

/****************************************/
/****************************************/

bool CFootBotGETaskSpecialisationController::CheckWantObject(){
	return IS_WANT_OBJECT;
}

bool CFootBotGETaskSpecialisationController::CheckHasObject(){
	return P_HAS_OBJECT;
}

bool CFootBotGETaskSpecialisationController::CheckDropObject(){
	return IS_DROP_OBJECT;
}

void CFootBotGETaskSpecialisationController::SetHasObjectFlag(bool flag){
	P_HAS_OBJECT = flag;
}

/*
 * This statement notifies ARGoS of the existence of the controller.
 * It binds the class passed as first argument to the string passed as
 * second argument.
 * The string is then usable in the XML configuration file to refer to
 * this controller.
 * When ARGoS reads that string in the XML file, it knows which controller
 * class to instantiate.
 * See also the XML configuration files for an example of how this is used.
 */
REGISTER_CONTROLLER(CFootBotGETaskSpecialisationController, "footbot_ge_task_specialisation_controller")
