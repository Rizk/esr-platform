#include "footbot_nn_controller.h"

/****************************************/
/****************************************/

static CRange<Real> NN_OUTPUT_RANGE(0.0f, 1.0f);
Real MaxSpeed = 20.0f;
CRange<Real> WHEEL_ACTUATION_RANGE(-MaxSpeed, MaxSpeed);
static CRange<int> DROP_VARIABLE_RANGE(0,1);

/****************************************/
/****************************************/

CFootBotNNController::CFootBotNNController() {
}

/****************************************/
/****************************************/

CFootBotNNController::~CFootBotNNController() {
}

/****************************************/
/****************************************/

void CFootBotNNController::Init(TConfigurationNode& t_node) {
   /*
    * Get sensor/actuator handles
    */
   try {
      m_pcWheels    = GetActuator<CCI_DifferentialSteeringActuator>("differential_steering");
      m_pcProximity = GetSensor  <CCI_FootBotProximitySensor      >("footbot_proximity"    );
      m_pcLight     = GetSensor  <CCI_FootBotLightSensor          >("footbot_light"        );
      m_pcGround    = GetSensor  <CCI_FootBotMotorGroundSensor    >("footbot_motor_ground" );
      m_pcRABSens   = GetSensor  <CCI_RangeAndBearingSensor       >("range_and_bearing"    );
   }
   catch(CARGoSException& ex) {
      THROW_ARGOSEXCEPTION_NESTED("Error initializing sensors/actuators", ex);
   }

   /* Initialize the perceptron */
   try {
      m_cPerceptron.Init(t_node);
   }
   catch(CARGoSException& ex) {
      THROW_ARGOSEXCEPTION_NESTED("Error initializing the perceptron network", ex);
   }
   
   hasObject = false;
   wantObject = false;
   dropObject = false;
   m_CylindersGathered = 0;
}

/****************************************/
/****************************************/

void CFootBotNNController::ControlStep() {
   /* Get sensory data */
   const CCI_FootBotProximitySensor::TReadings& tProxReads = m_pcProximity->GetReadings();
   const CCI_FootBotLightSensor::TReadings& tLightReads = m_pcLight->GetReadings();
   const CCI_FootBotMotorGroundSensor::TReadings& tGroundReads = m_pcGround->GetReadings();
   const CCI_RangeAndBearingSensor::TReadings& tRABReads = m_pcRABSens->GetReadings();
   /* Fill NN inputs from sensory data */
   for(size_t i = 0; i < tProxReads.size(); ++i) {
      m_cPerceptron.SetInput(i, tProxReads[i].Value);
   }
   for(size_t i = 0; i < tLightReads.size(); ++i) {
      m_cPerceptron.SetInput(tProxReads.size()+i, tLightReads[i].Value);
   }
   for(size_t i = 0; i < tGroundReads.size(); ++i) {
	   m_cPerceptron.SetInput(tProxReads.size()+tLightReads.size()+i, tGroundReads[i].Value);
   }
   CVector2 RABVector = CVector2();
   for(size_t i = 0; i < tRABReads.size(); ++i) {
		RABVector += CVector2(1/tRABReads[i].Range, tRABReads[i].HorizontalBearing);
   }
   m_cPerceptron.SetInput(tProxReads.size()+tLightReads.size()+tGroundReads.size()+1, RABVector.DotProduct(CVector2(1.0f,CRadians::PI_OVER_FOUR)) );
   m_cPerceptron.SetInput(tProxReads.size()+tLightReads.size()+tGroundReads.size()+2, RABVector.DotProduct(CVector2(1.0f,3*CRadians::PI_OVER_FOUR)) );
   m_cPerceptron.SetInput(tProxReads.size()+tLightReads.size()+tGroundReads.size()+3, RABVector.DotProduct(CVector2(1.0f,5*CRadians::PI_OVER_FOUR)) );
   m_cPerceptron.SetInput(tProxReads.size()+tLightReads.size()+tGroundReads.size()+4, RABVector.DotProduct(CVector2(1.0f,7*CRadians::PI_OVER_FOUR)) );
   m_cPerceptron.SetInput(tProxReads.size()+tLightReads.size()+tGroundReads.size()+5, 1-(2/(1+::exp(tRABReads.size()))));
   /* Compute NN outputs */
   m_cPerceptron.ComputeOutputs();
   /*
    * Apply NN outputs to actuation
    * The NN outputs are in the range [0,1]
    * To allow for backtracking, we remap this range
    * into [-5:5] linearly.
    */
   NN_OUTPUT_RANGE.MapValueIntoRange(
      m_fLeftSpeed,               // value to write
      m_cPerceptron.GetOutput(0), // value to read
      WHEEL_ACTUATION_RANGE       // target range (here [-5:5])
      );
   NN_OUTPUT_RANGE.MapValueIntoRange(
      m_fRightSpeed,              // value to write
      m_cPerceptron.GetOutput(1), // value to read
      WHEEL_ACTUATION_RANGE       // target range (here [-5:5])
      );
   /*NN_OUTPUT_RANGE.MapValueIntoRange(
      m_dropObjectInt,              // value to write
      m_cPerceptron.GetOutput(2), // value to read
      DROP_VARIABLE_RANGE       // target range (here [-5:5])
      );*/
   m_pcWheels->SetLinearVelocity(
      m_fLeftSpeed,
      m_fRightSpeed);
   /*if(m_dropObjectInt == 1){
      dropObject = true;
      wantObject = false;
   }
   else if(m_dropObjectInt == 0){
      dropObject = false;
      wantObject = true;
   }

   std::cout<<dropObject<<std::endl;*/
}

/****************************************/
/****************************************/

void CFootBotNNController::Reset() {
   m_cPerceptron.Reset();
   hasObject = false;
   dropObject = false;
   wantObject = false;
   m_CylindersGathered = 0;
}

/****************************************/
/****************************************/

void CFootBotNNController::Destroy() {
   m_cPerceptron.Destroy();
}

/****************************************/
/****************************************/

bool CFootBotNNController::CheckHasObject(){
	return hasObject;
}

bool CFootBotNNController::CheckWantObject(){
  return wantObject;
}

bool CFootBotNNController::CheckDropObject(){
  return dropObject;
}

void CFootBotNNController::SetHasObjectFlag(bool flag){
	hasObject = flag;
}


int CFootBotNNController::GetCylindersGathered() {
	return m_CylindersGathered;
}

void CFootBotNNController::IncrementCylindersGathered(){
	m_CylindersGathered++;
}

Real CFootBotNNController::GetMaxSpeed(){
  return MaxSpeed;
}

void CFootBotNNController::SetMaxSpeed(Real newMaxSpeed){
  MaxSpeed = newMaxSpeed;
}


/****************************************/
/****************************************/

REGISTER_CONTROLLER(CFootBotNNController, "footbot_nn_controller")
