/* ARGoS-related headers */
#include <argos3/core/simulator/simulator.h>
#include <argos3/core/simulator/loop_functions.h>

#include <loop_functions/ge_aggregation_loop_functions/ge_aggregation_loop_functions.h>

/* File writing */
#include <fstream>

/*
 * The function used to aggregate the scores of each trial.  In this
 * experiment, the score is the distance of the robot from the
 * light. We take the maximum value as aggregated score.
 */
Real ScoreAggregator(const std::vector<Real>& vec_scores) {
   Real fScore = vec_scores[0];
   for(size_t i = 1; i < vec_scores.size(); ++i) {
      fScore = Max(fScore, vec_scores[i]);
   }
   return fScore;
}

int main(int argc, char** argv) {
   
   /*
   argos::LOG<<argc<<std::endl;
   for(int i=0; i<argc; i++) {
		argos::LOG<<"Arg "<<i<<": "<<argv[i]<<std::endl;
   }
   */
  
   /* The CSimulator class of ARGoS is a singleton. Therefore, to
    * manipulate an ARGoS experiment, it is enough to get its instance.
    * This variable is declared 'static' so it is created
    * once and then reused at each call of this function.
    * This line would work also without 'static', but written this way
    * it is faster. */
   static argos::CSimulator& cSimulator = argos::CSimulator::GetInstance();
   
   /* Set the .argos configuration file
    * This is a relative path which assumed that you launch the executable
    * from argos3-examples (as said also in the README) */
   cSimulator.SetExperimentFileName("experiments/ge_aggregation.argos");
   /* Load it to configure ARGoS */
   cSimulator.LoadExperiment();
   /* Get a reference to the loop functions */
   static GEAggregationLoopFunctions& cLoopFunctions = dynamic_cast<GEAggregationLoopFunctions&>(cSimulator.GetLoopFunctions());
   
   std::cout<<"ABOUT TO DO RULE CHECK"<<std::endl;
   
   /* Assign rules to each controller */
   if(argc > 1) {
		/*std::cout<<"THERE ARE "<<argc<<" ARGUMENTS"<<std::endl;
		for(int i=0;i<argc;i++){
			std::cout<<"ARG "<<i<<": "<<argv[i]<<std::endl;
		}*/
		std::cout<<"Main: Assigning Rules"<<std::endl;
		cLoopFunctions.AssignRules(argv[1]);
	}
	else {
		LOGERR<<"There is a missing argument. Did you pass the rules?"<<std::endl;
		//std::cout<<"YOU DUN GOOFED"<<std::endl;
	}
   
   /*
    * Run n trials and take the worst performance as final value.
    * The performance here is average distance from the center of mass of the swarm.
    * Thus we take the greatest distance
    */
   Real fDistance = 0.0f;
   for(size_t i = 0; i < cLoopFunctions.GetNumTrials(); ++i) {
	  argos::LOG<<"Trial #"<<i<<std::endl;
      /* Tell the loop functions to get ready for the i-th trial */
      cLoopFunctions.SetTrial(i);
      /* Reset the experiment. */
      cSimulator.Reset();
      /* Re-assigning rules */
      cLoopFunctions.AssignRules(argv[1]);
      /* Run the experiment */
      cSimulator.Execute();
      /* Update performance */
      fDistance = Max(fDistance, cLoopFunctions.Performance());
   }
   
   //std::fstream performanceFile;
   //performanceFile.open("aggregation_performance.txt", std::fstream::out);
   //performanceFile<<fDistance;
   //performanceFile.close();

   /*
    * Dispose of ARGoS stuff
    */
   cSimulator.Destroy();
   
   std::cout<<"PERFORMANCE,"<<fDistance<<",PERFORMANCE"<<std::endl;

   /* All is OK */
   //std::cout<<"So far so good"<<std::endl;
   return 0;
}
