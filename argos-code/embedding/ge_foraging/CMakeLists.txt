add_executable(ge_foraging main.cpp)

target_link_libraries(ge_foraging
  footbot_ge_foraging
  ge_foraging_loop_functions
  argos3core_simulator)
