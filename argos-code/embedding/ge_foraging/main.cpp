/* ARGoS-related headers */
#include <argos3/core/simulator/simulator.h>
#include <argos3/core/simulator/loop_functions.h>

#include <loop_functions/ge_foraging_loop_functions/ge_foraging_loop_functions.h>

/* File writing */
#include <fstream>

/*
 * The function used to aggregate the scores of each trial.  In this
 * experiment, the score is the number of cylinders gathered. 
 * We take the maximum value as aggregated score.
 */
Real ScoreAggregator(const std::vector<Real>& vec_scores) {
   Real fScore = vec_scores[0];
   for(size_t i = 1; i < vec_scores.size(); ++i) {
      fScore = Max(fScore, vec_scores[i]);
   }
   return fScore;
}

int main(int argc, char** argv) {
  
   /* The CSimulator class of ARGoS is a singleton. Therefore, to
    * manipulate an ARGoS experiment, it is enough to get its instance.
    * This variable is declared 'static' so it is created
    * once and then reused at each call of this function.
    * This line would work also without 'static', but written this way
    * it is faster. */
   static argos::CSimulator& cSimulator = argos::CSimulator::GetInstance();
   
   /* Set the .argos configuration file
    * This is a relative path which assumed that you launch the executable
    * from argos3-examples (as said also in the README) */
   cSimulator.SetExperimentFileName("experiments/ge_foraging.argos");
   
   /* Load it to configure ARGoS */
   cSimulator.LoadExperiment();
   /* Get a reference to the loop functions */
   static GEForagingLoopFunctions& cLoopFunctions = dynamic_cast<GEForagingLoopFunctions&>(cSimulator.GetLoopFunctions());
   
   /* Assign rules to each controller */
   
   if(argc > 1)
		cLoopFunctions.AssignRules(argv[1]);
	else
		LOGERR<<"There is a missing argument. Did you pass the rules?"<<std::endl;
   
   /*
    * Run n trials and take the worst performance as final value.
    * The performance here is number of cylinders collected.
    * Thus we take the greatest distance
    */
   float fCylinders = 0.0;
   for(size_t i = 0; i < cLoopFunctions.GetNumTrials(); i++) {
	//  argos::LOG<<"Trial #"<<i<<std::endl;
	  //argos::LOG<<"Fails in SetTrial"<<i<<std::endl;
      /* Tell the loop functions to get ready for the i-th trial */
      cLoopFunctions.SetTrial(i);
      //argos::LOG<<"Fails in Reset"<<i<<std::endl;
      /* Reset the experiment. */
      cSimulator.Reset();
      /* Re-assigning rules */
      cLoopFunctions.AssignRules(argv[1]);
     //argos::LOG<<"Fails in Execute"<<i<<std::endl;
      //argos::LOG<<cLoopFunctions.GetTrial()<<std::endl;
      /* Run the experiment */
      cSimulator.Execute();
      //argos::LOG<<cLoopFunctions.GetTrial()<<std::endl;
      //argos::LOG<<"Fails in Performance"<<i<<std::endl;
      /* Update performance */
      fCylinders += float(cLoopFunctions.Performance());
      //argos::LOG<<"The performance is "<<fCylinders<<std::endl;
   }
   fCylinders /= cLoopFunctions.GetNumTrials();
   
  // argos::LOG<<"Max performance is "<<fCylinders<<std::endl;

   /*
    * Dispose of ARGoS stuff
    */
   //std::cout<<"Did we even get this far?"<<std::endl;
   cSimulator.Destroy();
   
   //IMPORTANT LINE OF CODE!
   std::cout<<"PERFORMANCE,"<<fCylinders<<",PERFORMANCE"<<std::endl;
   //std::cout<<"PERFORMANCE,"<<1<<",PERFORMANCE"<<std::endl;

   /* All is OK */
   return 0;
}
