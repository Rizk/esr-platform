/* ARGoS-related headers */
#include <argos3/core/simulator/simulator.h>
#include <argos3/core/simulator/loop_functions.h>

#include <loop_functions/ge_task_specialisation_loop_functions/ge_task_specialisation_loop_functions.h>

/* File writing */
#include <fstream>
#include <fenv.h>

/*
 * The function used to aggregate the scores of each trial.  In this
 * experiment, the score is the distance of the robot from the
 * light. We take the maximum value as aggregated score.
 */
Real ScoreAggregator(const std::vector<Real>& vec_scores) {
   Real fScore = vec_scores[0];
   for(size_t i = 1; i < vec_scores.size(); ++i) {
      fScore = Max(fScore, vec_scores[i]);
   }
   return fScore;
}

//Arguments: visualisation_flag logging_flag slope_angle num_trials rules
int main(int argc, char** argv) {
   //feenableexcept(FE_INVALID);
   int num_trials;
  
   /* The CSimulator class of ARGoS is a singleton. Therefore, to
    * manipulate an ARGoS experiment, it is enough to get its instance.
    * This variable is declared 'static' so it is created
    * once and then reused at each call of this function.
    * This line would work also without 'static', but written this way
    * it is faster. */
   static argos::CSimulator& cSimulator = argos::CSimulator::GetInstance();
   
   /* Set .argos file to be used 
      "vis" = visualisation on
      "no-vis" = no visualisation
      TODO: Fix this. Visualisation part doesn't work.
   */
   if(argc > 1) {
    if(atoi(argv[1]) == 1) {
      /* Set the .argos configuration file
      * This is a relative path which assumed that you launch the executable
    * from argos3-examples (as said also in the README) */
      cSimulator.SetExperimentFileName("experiments/ge_task_specialisation-trial.argos");
      //std::cout<<"Visualisation on"<<std::endl;
    }
    else if(atoi(argv[1]) == 0) {
      cSimulator.SetExperimentFileName("experiments/ge_task_specialisation.argos");
      //std::cout<<"Visualisation off"<<std::endl;
    }
      
   }

   /* Load it to configure ARGoS */
   cSimulator.LoadExperiment();
   /* Get a reference to the loop functions */
   static GETaskSpecialisationLoopFunctions& cLoopFunctions = dynamic_cast<GETaskSpecialisationLoopFunctions&>(cSimulator.GetLoopFunctions());
   
   //std::cout<<"ABOUT TO DO RULE CHECK"<<std::endl;

   //
   if(argc > 2) {
    if(atoi(argv[2]) == 1)
      cLoopFunctions.SetLogging(true);
    else if(atoi(argv[2]) == 0) {
      cLoopFunctions.SetLogging(false);
    }
   }

   /* Set angle of slope */
   if(argc > 3) {
    cLoopFunctions.SetSlopeAngle( atof(argv[3]) );
    //std::cout<<"Slope angle set to "<<argv[3]<<std::endl;
   }

   if(argc > 4) {
    num_trials = atoi(argv[4]);
    cLoopFunctions.SetNumTrials(num_trials);
   }

   /* Assign rules to each controller */
   if(argc > 5) {
		/*std::cout<<"THERE ARE "<<argc<<" ARGUMENTS"<<std::endl;
		for(int i=0;i<argc;i++){
			std::cout<<"ARG "<<i<<": "<<argv[i]<<std::endl;
		}*/
		cLoopFunctions.AssignRules(argv[5]);
		//std::cout<<"Main: Assigning Rules"<<std::endl;
	}
	else {
		LOGERR<<"There is a missing argument. Did you pass the rules?"<<std::endl;
		//std::cout<<"YOU DUN GOOFED"<<std::endl;
	}
   
   /*
    * Run n trials and take the worst performance as final value.
    * The performance here is average distance from the center of mass of the swarm.
    * Thus we take the greatest distance. Also calculate the degree of task specialisation.
    */
   float fCylinders = 0.0;
   float dots = 0.0;
   for(size_t i = 0; i < num_trials; ++i) {
	  //argos::LOG<<"Trial #"<<i<<std::endl;
      /* Tell the loop functions to get ready for the i-th trial */
      cLoopFunctions.SetTrial(i);
      //std::cout<<"Fails in reset"<<std::endl;
      /* Reset the experiment. */
      cSimulator.Reset();
      /* Re-assigning rules */
      cLoopFunctions.AssignRules(argv[5]);
      //std::cout<<"Fails in execute"<<std::endl;
      /* Run the experiment */
      cSimulator.Execute();
      //std::cout<<"Fails in max"<<std::endl;
      /* Update performance */
      fCylinders += float(cLoopFunctions.Performance());
      dots += float(cLoopFunctions.DegreeOfTaskSpecialisation());
   }
   
   fCylinders /= cLoopFunctions.GetNumTrials();
   dots /= cLoopFunctions.GetNumTrials();
   
   //std::fstream performanceFile;
   //performanceFile.open("aggregation_performance.txt", std::fstream::out);
   //performanceFile<<fDistance;
   //performanceFile.close();

   //std::cout<<"Fails in destroy"<<std::endl;
   /*
    * Dispose of ARGoS stuff
    */
   cSimulator.Destroy();
   
   std::cout<<"PERFORMANCE,"<<fCylinders<<",PERFORMANCE"<<std::endl;
   std::cout<<"DOTS,"<<dots<<",DOTS"<<std::endl;

   /* All is OK */
   //std::cout<<"So far so good"<<std::endl;
   return 0;
}
