/*
 * This is a multi-process genetic algorithm for evolving a foraging
 * controller
 */

#include <iostream>
#include <fstream>
#include <loop_functions/mpga_loop_functions/mpga.h>
#include <loop_functions/mpga_loop_functions/mpga_foraging_loop_functions.h>

#include <sys/stat.h>

std::vector<std::pair<Real,Real>> fitness_log;

/*
 * Flush best individual
 */
void FlushIndividual(const CMPGA::SIndividual& s_ind,
                     UInt32 un_generation, 
                     const char* seed) {
   
   std::string parent_folder_name = "results";
   int dir_err = mkdir(parent_folder_name.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH); // Create folder for best logs
   std::string folder_prefix = "results/BEST_LOGS_";
   std::string folder_name = folder_prefix + seed;
   dir_err = mkdir(folder_name.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH); // Create folder for best logs
   std::ostringstream cOSS;
   std::string dat_file_prefix = "/foraging_best_";
   std::string dat_name = folder_name + dat_file_prefix;
   cOSS << dat_name << un_generation << ".dat";
   
   std::ofstream cOFS(cOSS.str().c_str(), std::ios::out | std::ios::trunc);
   /* First write the number of values to dump */
   cOFS << GENOME_SIZE;
   /* Then dump the genome */
   for(UInt32 i = 0; i < GENOME_SIZE; ++i) {
      cOFS << " " << s_ind.Genome[i];
   }
   /* End line */
   cOFS << std::endl;
}

void FlushFitness(const CMPGA::SIndividual& s_ind,
                     UInt32 un_generation,
                     Real avgScore,
                     bool lastRun, 
                     const char* seed) {
  if(lastRun) {
    std::ostringstream cOSS;
    std::string folder_prefix = "results/BEST_LOGS_";
    std::string folder_suffix = "/foraging_stats.tsv";
    std::string filename = folder_prefix + seed + folder_suffix;
    cOSS << filename;
    std::ofstream cOFS(cOSS.str().c_str(), std::ios::out | std::ios::trunc);
    cOFS <<"best_fitness"<<"\t"<<"avg_fitness"<<std::endl;

    for(int i=0; i<fitness_log.size();i++)
      cOFS <<std::get<0>(fitness_log[i])<<"\t"<<std::get<1>(fitness_log[i])<<std::endl;

  }

  else {
    std::pair <Real,Real> score_pair(s_ind.Score,avgScore);
    fitness_log.push_back(score_pair);
  }
}

/*
 * The function used to aggregate the scores of each trial.  In this
 * experiment, the score is the number of cylinders gathered. We take the average
 */
Real ScoreAggregator(const std::vector<Real>& vec_scores) {
   Real fScore = vec_scores[0];
   for(size_t i = 1; i < vec_scores.size(); ++i) {
      //fScore = Max(fScore, vec_scores[i]);
      fScore += vec_scores[i];
   }
   fScore /= vec_scores.size();
   return fScore;
}

int main(int argc, char** argv) {
   /* 
   	Run the command with arguments in the following format and order:

   	build/embedding/mpga/mpga_foraging POP_SIZE MUTATION_PROB NUM_TRIALS NUM_GEN SEED
   	*/

   if(argc != 6)
   	LOGERR<<"You passed the wrong number of arguments. Arguments are: POP_SIZE MUTATION_PROB NUM_TRIALS NUM_GEN SEED"<<std::endl;

   //std::size_t pos;

   int POP_SIZE = std::stoi(argv[1], nullptr);
   int MUTATION_PROB = std::stof(argv[2], nullptr);
   int NUM_TRIALS = std::stoi(argv[3], nullptr);
   int NUM_GEN = std::stoi(argv[4], nullptr);
   int SEED = std::stoi(argv[5], nullptr);

   CMPGA cGA(CRange<Real>(-5.0,5.0),            // Allele range
             GENOME_SIZE,                         // Genome size
             POP_SIZE,//100,//                                   // Population size
             MUTATION_PROB,//0.05,                                // Mutation probability
             NUM_TRIALS,//5,//                                   // Number of trials
             NUM_GEN,//100,//                                 // Number of generations
             true,                               // Maximise score
             "experiments/mpga_foraging.argos",            // .argos conf file
             &ScoreAggregator,                    // The score aggregator
             SEED//12345//                                // Random seed
      );
   cGA.Evaluate();
   //argos::LOG << "Generation #" << cGA.GetGeneration() << "...";
   //argos::LOG << " scores:";
   //for(UInt32 i = 0; i < cGA.GetPopulation().size(); ++i) {
   //   argos::LOG << " " << cGA.GetPopulation()[i]->Score;
  // }
   //LOG << std::endl;
   //LOG.Flush();
   fitness_log.reserve(NUM_GEN);

   bool lastRun = false;
   Real avgScore = 0;
   for(UInt32 i = 0; i < cGA.GetPopulation().size(); ++i) {
    avgScore += cGA.GetPopulation()[i]->Score;
    }
  avgScore /= cGA.GetPopulation().size();

   FlushFitness(*cGA.GetPopulation()[0],
                    cGA.GetGeneration(),
                    avgScore,
                    lastRun,
                    argv[5]);
   
   while(!cGA.Done()) {
      cGA.NextGen();
      cGA.Evaluate();
      //argos::LOG << "Generation #" << cGA.GetGeneration() << "...";
      //argos::LOG << " scores:";
      
      Real avgScore = 0;
      for(UInt32 i = 0; i < cGA.GetPopulation().size(); ++i) {
         //argos::LOG << " " << cGA.GetPopulation()[i]->Score;
         avgScore += cGA.GetPopulation()[i]->Score;
      }
      avgScore /= cGA.GetPopulation().size();
      
      if(cGA.GetGeneration() % 5 == 0) {
         //argos::LOG << " [Flushing genome... ";
         /* Flush scores of best individual */
      FlushIndividual(*cGA.GetPopulation()[0],
                      cGA.GetGeneration(),
                      argv[5]);
         //argos::LOG << "done.]";
      }

      if(cGA.GetGeneration() == NUM_GEN)
        lastRun = true;

      FlushFitness(*cGA.GetPopulation()[0],
                    cGA.GetGeneration(),
                    avgScore,
                    lastRun,
                    argv[5]);

      //LOG << std::endl;
      //LOG.Flush();
   }
   return 0;
}
