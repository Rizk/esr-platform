<?xml version="1.0" ?>

<!-- *************************************************** -->
<!-- * A fully commented XML is diffusion_1.xml. Refer * -->
<!-- * to it to have full information about what       * -->
<!-- * these options mean.                             * -->
<!-- *************************************************** -->

<argos-configuration>

  <!-- ************************* -->
  <!-- * General configuration * -->
  <!-- ************************* -->
  <framework>
    <system threads="0" />
    <!-- Each experimental run is 120 seconds long -->
    <experiment length="200"
                ticks_per_second="10"
                random_seed="1" />
  </framework>

  <!-- *************** -->
  <!-- * Controllers * -->
  <!-- *************** -->
  <controllers>

    <footbot_ge_aggregation_controller id="fgea"
                           library="build/controllers/footbot_ge_aggregation/libfootbot_ge_aggregation">
      <actuators>
        <differential_steering implementation="default" />
        <range_and_bearing implementation="default" />
      </actuators>
      <sensors>
        <footbot_proximity implementation="default"    show_rays="false" />
        <range_and_bearing implementation="medium" medium="rab" show_rays="true" />
        <differential_steering implementation="default" />
      </sensors>
      <params>
        <wheel_turning hard_turn_angle_threshold="90"
                       soft_turn_angle_threshold="70"
                       no_turn_angle_threshold="10"
                       max_speed="10" />
        <flocking target_distance="50"
                  gain="1000"
                  exponent="2" />
      </params>
    </footbot_ge_aggregation_controller>

  </controllers>

  <!-- ****************** -->
  <!-- * Loop functions * -->
  <!-- ****************** -->
  <loop_functions library="build/loop_functions/ge_aggregation_loop_functions/libge_aggregation_loop_functions"
                  label="ge_aggregation_loop_functions" />

  <!-- *********************** -->
  <!-- * Arena configuration * -->
  <!-- *********************** -->
  <arena size="10, 10, 10" center="0,0,5">

    <floor id="floor"
           source="loop_functions"
           pixels_per_meter="50" />
           
    <!--
        Here we just put the static elements of the environment (the walls
        and the light).
        The dynamic ones, in this case the foot-bot, are placed by the
        loop functions at the beginning of each experimental run.
    -->

    <box id="wall_north" size="5,0.1,1" movable="false">
      <body position="0,2.5,0" orientation="0,0,0"  />
    </box>

    <box id="wall_south" size="5,0.1,1" movable="false">
      <body position="0,-2.5,0" orientation="0,0,0" />
    </box>

    <box id="wall_east" size="0.1,5,1" movable="false">
      <body position="2.5,0,0" orientation="0,0,0" />
    </box>

    <box id="wall_west" size="0.1,5,1" movable="false">
      <body position="-2.5,0,0" orientation="0,0,0" />
    </box>

  </arena>

  <!-- ******************* -->
  <!-- * Physics engines * -->
  <!-- ******************* -->
  <physics_engines>
    <dynamics2d id="dyn2d" />
  </physics_engines>

  <!-- ********* -->
  <!-- * Media * -->
  <!-- ********* -->
  <media>
    <range_and_bearing id="rab" />
  </media>

  <!-- ****************** -->
  <!-- * Visualization * -->
  <!-- ****************** -->
  <visualization>
    <qt-opengl>
      <camera>
        <placement idx="0" position="0,0,5" look_at="0,0,0" lens_focal_length="20" up="0,1,0" />
      </camera>
    </qt-opengl>
  </visualization> 

</argos-configuration>
