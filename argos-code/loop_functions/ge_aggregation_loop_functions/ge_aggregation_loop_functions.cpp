#include "ge_aggregation_loop_functions.h"

/****************************************/
/****************************************/

GEAggregationLoopFunctions::GEAggregationLoopFunctions() :
   m_vecInitSetup(NUM_TRIALS, std::vector<SInitSetup>(NUM_FOOTBOTS)),
   m_pcFootBots(NUM_FOOTBOTS),
   m_pcControllers(NUM_FOOTBOTS, NULL),
   m_pcRNG(NULL),
   m_unCurrentTrial(0) {}
   
/****************************************/
/****************************************/

GEAggregationLoopFunctions::~GEAggregationLoopFunctions() {
}

/****************************************/
/****************************************/

void GEAggregationLoopFunctions::Init(TConfigurationNode& t_node) {
   LOG<<"Random Seed: "<<GetSimulator().GetInstance().GetRandomSeed()<<std::endl;
   //GetSimulator().GetInstance().SetRandomSeed(3);
   //LOG<<"Random Seed: "<<GetSimulator().GetInstance().GetRandomSeed()<<std::endl;
   
   /*
   TConfigurationNode& t_root = GetSimulator().GetConfigurationRoot();
   TConfigurationNode& t_exp = GetNode(GetNode(t_root, "framework"),"experiment");
   SetNodeAttribute(t_exp,"random_seed",2);
   */
   
   /*
    * Create the random number generator
    */
   m_pcRNG = CRandom::CreateRNG("argos");

   /*
    * Create the foot-bots and get references to their controllers
    */
   for(int i=0; i<NUM_FOOTBOTS; i++){
		m_pcFootBots[i] = new CFootBotEntity(
      "fb"+std::to_string(i),    // entity id
      "fgea",    // controller id as set in the XML
      CVector3(),CQuaternion(), COMMUNICATION_RANGE// RAB range
      );
      AddEntity(*m_pcFootBots[i]);
      m_pcControllers[i] = &dynamic_cast<CFootBotGEAggregationController&>(m_pcFootBots[i]->GetControllableEntity().GetController());
	}
   
   for(int i=0; i<NUM_TRIALS; i++) 
		for(int j=0; j<NUM_FOOTBOTS; j++) 
			m_vecInitSetup[i][j] = GenerateEntitySetup("ROBOT");
   
   for(int i=0; i<NUM_FOOTBOTS; i++)
		PlaceEntity("ROBOT", m_pcFootBots[i]->GetEmbodiedEntity(), i);

}

void GEAggregationLoopFunctions::PostExperiment() {
	//LOG<<"Performing post-experiment logging"<<std::endl;
	
	/*
	if(m_unCurrentTrial <= NUM_TRIALS) {
		LOG<<"Trial: "<<m_unCurrentTrial<<std::endl;
		//GetSimulator().GetInstance().Reset(GetSimulator().GetRandomSeed()+1);
		GetSimulator().SetRandomSeed(GetSimulator().GetRandomSeed()+1);
		Reset();
		LOG<<GetSimulator().GetRandomSeed()<<std::endl;
		m_unCurrentTrial++;
		LOG<<m_unCurrentTrial<<std::endl;
		LOG<<"---------"<<std::endl;
	}*/
	//LOG<<"Trial # :"<<m_unCurrentTrial<<std::endl;
	
}

/****************************************/
/****************************************/

GEAggregationLoopFunctions::SInitSetup GEAggregationLoopFunctions::GenerateEntitySetup(std::string entityType) {
	
	float_t x_start, x_end, y_start, y_end;
	CRadians cOrient;
	
	x_start = ARENA_START_X;
	x_end = ARENA_START_X+ARENA_WIDTH;
	
	if(entityType == "ROBOT"){
		
		y_start = ARENA_START_Y;
		y_end = ARENA_START_Y+ARENA_LENGTH;
		
		cOrient = m_pcRNG->Uniform(CRadians::UNSIGNED_RANGE);
	}
	
	SInitSetup entitySetup;
	entitySetup.Position = CVector3(m_pcRNG->Uniform(CRange<Real>(x_start, x_end)), 
												m_pcRNG->Uniform(CRange<Real>(y_start, y_end)), 
												0.0);
	
	entitySetup.Orientation.FromEulerAngles(
         cOrient,        // rotation around Z
         CRadians::ZERO, // rotation around Y
         CRadians::ZERO  // rotation around X
         );
    
    return entitySetup;
}

void GEAggregationLoopFunctions::PlaceEntity(std::string entityType, CEmbodiedEntity& entityToPlace, int robotID) {

	/* Place entity in the arena. If placement is not possible, try a different random setup */
   SInitSetup entitySetup;
   int numAttempts = 0;
   bool entityPlaced = false;
   
   while(numAttempts < ENTITY_PLACEMENT_ATTEMPTS && !entityPlaced) {
	   entitySetup = m_vecInitSetup[m_unCurrentTrial][robotID];
	   
	   if(!MoveEntity(
			entityToPlace,
			entitySetup.Position,    // to this position
			entitySetup.Orientation, // with this orientation
			false
			)) {
				m_vecInitSetup[m_unCurrentTrial][robotID] = GenerateEntitySetup(entityType);
				numAttempts++;
			}
		else {
			entityPlaced = true;
			}
	}
	   
	if(numAttempts >= ENTITY_PLACEMENT_ATTEMPTS){
		LOGERR << "Can't move entity in <"
					 << entitySetup.Position
					 << ">, <"
					 << entitySetup.Orientation
					 << ">"
					 << std::endl;
	}

}

CColor GEAggregationLoopFunctions::GetFloorColor(const CVector2& c_position_on_plane) {
   /* Main arena */
   if(c_position_on_plane.GetX() >= ARENA_START_X &&
		c_position_on_plane.GetX() < ARENA_START_X+ARENA_WIDTH &&
		c_position_on_plane.GetY() >= ARENA_START_Y &&
		c_position_on_plane.GetY() < ARENA_START_Y+ARENA_LENGTH) {
      return ARENA_COLOUR;
   }
   /* Rest of the environment*/
   else return OUTER_ARENA_COLOUR;
}

void GEAggregationLoopFunctions::Reset() {	
	//for(int i=0; i<NUM_FOOTBOTS;i++)
	//	LOG<<"Trial "<<m_unCurrentTrial<<": "<<m_vecInitSetup[m_unCurrentTrial][i].Position<<std::endl;
	
	/*
    * Move robot to the initial position corresponding to the current trial
    */
   for(int i=0; i<NUM_FOOTBOTS; i++)
		PlaceEntity("ROBOT", m_pcFootBots[i]->GetEmbodiedEntity(), i);
}

/****************************************/
/****************************************/

Real GEAggregationLoopFunctions::Performance() {
   /* The performance is the average distance of all robots to the swarm's center of mass at the end of the trial */
   
  CVector2 tCenterOfMass = CVector2();
  Real tAverageDistance = 0.0f;
  
  for(int i=0; i<NUM_FOOTBOTS;i++) {
	  CVector2 robotPosition;
	  m_pcFootBots[i]->GetEmbodiedEntity().GetOriginAnchor().Position.ProjectOntoXY(robotPosition);
	  tCenterOfMass += robotPosition;
	  //LOG<<"Robot Position: "<<robotPosition<<std::endl;
  }
  
  tCenterOfMass /= NUM_FOOTBOTS;
  //LOG<<"Center of Mass: "<<tCenterOfMass<<std::endl;
  
  for(int i=0; i<NUM_FOOTBOTS;i++) {
	  CVector2 robotPosition;
	  m_pcFootBots[i]->GetEmbodiedEntity().GetOriginAnchor().Position.ProjectOntoXY(robotPosition);
	  Real tNormalisedDistance = Distance(tCenterOfMass, robotPosition) / COMMUNICATION_RANGE;
	  tAverageDistance += Abs(1.0f - tNormalisedDistance );
	  
	  //LOG<<"Distance: "<<Distance(tCenterOfMass, robotPosition)<<std::endl;
  }
  
  tAverageDistance /= NUM_FOOTBOTS;
  
  //LOG<<tAverageDistance<<std::endl;
   
   return tAverageDistance;
}

size_t GEAggregationLoopFunctions::GetNumTrials() {
	return NUM_TRIALS;
}

void GEAggregationLoopFunctions::AssignRules(std::string rulesString) {
	
	//std::cout<<"Loop Functions: Assigning Rules"<<std::endl;
	
	for(int i=0; i<NUM_FOOTBOTS; i++) {
		m_pcControllers[i]->SetRulesFromInput(rulesString);
	}
}

/****************************************/
/****************************************/

REGISTER_LOOP_FUNCTIONS(GEAggregationLoopFunctions, "ge_aggregation_loop_functions")
