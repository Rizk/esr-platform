#ifndef GE_AGGREGATION_LOOP_FUNCTIONS_H
#define GE_AGGREGATION_LOOP_FUNCTIONS_H

/* ARGoS-related headers */
#include <argos3/core/simulator/loop_functions.h>
#include <argos3/core/utility/math/rng.h>
#include <argos3/plugins/robots/foot-bot/simulator/footbot_entity.h>

#include <argos3/core/simulator/entity/floor_entity.h>

#include <controllers/footbot_ge_aggregation/footbot_ge_aggregation.h>
/****************************************/
/****************************************/


static const size_t NUM_FOOTBOTS = 5;
static const size_t NUM_TRIALS = 12;
static const size_t ENTITY_PLACEMENT_ATTEMPTS = 100;
static const float_t COMMUNICATION_RANGE = 1.0f;

static const float_t ARENA_START_X = -2.5f;
static const float_t ARENA_START_Y = -2.5f;
static const float_t ARENA_WIDTH = 5.0f;
static const float_t ARENA_LENGTH = 5.0f;

static const CColor ARENA_COLOUR = CColor::GRAY50;
static const CColor OUTER_ARENA_COLOUR = CColor::WHITE;


/****************************************/
/****************************************/

using namespace argos;

class GEAggregationLoopFunctions : public CLoopFunctions {

public:

   GEAggregationLoopFunctions();
   virtual ~GEAggregationLoopFunctions();

   virtual void Init(TConfigurationNode& t_node);
   virtual void PostExperiment();
   virtual CColor GetFloorColor(const CVector2& c_position_on_plane);
   
   virtual void Reset();
   

   /* Returns the performance of the robots in the current trial */
   Real Performance();
   
   size_t GetNumTrials();
   
   inline void SetTrial(size_t un_trial) {
      m_unCurrentTrial = un_trial;
   }
   
   void AssignRules(std::string rulesString);

private:

   /* The initial setup of a trial */
   struct SInitSetup {
      CVector3 Position;
      CQuaternion Orientation;
   };

   SInitSetup GenerateEntitySetup(std::string entityType);
   void PlaceEntity(std::string entityType, CEmbodiedEntity& entityToPlace, int robotID);
   
   std::vector<std::vector<SInitSetup>> m_vecInitSetup;
   std::vector<CFootBotEntity*> m_pcFootBots;
   std::vector<CFootBotGEAggregationController*> m_pcControllers;
   CRandom::CRNG* m_pcRNG;
   
   UInt32 m_unCurrentTrial;

};

#endif
