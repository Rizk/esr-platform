#include "ge_foraging_loop_functions.h"

/* File writing */
#include <fstream>

/****************************************/
/****************************************/

GEForagingLoopFunctions::GEForagingLoopFunctions() :
   m_vecRobotInitSetup(NUM_TRIALS, std::vector<SInitSetup>(NUM_FOOTBOTS)),
   m_vecCylinderInitSetup(NUM_TRIALS, std::vector<SInitSetup>(NUM_CYLINDERS)),
   m_pcFootBots(NUM_FOOTBOTS),
   m_pcCylinders(NUM_CYLINDERS, NULL),
   m_pcControllers(NUM_FOOTBOTS, NULL),
   m_pcRNG(NULL),
   m_CylinderWithRobot(NUM_FOOTBOTS,-1),
   m_CylinderDropSpots(NUM_CYLINDERS,CVector3()),
   m_DropCount(0),
   m_unCurrentTrial(0),
   m_time_steps(0) {}
   
//,m_distanceTravelledByRobot(NUM_FOOTBOTS)
/****************************************/
/****************************************/

GEForagingLoopFunctions::~GEForagingLoopFunctions() {
}

/****************************************/
/****************************************/

void GEForagingLoopFunctions::Init(TConfigurationNode& t_node) {
   /*
    * Create the random number generator
    */
   argos::LOG<<"Starting"<<std::endl;
   m_pcRNG = CRandom::CreateRNG("argos");

   behaviourCharacterisation.reserve(BCV_LENGTH);

   /*
    * Create the foot-bots and get references to their controllers
    */
    
    /* */
   for(int i=0; i<NUM_FOOTBOTS; i++){
		m_pcFootBots[i] = new CFootBotEntity(
      "fb"+std::to_string(i),    // entity id
      "fgef",    // controller id as set in the XML
      CVector3(),CQuaternion(), COMMUNICATION_RANGE// RAB range
      );
      AddEntity(*m_pcFootBots[i]);
      m_pcControllers[i] = &dynamic_cast<CFootBotGEForagingController&>(m_pcFootBots[i]->GetControllableEntity().GetController());
	}
	
	Reset();
	
}

void GEForagingLoopFunctions::PreStep() {
	//argos::LOG<<"***Before the control step***"<<std::endl;
	//argos::LOG<<"***After the control step***"<<std::endl;
	//LOG<<"Start of next trial"<<std::endl;
}

void GEForagingLoopFunctions::PostStep() {
	/* */
	for(int i=0; i<NUM_FOOTBOTS; i++){
		CVector2 robotPosition;
		m_pcFootBots[i]->GetEmbodiedEntity().GetOriginAnchor().Position.ProjectOntoXY(robotPosition);
		
		// Let robot pick up any cylinder that is nearby, remove it from the environment and spawn a replacement 
		if(m_pcControllers[i]->CheckWantObject() && !m_pcControllers[i]->CheckHasObject() &&  !m_pcControllers[i]->CheckDropObject()){
			int currentCylindersSize = m_pcCylinders.size();
			int currentNumCylinders = 0;
			
			for(int k=0; k<m_pcCylinders.size(); k++){
				if(m_pcCylinders[k] != NULL)
					currentNumCylinders++;
			}
			
			for(int j=0; j<m_pcCylinders.size(); j++){
				
				if(m_pcCylinders[j] == NULL)
					continue;
				
				CVector2 cylinderPosition;
				m_pcCylinders[j]->GetEmbodiedEntity().GetOriginAnchor().Position.ProjectOntoXY(cylinderPosition);
				
				if(Abs(Distance(robotPosition, cylinderPosition)) < CYLINDER_PICKUP_DISTANCE){ //if nearby
					
					RemoveEntity(*m_pcCylinders[j]);//remove cylinder
					m_pcCylinders[j] = NULL;
					currentNumCylinders--;
					 
					m_pcControllers[i]->SetHasObjectFlag(true);
					m_CylinderWithRobot[i] = j;
					
					//spawn new cylinder in environment and add to vector
					
					//if(currentNumCylinders >= m_pcCylinders.capacity())
					//	m_pcCylinders.reserve(currentNumCylinders*3);
					
					if(currentNumCylinders < NUM_CYLINDERS) {
						m_pcCylinders.push_back(CreateCylinder(currentCylindersSize));
						AddEntity(*m_pcCylinders[currentCylindersSize]);
						SpawnNewCylinder(currentCylindersSize, m_pcCylinders[currentCylindersSize]->GetEmbodiedEntity());
						currentNumCylinders++;
						//argos::LOG<<"Cylinder picked up"<<std::endl;
						break;
					}
				}
				
			}
			
		}
		
		// Let robot drop cylinder. If cylinder is in nest area remove it from the environment, else make it reappear
		if(m_pcControllers[i]->CheckHasObject() &&  m_pcControllers[i]->CheckDropObject()){
			//LOG<<"Cylinder Dropped"<<std::endl;
			CVector2 robotPosition;
			m_pcFootBots[i]->GetEmbodiedEntity().GetOriginAnchor().Position.ProjectOntoXY(robotPosition);
			
			CCylinderEntity* cylinder = CreateCylinder(m_CylinderWithRobot[i]);//Create new cylinder with old id
			m_pcCylinders[m_CylinderWithRobot[i]] = cylinder;//Put new cylinder in cylinder vector
			AddEntity(*cylinder);//Add entity to environment
			CEmbodiedEntity& cylinderEntity = cylinder->GetEmbodiedEntity();
			
			DropCylinder(cylinderEntity, robotPosition);//Place cylinder near robot
			CVector3 cylinderPosition = cylinderEntity.GetOriginAnchor().Position;
			
			//Foot-bot no longer has cylinder
			m_pcControllers[i]->SetHasObjectFlag(false);
			int cylinderID = m_CylinderWithRobot[i];
			m_CylinderWithRobot[i] = -1;
			

			if(IsCylinderOnNest(cylinderEntity)){//if cylinder is on nest
				//remove cylinder from environment and vector
				RemoveEntity(*cylinder);
				m_pcCylinders[cylinderID] = NULL;
				m_pcControllers[i]->IncrementCylindersGathered();
				//LOG<<"Cylinder delivered to nest"<<std::endl;
			}
		}

		
		//LOG<<i<<": "<<m_pcFootBots[i]->GetEmbodiedEntity().GetOriginAnchor().Orientation<<std::endl;
	} 
	
	//argos::LOG<<"---Ending footbot loop---"<<std::endl;

	if(log_BC == true) {
		if(m_time_steps%5 == 0 && m_time_steps != 0){
			LogBehaviourCharacterisation();
		}
		m_time_steps++;
	}
	
}

void GEForagingLoopFunctions::PostExperiment() {
	
	/* 
	for(int j=0; j<NUM_CYLINDERS; j++){
		argos::LOG<<"Cylinder "<<j<<" at trial "<<m_unCurrentTrial<<": "<<m_vecCylinderInitSetup[m_unCurrentTrial][j].Position<<std::endl;
	}
	*/
	
	//LOG<<GetTrial()<<std::endl;
	//LOG<<"Reached the end of the trial"<<std::endl;

	if(log_BC == true)
		LogBCFile();
	if(log_performance == true)
		std::cout<<"PERFORMANCE,"<<Performance()<<",PERFORMANCE"<<std::endl;
}

/****************************************/
/****************************************/

GEForagingLoopFunctions::SInitSetup GEForagingLoopFunctions::GenerateEntitySetup(std::string entityType) {
	
	float_t x_start, x_end, y_start, y_end;
	CRadians cOrient;
	
	x_start = ARENA_START_X;
	x_end = ARENA_START_X+ARENA_WIDTH;
	
	if(entityType == "ROBOT"){
		y_start = ARENA_START_Y;
		y_end = ARENA_START_Y+NEST_LENGTH;//+CACHE_LENGTH;
		
		cOrient = m_pcRNG->Uniform(CRadians::UNSIGNED_RANGE);
	}
	else if(entityType == "CYLINDER"){
		y_start = ARENA_START_Y+ARENA_LENGTH-SOURCE_LENGTH;
		y_end = ARENA_START_Y+ARENA_LENGTH;
		
		cOrient = CRadians::ZERO;
	}
	
	SInitSetup entitySetup;
	entitySetup.Position = CVector3(m_pcRNG->Uniform(CRange<Real>(x_start, x_end)), 
												m_pcRNG->Uniform(CRange<Real>(y_start, y_end)), 
												0.0);
	
	entitySetup.Orientation.FromEulerAngles(
         cOrient,        // rotation around Z
         CRadians::ZERO, // rotation around Y
         CRadians::ZERO  // rotation around X
         );
    
    return entitySetup;
}

void GEForagingLoopFunctions::PlaceEntity(std::string entityType, CEmbodiedEntity& entityToPlace, int entityID) {
	/* Place entity in the arena. If placement is not possible, try a different random setup  */
   SInitSetup entitySetup;
   int numAttempts = 0;
   bool entityPlaced = false;
   
   while(numAttempts < ENTITY_PLACEMENT_ATTEMPTS && !entityPlaced) {
	   if(entityType == "ROBOT")
			entitySetup = m_vecRobotInitSetup[m_unCurrentTrial][entityID];
		else if(entityType == "CYLINDER")
			entitySetup = m_vecCylinderInitSetup[m_unCurrentTrial][entityID];
	   
	   if(!MoveEntity(
			entityToPlace,
			entitySetup.Position,    // to this position
			entitySetup.Orientation, // with this orientation
			false
			)) {
				if(entityType == "ROBOT")
					m_vecRobotInitSetup[m_unCurrentTrial][entityID] = GenerateEntitySetup(entityType);
				else if(entityType == "CYLINDER")
					m_vecCylinderInitSetup[m_unCurrentTrial][entityID] = GenerateEntitySetup(entityType);
				numAttempts++;
			}
		else {
			entityPlaced = true;
			}
	}
	   
	if(numAttempts >= ENTITY_PLACEMENT_ATTEMPTS){
		LOGERR << "Can't move entity in <"
					 << entitySetup.Position
					 << ">, <"
					 << entitySetup.Orientation
					 << ">"
					 << std::endl;
	}

}

CCylinderEntity* GEForagingLoopFunctions::CreateCylinder(int cylinderID){
	
	return new CCylinderEntity("c"+std::to_string(cylinderID),	//Cylinder ID
						  CVector3(),	//Cylinder position
						  CQuaternion(),//Cylinder orientation
						  true,	//Cylinder is movable
						  CYLINDER_RADIUS, //Radius
						  CYLINDER_HEIGHT,	//Height
						  CYLINDER_MASS);	//Mass
}

GEForagingLoopFunctions::SInitSetup GEForagingLoopFunctions::GenerateCylinderSetupNearRobot(CVector2 robotPosition){
	SInitSetup entitySetup;
	entitySetup.Position = CVector3(m_pcRNG->Uniform(CRange<Real>(robotPosition.GetX() - 0.2f, robotPosition.GetX() + 0.2f)), 
												m_pcRNG->Uniform(CRange<Real>(robotPosition.GetY() - 0.2f, robotPosition.GetY() + 0.2f)), 
												0.0);
	
	entitySetup.Orientation.FromEulerAngles(
         CRadians::ZERO,        // rotation around Z
         CRadians::ZERO, // rotation around Y
         CRadians::ZERO  // rotation around X
         );
    
    return entitySetup;
}

CColor GEForagingLoopFunctions::GetFloorColor(const CVector2& c_position_on_plane) {
   /* Nest*/
   if(c_position_on_plane.GetX() >= ARENA_START_X && 
		c_position_on_plane.GetX() < (ARENA_START_X+ARENA_WIDTH) &&
		c_position_on_plane.GetY() >= ARENA_START_Y &&
		c_position_on_plane.GetY() < ARENA_START_Y+NEST_LENGTH) {
      return NEST_COLOUR;
   }
   /* Cache */
   else if(c_position_on_plane.GetX() >= ARENA_START_X && 
		c_position_on_plane.GetX() < (ARENA_START_X+ARENA_WIDTH) &&
		c_position_on_plane.GetY() >= (ARENA_START_Y+NEST_LENGTH) &&
		c_position_on_plane.GetY() < (ARENA_START_Y+NEST_LENGTH+CACHE_LENGTH)) {
      return CACHE_COLOUR;
   }
   /* No man's land */
   else if(c_position_on_plane.GetX() >= ARENA_START_X && 
		c_position_on_plane.GetX() < (ARENA_START_X+ARENA_WIDTH) &&
		c_position_on_plane.GetY() >= (ARENA_START_Y+NEST_LENGTH+CACHE_LENGTH) &&
		c_position_on_plane.GetY() < (ARENA_LENGTH-SOURCE_LENGTH)) {
      return NO_MANS_COLOUR;
   }
   /* Source*/
   else if(c_position_on_plane.GetX() >= ARENA_START_X && 
		c_position_on_plane.GetX() < (ARENA_START_X+ARENA_WIDTH) &&
		c_position_on_plane.GetY() >= (ARENA_LENGTH-SOURCE_LENGTH) &&
		c_position_on_plane.GetY() < (ARENA_START_Y+ARENA_LENGTH)) {
      return SOURCE_COLOUR;
   }
   /* Rest of the environment*/
   else return OUTER_ARENA_COLOUR;
}

void GEForagingLoopFunctions::Reset() {	
	
	m_pcCylinders.reserve(NUM_CYLINDERS*3);
	m_CylinderDropSpots.reserve(NUM_CYLINDERS*3);
	
	//Robots are not holding any cylinders
	for(int i=0; i<NUM_FOOTBOTS; i++)
		m_CylinderWithRobot[i] = -1;
	 
	//Remove all cylinders
	for(int i=0; i<m_pcCylinders.size(); i++){
		if(m_pcCylinders[i] != NULL) {
			RemoveEntity(*m_pcCylinders[i]);
			m_pcCylinders[i] = NULL;
		}
	 }
	 
	m_pcCylinders.resize(NUM_CYLINDERS);  
	
	//Add original cylinders
	 for(int i=0; i<NUM_CYLINDERS; i++){
		m_pcCylinders[i] = CreateCylinder(i);
		AddEntity(*m_pcCylinders[i]);
	 }
	
	for(int i=0; i<NUM_TRIALS; i++) {
		for(int j=0; j<NUM_FOOTBOTS; j++) 
			m_vecRobotInitSetup[i][j] = GenerateEntitySetup("ROBOT");
		
		for(int j=0; j<NUM_CYLINDERS; j++) {
			m_vecCylinderInitSetup[i][j] = GenerateEntitySetup("CYLINDER");
			//argos::LOG<<"Cylinder "<<j<<" at trial "<<i<<": "<<m_vecCylinderInitSetup[i][j].Position<<std::endl;
		}
	}
	
	/*
    * Move robots and cylinders to the initial position corresponding to the current trial
    */
	for(int i=0; i<NUM_FOOTBOTS; i++)
		PlaceEntity("ROBOT", m_pcFootBots[i]->GetEmbodiedEntity(), i);
	
	for(int i=0; i<NUM_CYLINDERS; i++)
		PlaceEntity("CYLINDER", m_pcCylinders[i]->GetEmbodiedEntity(), i);
	
	for(int i=0; i<NUM_TRIALS; i++){
		for(int j=0; j<NUM_CYLINDERS; j++){
			//argos::LOG<<"Cylinder ["<<i<<"]["<<j<<"] init is: "<<m_vecCylinderInitSetup[i][j].Position<<std::endl;
		}
	}
	
	m_DropCount = 0;
		
}

/****************************************/
/****************************************/

int GEForagingLoopFunctions::Performance() {
   /* The performance is the number of cylinders gathered by all robots during a trial */
   int numCylinders = 0;
   
   for(int i=0; i<NUM_FOOTBOTS; i++) {
		numCylinders += m_pcControllers[i]->GetCylindersGathered();
   }
   
   return numCylinders;
}

size_t GEForagingLoopFunctions::GetNumTrials() {
	return NUM_TRIALS;
}

UInt32 GEForagingLoopFunctions::GetTrial() {
      return m_unCurrentTrial;
}

void GEForagingLoopFunctions::AssignRules(std::string rulesString) {
	
	//std::cout<<"Loop Functions: Assigning Rules"<<std::endl;
	
	for(int i=0; i<NUM_FOOTBOTS; i++) {
		m_pcControllers[i]->SetRulesFromInput(rulesString);
	}
}

void GEForagingLoopFunctions::SpawnNewCylinder(int cylinderID, CEmbodiedEntity& cylinder){
   SInitSetup entitySetup;
   int numAttempts = 0;
   bool entityPlaced = false;
   
   while(numAttempts < ENTITY_PLACEMENT_ATTEMPTS && !entityPlaced) {
	   entitySetup = GenerateEntitySetup("CYLINDER");
	   
	   if(!MoveEntity(
			cylinder,
			entitySetup.Position,    // to this position
			entitySetup.Orientation, // with this orientation
			false
			)) {
				entitySetup = GenerateEntitySetup("CYLINDER");
				numAttempts++;
			}
		else {
			entityPlaced = true;
			}
	}
	   
	if(numAttempts >= ENTITY_PLACEMENT_ATTEMPTS){
		LOGERR << "Can't move entity in <"
					 << entitySetup.Position
					 << ">, <"
					 << entitySetup.Orientation
					 << ">"
					 << std::endl;
	}
}

void GEForagingLoopFunctions::DropCylinder(CEmbodiedEntity& cylinder, CVector2 robotPosition){
	SInitSetup entitySetup;
	int numAttempts = 0;
	bool entityPlaced = false;
   
	while(numAttempts < ENTITY_PLACEMENT_ATTEMPTS && !entityPlaced) {
	   entitySetup = GenerateCylinderSetupNearRobot(robotPosition);
	   
	   if(!MoveEntity(
			cylinder,
			entitySetup.Position,    // to this position
			entitySetup.Orientation, // with this orientation
			false
			)) {
				entitySetup = GenerateCylinderSetupNearRobot(robotPosition);
				numAttempts++;
			}
		else {
			entityPlaced = true;
			}
	}
	   
	if(numAttempts >= ENTITY_PLACEMENT_ATTEMPTS){
		LOGERR << "Can't move entity in <"
					 << entitySetup.Position
					 << ">, <"
					 << entitySetup.Orientation
					 << ">"
					 << std::endl;
	}
}
   
bool GEForagingLoopFunctions::IsCylinderOnNest(CEmbodiedEntity& cylinder){
	CVector2 cylinderPosition;
	cylinder.GetOriginAnchor().Position.ProjectOntoXY(cylinderPosition);
	
	if(cylinderPosition.GetX() >= ARENA_START_X && 
		cylinderPosition.GetX() < (ARENA_START_X+ARENA_WIDTH) &&
		cylinderPosition.GetY() >= ARENA_START_Y &&
		cylinderPosition.GetY() < ARENA_START_Y+NEST_LENGTH) {
      return true;
    }
    
    else
		return false;
}

void GEForagingLoopFunctions::LogBehaviourCharacterisation(){
	for(int i=0; i<NUM_FOOTBOTS; i++){
		CVector2 robotPosition;
		//CRadians robotOrientation,y,z;
		m_pcFootBots[i]->GetEmbodiedEntity().GetOriginAnchor().Position.ProjectOntoXY(robotPosition);
		//m_pcFootBots[i]->GetEmbodiedEntity().GetOriginAnchor().Orientation.ToEulerAngles(robotOrientation,y,z);
		behaviourCharacterisation.push_back(robotPosition.GetX());
		behaviourCharacterisation.push_back(robotPosition.GetY());
		//behaviourCharacterisation.push_back(robotOrientation.GetValue());
	}
}

void GEForagingLoopFunctions::LogBCFile(){
	std::ostringstream cOSS;
   cOSS << "BC_LOGS_GE.csv";
   std::ofstream cOFS(cOSS.str().c_str(), std::ios::out | std::ios::app);
   /* Then dump the genome */
   for(int i=0; i<behaviourCharacterisation.size(); i++){
      cOFS << behaviourCharacterisation[i] <<",";
   }
   /* End line */
   cOFS << std::endl;
}

/****************************************/
/****************************************/

REGISTER_LOOP_FUNCTIONS(GEForagingLoopFunctions, "ge_foraging_loop_functions")
