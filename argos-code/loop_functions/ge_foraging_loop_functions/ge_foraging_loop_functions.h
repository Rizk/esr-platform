#ifndef GE_FORAGING_LOOP_FUNCTIONS_H
#define GE_FORAGING_LOOP_FUNCTIONS_H

/* ARGoS-related headers */
#include <argos3/core/simulator/loop_functions.h>
#include <argos3/core/utility/math/rng.h>
#include <argos3/plugins/robots/foot-bot/simulator/footbot_entity.h>
#include <argos3/plugins/simulator/entities/cylinder_entity.h>

#include <argos3/core/simulator/entity/floor_entity.h>

#include <controllers/footbot_ge_foraging/footbot_ge_foraging.h>
/****************************************/
/****************************************/


static const size_t NUM_FOOTBOTS = 4;
static const size_t NUM_CYLINDERS = 5;
static const size_t NUM_TRIALS = 3;
static const size_t ENTITY_PLACEMENT_ATTEMPTS = 100;
static const float_t COMMUNICATION_RANGE = 1.0f;

static const float_t CYLINDER_RADIUS = 0.1f;
static const float_t CYLINDER_HEIGHT = 0.2f;
static const float_t CYLINDER_MASS = 0.1f;
static const float_t FOOTBOT_DIAMETER = 0.17f;
static const float_t CYLINDER_PICKUP_DISTANCE = FOOTBOT_DIAMETER + CYLINDER_RADIUS;//Same as foot-bot diameter

static const float_t ARENA_START_X = 4.125f;
static const float_t ARENA_START_Y = 0.0f;
static const float_t ARENA_WIDTH = 1.75f;
static const float_t ARENA_LENGTH = 9.75f;

static const float_t NEST_LENGTH = 0.5f;
static const float_t CACHE_LENGTH = 2.5f;
static const float_t SOURCE_LENGTH = 2.0f;

static const CColor NEST_COLOUR = CColor::WHITE;
static const CColor CACHE_COLOUR = CColor::GRAY50;
static const CColor NO_MANS_COLOUR = CColor::RED;
static const CColor SOURCE_COLOUR = CColor::BLACK;
static const CColor OUTER_ARENA_COLOUR = CColor::GRAY90;


/****************************************/
/****************************************/

using namespace argos;

class GEForagingLoopFunctions : public CLoopFunctions {

public:

   GEForagingLoopFunctions();
   virtual ~GEForagingLoopFunctions();

   virtual void Init(TConfigurationNode& t_node);
   virtual void PreStep();
   virtual void PostStep();
   virtual void PostExperiment();
   virtual CColor GetFloorColor(const CVector2& c_position_on_plane);
   
   virtual void Reset();

   /* Returns the performance of the robots in the current trial */
   int Performance();   
   
   size_t GetNumTrials();
   
   inline void SetTrial(size_t un_trial) {
      m_unCurrentTrial = un_trial;
   }
   
   UInt32 GetTrial();
   
   void AssignRules(std::string rulesString);
   
   void SpawnNewCylinder(int cylinderID, CEmbodiedEntity& cylinder);
   
   void DropCylinder(CEmbodiedEntity& cylinder, CVector2 robotPosition);
   
   bool IsCylinderOnNest(CEmbodiedEntity& cylinder);

   void LogBehaviourCharacterisation();

   void LogBCFile();

private:

   /* The initial setup of a trial */
   struct SInitSetup {
      CVector3 Position;
      CQuaternion Orientation;
   };

   SInitSetup GenerateEntitySetup(std::string entityType);
   void PlaceEntity(std::string entityType, CEmbodiedEntity& entityToPlace, int entityID);
   CCylinderEntity* CreateCylinder(int cylinderID);
   SInitSetup GenerateCylinderSetupNearRobot(CVector2 robotPosition);
   
   std::vector<std::vector<SInitSetup>> m_vecRobotInitSetup;
   std::vector<std::vector<SInitSetup>> m_vecCylinderInitSetup;
   std::vector<CFootBotEntity*> m_pcFootBots;
   std::vector<CCylinderEntity*> m_pcCylinders;
   std::vector<CFootBotGEForagingController*> m_pcControllers;
   Real* m_pfControllerParams;
   CRandom::CRNG* m_pcRNG;
   std::vector<int> m_CylinderWithRobot;//ID of cylinder each robot is carrying. -1 if none
   std::vector<CVector3> m_CylinderDropSpots;
   int m_DropCount;
   
   UInt32 m_unCurrentTrial;

   std::vector<Real> behaviourCharacterisation;
   int BCV_LENGTH = 12000;//x,y,theta * 4 robots * 1000 time intervals (every 5th time step)
   int m_time_steps;
   bool log_BC = false;
   bool log_performance = true;
   
   //std::vector<Real> m_distanceTravelledByRobot;

};

#endif
