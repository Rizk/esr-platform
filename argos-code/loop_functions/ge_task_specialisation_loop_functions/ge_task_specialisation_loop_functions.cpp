#include "ge_task_specialisation_loop_functions.h"

/* File writing */
#include <fstream>
#include <math.h>

/****************************************/
/****************************************/

GETaskSpecialisationLoopFunctions::GETaskSpecialisationLoopFunctions() :
   m_vecRobotInitSetup(3, std::vector<SInitSetup>(NUM_FOOTBOTS)),
   m_vecCylinderInitSetup(3, std::vector<SInitSetup>(NUM_CYLINDERS)),
   m_pcFootBots(NUM_FOOTBOTS),
   m_pcCylinders(NUM_CYLINDERS, NULL),
   m_pcCylinderPickupRecord(NUM_CYLINDERS, std::vector<int>(NUM_FOOTBOTS,0)),
   m_pcControllers(NUM_FOOTBOTS, NULL),
   m_pcRNG(NULL),
   m_CylinderWithRobot(NUM_FOOTBOTS,-1),
   m_CylinderDropSpots(NUM_CYLINDERS,CVector3()),
   m_DropCount(0),
   m_unCurrentTrial(0),
   m_originalMaxSpeed(0),
   m_time_steps(0),
   m_slopeAngle(CRadians::ZERO),
   m_numTrials(3) {}

int simulation_step_count = 0;

//,m_distanceTravelledByRobot(NUM_FOOTBOTS)
/****************************************/
/****************************************/

GETaskSpecialisationLoopFunctions::~GETaskSpecialisationLoopFunctions() {
}

/****************************************/
/****************************************/

void GETaskSpecialisationLoopFunctions::Init(TConfigurationNode& t_node) {
   /*
    * Create the random number generator
    */
   argos::LOG<<"Starting"<<std::endl;
   m_pcRNG = CRandom::CreateRNG("argos");

   behaviourCharacterisation.reserve(BCV_LENGTH);

   performanceLog.reserve(m_numTrials);

   swarmTrajectory.reserve(NUM_FOOTBOTS);
   std::vector<Real> robotTrajectory;
   //robotTrajectory.assign(TRAJECTORY_LENGTH,-1);
   robotTrajectory.reserve(TRAJECTORY_LENGTH);
   for(int i=0; i<NUM_FOOTBOTS; i++){
   		swarmTrajectory.push_back(robotTrajectory);
   }

   cacheCount.reserve(CACHE_COUNT_LENGTH);

   /*
    * Create the foot-bots and get references to their controllers
    */

    /* */
   for(int i=0; i<NUM_FOOTBOTS; i++){
		m_pcFootBots[i] = new CFootBotEntity(
      "fb"+std::to_string(i),    // entity id
      "fgets",    // controller id as set in the XML
      CVector3(),CQuaternion(), COMMUNICATION_RANGE// RAB range
      );
      AddEntity(*m_pcFootBots[i]);
      m_pcControllers[i] = &dynamic_cast<CFootBotGETaskSpecialisationController&>(m_pcFootBots[i]->GetControllableEntity().GetController());
	}

	m_originalMaxSpeed = m_pcControllers[0]->GetMaxSpeed();

	is_frozen = std::vector<bool>(NUM_FOOTBOTS,false);
	freeze_timer = std::vector<int>(NUM_FOOTBOTS,0);

	Reset();

}

void GETaskSpecialisationLoopFunctions::PreStep() {
	//argos::LOG<<"***Before the control step***"<<std::endl;
	//argos::LOG<<"***After the control step***"<<std::endl;
	//LOG<<"Start of next trial"<<std::endl;

	//LOG<<"Original Max: "<<m_originalMaxSpeed<<std::endl;

	for(int i=0; i<NUM_FOOTBOTS; i++){

		//LOG<<"On Slope: "<<m_pcControllers[i]->IsOnSlope()<<std::endl;

		if(m_pcControllers[i]->IsOnSlope()) {
			CRadians robotOrientation,y,z;
			m_pcFootBots[i]->GetEmbodiedEntity().GetOriginAnchor().Orientation.ToEulerAngles(robotOrientation,y,z);
			Real maxVelocity = CalculateMaxRobotSpeed(robotOrientation);
			m_pcControllers[i]->SetMaxSpeed(maxVelocity);
			//LOG<<"Max: "<<maxVelocity<<std::endl;
		}

		else{
			m_pcControllers[i]->SetMaxSpeed(FLAT_SPEED);
		}

		//LOG<<"Max speed "<<i<<" "<<m_pcControllers[i]->GetMaxSpeed()<<std::endl;

		/*
		CRadians robotOrientation,y,z;
		m_pcFootBots[i]->GetEmbodiedEntity().GetOriginAnchor().Orientation.ToEulerAngles(robotOrientation,y,z);
		LOG<<"Angle: "<<i<<" "<<robotOrientation<<std::endl;
		*/
		//LOG<<"Angle = 0: "<<CalculateMaxRobotSpeed(CRadians::ZERO)<<std::endl;
	}

	for(int i=0; i<m_pcCylinders.size(); i++){

		if(m_pcCylinders[i] != NULL && IsCylinderOnSlope(m_pcCylinders[i]->GetEmbodiedEntity())){//If cylinder is on slope
			//Get y coordinate of cylinder
			CVector2 cylinderPosition;
			m_pcCylinders[i]->GetEmbodiedEntity().GetOriginAnchor().Position.ProjectOntoXY(cylinderPosition);
			CVector2 newCylinderPosition;
			newCylinderPosition.SetX(cylinderPosition.GetX());
			//Update y coordinate according to SLIDING_SPEED
			float cylinderDisplacement = ARENA_LENGTH - SOURCE_LENGTH - cylinderPosition.GetY();

			//Version of cylinder sliding with friction
			//Speed = sqrt(2*a*s - Vi^2) = sqrt( 2 * (g*sin(theta) - Uf*g*cos(theta)) * s - 0)
			float slidingSpeed = sqrt(2.0f * (GRAVITY*Sin(m_slopeAngle) - COEFFICIENT_OF_FRICTION*GRAVITY*Cos(m_slopeAngle)) * cylinderDisplacement);
			if(std::isnan(slidingSpeed))
			    slidingSpeed = 0.0f;
			//std::cout<<"Sliding: "<<slidingSpeed<<std::endl;

			//Version of cylinder sliding without friction
			//Speed = sqrt(2*a*s - Vi^2) = sqrt( 2 * (g*sin(theta)) * s - 0)
			//float slidingSpeed = sqrt(2.0f * (GRAVITY*Sin(m_slopeAngle)) * cylinderDisplacement);

			newCylinderPosition.SetY(cylinderPosition.GetY() - slidingSpeed/10.0f);
			/*
			std::cout<<"Old position: "<<cylinderPosition.GetY()<<std::endl;
			std::cout<<"New position: "<<newCylinderPosition.GetY()<<std::endl;
			std::cout<<"Sliding speed: "<<slidingSpeed<<std::endl;
			std::cout<<"Fg: "<<GRAVITY*Sin(m_slopeAngle)<<std::endl;
			std::cout<<"Ff: "<<COEFFICIENT_OF_FRICTION*GRAVITY*Cos(m_slopeAngle)<<std::endl;
			std::cout<<"d: "<<(ARENA_LENGTH - cylinderPosition.GetY())<<std::endl;
			*/

			//Move entity to new coordinate
			SlideCylinder(m_pcCylinders[i]->GetEmbodiedEntity(),newCylinderPosition);
		}

	}
}

void GETaskSpecialisationLoopFunctions::PostStep() {

    /* */
	for(int i=0; i<NUM_FOOTBOTS; i++){
		CVector2 robotPosition;
		m_pcFootBots[i]->GetEmbodiedEntity().GetOriginAnchor().Position.ProjectOntoXY(robotPosition);

		/* Let robot pick up any cylinder that is nearby, remove it from the environment and spawn a replacement */
		if(m_pcControllers[i]->CheckWantObject() && !m_pcControllers[i]->CheckHasObject() &&  !m_pcControllers[i]->CheckDropObject() && !is_frozen[i]){
			int currentCylindersSize = m_pcCylinders.size();
			int currentNumCylinders = 0;
			int numCylindersOnSource = 0;

			//Count number of cylinders in the arena and number of cylinders on the source
			for(int k=0; k<m_pcCylinders.size(); k++){
				if(m_pcCylinders[k] != NULL) {
                    CVector2 cylinderPosition;
                    m_pcCylinders[k]->GetEmbodiedEntity().GetOriginAnchor().Position.ProjectOntoXY(cylinderPosition);

				    currentNumCylinders++;
					if(IsCylinderOnSource(m_pcCylinders[k]->GetEmbodiedEntity())){
						numCylindersOnSource++;
					}
				}
			}

			//Remove cylinder from the environment, give it to the robot and spawn a new cylinder
			for(int j=0; j<m_pcCylinders.size(); j++){

				if(m_pcCylinders[j] == NULL)
					continue;

				CVector2 cylinderPosition;
				m_pcCylinders[j]->GetEmbodiedEntity().GetOriginAnchor().Position.ProjectOntoXY(cylinderPosition);

				if(Abs(Distance(robotPosition, cylinderPosition)) < CYLINDER_PICKUP_DISTANCE && !m_pcControllers[i]->CheckHasObject()){ //if nearby

					if(IsCylinderOnSource(m_pcCylinders[j]->GetEmbodiedEntity()))
						numCylindersOnSource--;
					RemoveEntity(*m_pcCylinders[j]);//remove cylinder
					m_pcCylinders[j] = NULL;
					currentNumCylinders--;

					m_pcControllers[i]->SetHasObjectFlag(true);
					m_CylinderWithRobot[i] = j;

					m_pcCylinderPickupRecord[j][i]++;//Cylinder j picked up by robot i

					is_frozen[i] = true;

					//spawn new cylinder in environment and add to vector

					//if(currentNumCylinders >= m_pcCylinders.capacity())
					//	m_pcCylinders.reserve(currentNumCylinders*3);

					//if(currentNumCylinders < NUM_CYLINDERS) {
					if(numCylindersOnSource < NUM_CYLINDERS){
						m_pcCylinders.push_back(CreateCylinder(currentCylindersSize));
						m_pcCylinderPickupRecord.push_back(std::vector<int>(NUM_FOOTBOTS,0));
						AddEntity(*m_pcCylinders[currentCylindersSize]);
						SpawnNewCylinder(currentCylindersSize, m_pcCylinders[currentCylindersSize]->GetEmbodiedEntity());
						currentNumCylinders++;
						//argos::LOG<<"Cylinder picked up"<<std::endl;
						break;
					}

					//std::cout<<"Robot "<<i<<" has picked up cylinder "<<j<<std::endl;
				}

			}

		}

		/* Let robot drop cylinder. If cylinder is in nest area remove it from the environment, else make it reappear */
		if(m_pcControllers[i]->CheckHasObject() &&  m_pcControllers[i]->CheckDropObject() && !is_frozen[i]){
			//LOG<<"Cylinder Dropped"<<std::endl;
			CVector2 robotPosition;
			m_pcFootBots[i]->GetEmbodiedEntity().GetOriginAnchor().Position.ProjectOntoXY(robotPosition);

			CCylinderEntity* cylinder = CreateCylinder(m_CylinderWithRobot[i]);//Create new cylinder with old id
			m_pcCylinders[m_CylinderWithRobot[i]] = cylinder;//Put new cylinder in cylinder vector
			AddEntity(*cylinder);//Add entity to environment
			CEmbodiedEntity& cylinderEntity = cylinder->GetEmbodiedEntity();

			DropCylinder(cylinderEntity, robotPosition);//Place cylinder near robot
			is_frozen[i] = true;
			CVector3 cylinderPosition = cylinderEntity.GetOriginAnchor().Position;

			//Foot-bot no longer has cylinder
			m_pcControllers[i]->SetHasObjectFlag(false);
			int cylinderID = m_CylinderWithRobot[i];
			m_CylinderWithRobot[i] = -1;

			//std::cout<<"Robot "<<i<<" has dropped cylinder "<<cylinderID<<std::endl;


			if(IsCylinderOnNest(cylinderEntity)){//if cylinder is on nest
				//Log degree of task specialisation
				int numRobotsPickedUp = 0;
				for(int k=0; k<NUM_FOOTBOTS; k++)
					if(m_pcCylinderPickupRecord[cylinderID][k] > 0)
						numRobotsPickedUp++;
				if(numRobotsPickedUp > 1)
					numCylindersRetrievedWithTS++;
				numTotalCylindersRetrieved++;

				//Remove cylinder from environment and vector
				RemoveEntity(*cylinder);
				m_pcCylinders[cylinderID] = NULL;
				m_pcControllers[i]->IncrementCylindersGathered();
				//LOG<<"Cylinder delivered to nest"<<std::endl;
				//std::cout<<"Cylinder "<<cylinderID<<" removed from nest"<<std::endl;
			}
		}

		/* If robot has picked up a cylinder, freeze movement for set amount of time to simulate delay of a gripper */
		if( is_frozen[i] && (freeze_timer[i]<FREEZE_LIMIT) ) {
			m_pcControllers[i]->Freeze();
			freeze_timer[i]++;
			//std::cout<<"Robot #"<<i<<" is freezing"<<std::endl;
		}
		else{
			is_frozen[i] = false;
			freeze_timer[i] = 0;
		}
	}

	/* Log data */

	if(log_BC == true) {
		if(m_time_steps%5 == 0 && m_time_steps != 0){
			LogBehaviourCharacterisation();
		}
	}


	if(m_time_steps%10 == 0){
		if(log_trajectory == true)
			LogTrajectory();
		if(log_cache == true)
			LogCache();
	}

	/*
    for(int i=0; i<m_pcCylinders.size(); i++) {

        if (m_pcCylinders[i] != NULL) {
            //Get y coordinate of cylinder
            CVector2 cylinderPosition;
            m_pcCylinders[i]->GetEmbodiedEntity().GetOriginAnchor().Position.ProjectOntoXY(cylinderPosition);
            float y_coordinate = cylinderPosition.GetY();
        }
    }*/

	m_time_steps++;

	/*
	std::cout<<"Cylinder	|	Location"<<std::endl;
	for(int j=0; j<m_pcCylinders.size(); j++){
		//std::cout<<"|	"<<j<<"		|	"<<m_pcCylinders[j]<<"|"<<std::endl;

		if(m_pcCylinders[j] != NULL) {
			CVector2 cylinderCoordinates;
			m_pcCylinders[j]->GetEmbodiedEntity().GetOriginAnchor().Position.ProjectOntoXY(cylinderCoordinates);
			Real y_coordinate = cylinderCoordinates.GetY();
			std::cout<<"|	"<<j<<"		|	"<<y_coordinate<<"|"<<std::endl;
		}

		else{
			std::cout<<"|	"<<j<<"		|	"<<"NULL"<<"|"<<std::endl;
		}
	}*/
}

void GETaskSpecialisationLoopFunctions::PostExperiment() {

	/*
	for(int j=0; j<NUM_CYLINDERS; j++){
		argos::LOG<<"Cylinder "<<j<<" at trial "<<m_unCurrentTrial<<": "<<m_vecCylinderInitSetup[m_unCurrentTrial][j].Position<<std::endl;
	}
	*/

	//LOG<<GetTrial()<<std::endl;
	//LOG<<"Reached the end of the trial"<<std::endl;
	//LogBCFile();
	if(log_BC == true)
		LogBCFile();
	if(log_performance == true){
		LogPerformance();
		LogPerformanceFile();
		//std::cout<<"PERFORMANCE,"<<Performance()<<",PERFORMANCE"<<std::endl;
	}
	if(log_trajectory == true){
		LogTrajectoryFile();
	}
	if(log_cache == true){
		LogCacheFile();
	}
	if(log_degree_of_TS == true){
		LogDegreeOfTSFile();
	}

}

/****************************************/
/****************************************/

/* Generate the position and orientation for an entity (robot or cylinder) */
GETaskSpecialisationLoopFunctions::SInitSetup GETaskSpecialisationLoopFunctions::GenerateEntitySetup(std::string entityType) {

	float_t x_start, x_end, y_start, y_end;
	CRadians cOrient;

	x_start = ARENA_START_X;
	x_end = ARENA_START_X+ARENA_WIDTH;

	if(entityType == "ROBOT"){
		y_start = ARENA_START_Y;
		y_end = ARENA_START_Y+NEST_LENGTH;//+CACHE_LENGTH;

		cOrient = m_pcRNG->Uniform(CRadians::UNSIGNED_RANGE);
	}
	else if(entityType == "CYLINDER"){
		y_start = ARENA_START_Y+ARENA_LENGTH-SOURCE_LENGTH;
		y_end = ARENA_START_Y+ARENA_LENGTH;

		cOrient = CRadians::ZERO;
	}

	SInitSetup entitySetup;
	entitySetup.Position = CVector3(m_pcRNG->Uniform(CRange<Real>(x_start, x_end)),
												m_pcRNG->Uniform(CRange<Real>(y_start, y_end)),
												0.0);

	entitySetup.Orientation.FromEulerAngles(
         cOrient,        // rotation around Z
         CRadians::ZERO, // rotation around Y
         CRadians::ZERO  // rotation around X
         );

    return entitySetup;
}

void GETaskSpecialisationLoopFunctions::PlaceEntity(std::string entityType, CEmbodiedEntity& entityToPlace, int entityID) {
	/* Place entity in the arena. If placement is not possible, try a different random setup  */
   SInitSetup entitySetup;
   int numAttempts = 0;
   bool entityPlaced = false;

   while(numAttempts < ENTITY_PLACEMENT_ATTEMPTS && !entityPlaced) {
	   if(entityType == "ROBOT")
			entitySetup = m_vecRobotInitSetup[m_unCurrentTrial][entityID];
		else if(entityType == "CYLINDER")
			entitySetup = m_vecCylinderInitSetup[m_unCurrentTrial][entityID];

	   if(!MoveEntity(
			entityToPlace,
			entitySetup.Position,    // to this position
			entitySetup.Orientation, // with this orientation
			false
			)) {
				if(entityType == "ROBOT")
					m_vecRobotInitSetup[m_unCurrentTrial][entityID] = GenerateEntitySetup(entityType);
				else if(entityType == "CYLINDER"){
					m_vecCylinderInitSetup[m_unCurrentTrial][entityID] = GenerateEntitySetup(entityType);
					std::cout<<"Placing Cylinder "<<entityID<<std::endl;
				}
				numAttempts++;
			}
		else {
			entityPlaced = true;
			}
	}

	if(numAttempts >= ENTITY_PLACEMENT_ATTEMPTS){
		LOGERR << "Can't move entity in <"
					 << entitySetup.Position
					 << ">, <"
					 << entitySetup.Orientation
					 << ">"
					 << std::endl;
	}
}

CCylinderEntity* GETaskSpecialisationLoopFunctions::CreateCylinder(int cylinderID){

	return new CCylinderEntity("c"+std::to_string(cylinderID),	//Cylinder ID
						  CVector3(),	//Cylinder position
						  CQuaternion(),//Cylinder orientation
						  true,	//Cylinder is movable
						  CYLINDER_RADIUS, //Radius
						  CYLINDER_HEIGHT,	//Height
						  CYLINDER_MASS);	//Mass
}

GETaskSpecialisationLoopFunctions::SInitSetup GETaskSpecialisationLoopFunctions::GenerateCylinderSetupNearRobot(CVector2 robotPosition){
	SInitSetup entitySetup;
	entitySetup.Position = CVector3(m_pcRNG->Uniform(CRange<Real>(robotPosition.GetX() - 0.2f, robotPosition.GetX() + 0.2f)),
												m_pcRNG->Uniform(CRange<Real>(robotPosition.GetY() - 0.2f, robotPosition.GetY() + 0.2f)),
												0.0);

	entitySetup.Orientation.FromEulerAngles(
         CRadians::ZERO,        // rotation around Z
         CRadians::ZERO, // rotation around Y
         CRadians::ZERO  // rotation around X
         );

    return entitySetup;
}

CColor GETaskSpecialisationLoopFunctions::GetFloorColor(const CVector2& c_position_on_plane) {
   /* Nest*/
   if(c_position_on_plane.GetX() >= ARENA_START_X &&
		c_position_on_plane.GetX() < (ARENA_START_X+ARENA_WIDTH) &&
		c_position_on_plane.GetY() >= ARENA_START_Y &&
		c_position_on_plane.GetY() < ARENA_START_Y+NEST_LENGTH) {
      return NEST_COLOUR;
   }
   /* Cache */
   else if(c_position_on_plane.GetX() >= ARENA_START_X &&
		c_position_on_plane.GetX() < (ARENA_START_X+ARENA_WIDTH) &&
		c_position_on_plane.GetY() >= (ARENA_START_Y+NEST_LENGTH) &&
		c_position_on_plane.GetY() < (ARENA_START_Y+NEST_LENGTH+CACHE_LENGTH)) {
      return CACHE_COLOUR;
   }
   /* No man's land */
   else if(c_position_on_plane.GetX() >= ARENA_START_X &&
		c_position_on_plane.GetX() < (ARENA_START_X+ARENA_WIDTH) &&
		c_position_on_plane.GetY() >= (ARENA_START_Y+NEST_LENGTH+CACHE_LENGTH) &&
		c_position_on_plane.GetY() < (ARENA_LENGTH-SOURCE_LENGTH)) {
      return NO_MANS_COLOUR;
   }
   /* Source*/
   else if(c_position_on_plane.GetX() >= ARENA_START_X &&
		c_position_on_plane.GetX() < (ARENA_START_X+ARENA_WIDTH) &&
		c_position_on_plane.GetY() >= (ARENA_LENGTH-SOURCE_LENGTH) &&
		c_position_on_plane.GetY() < (ARENA_START_Y+ARENA_LENGTH)) {
      return SOURCE_COLOUR;
   }
   /* Rest of the environment*/
   else return OUTER_ARENA_COLOUR;
}

void GETaskSpecialisationLoopFunctions::Reset() {

	m_vecRobotInitSetup = std::vector<std::vector<SInitSetup>>(m_numTrials);
	for(int i=0; i<m_numTrials;i++)
		m_vecRobotInitSetup[i] = std::vector<SInitSetup>(NUM_FOOTBOTS);

	m_vecCylinderInitSetup = std::vector<std::vector<SInitSetup>>(m_numTrials);
	for(int i=0; i<m_numTrials;i++)
		m_vecCylinderInitSetup[i] = std::vector<SInitSetup>(NUM_CYLINDERS);

	m_pcCylinders.reserve(NUM_CYLINDERS*3);
	m_CylinderDropSpots.reserve(NUM_CYLINDERS*3);

	performanceLog.clear();
	performanceLog.reserve(m_numTrials);

   	swarmTrajectory.clear();
   	swarmTrajectory.reserve(NUM_FOOTBOTS);
   	std::vector<Real> robotTrajectory;
   	//robotTrajectory.assign(TRAJECTORY_LENGTH,-1);
   	robotTrajectory.reserve(TRAJECTORY_LENGTH);
   	for(int i=0; i<NUM_FOOTBOTS; i++){
   		swarmTrajectory.push_back(robotTrajectory);
   	}

   	cacheCount.clear();
   	cacheCount.reserve(CACHE_COUNT_LENGTH);

	//m_pcCylinderPickupRecord.clear();

	//swarmTrajectory.clear();
	//cacheCount.clear();

	//Robots are not holding any cylinders
	for(int i=0; i<NUM_FOOTBOTS; i++)
		m_CylinderWithRobot[i] = -1;

	//Remove all cylinders
	for(int i=0; i<m_pcCylinders.size(); i++){
		if(m_pcCylinders[i] != NULL) {
			RemoveEntity(*m_pcCylinders[i]);
			m_pcCylinders[i] = NULL;
			//std::cout<<"Cylinder "<<i<<" removed in reset"<<std::endl;
		}
	 }

	m_pcCylinders.resize(NUM_CYLINDERS);

	//Add original cylinders
	 for(int i=0; i<NUM_CYLINDERS; i++){
		m_pcCylinders[i] = CreateCylinder(i);
		AddEntity(*m_pcCylinders[i]);
	 }

	for(int i=0; i<m_numTrials; i++) {
		for(int j=0; j<NUM_FOOTBOTS; j++)
			m_vecRobotInitSetup[i][j] = GenerateEntitySetup("ROBOT");

		for(int j=0; j<NUM_CYLINDERS; j++) {
			m_vecCylinderInitSetup[i][j] = GenerateEntitySetup("CYLINDER");
			//argos::LOG<<"Cylinder "<<j<<" at trial "<<i<<": "<<m_vecCylinderInitSetup[i][j].Position<<std::endl;
		}
	}

	/*
    * Move robots and cylinders to the initial position corresponding to the current trial
    */
	for(int i=0; i<NUM_FOOTBOTS; i++)
		PlaceEntity("ROBOT", m_pcFootBots[i]->GetEmbodiedEntity(), i);

	for(int i=0; i<NUM_CYLINDERS; i++)
		PlaceEntity("CYLINDER", m_pcCylinders[i]->GetEmbodiedEntity(), i);

	for(int i=0; i<m_numTrials; i++){
		for(int j=0; j<NUM_CYLINDERS; j++){
			//argos::LOG<<"Cylinder ["<<i<<"]["<<j<<"] init is: "<<m_vecCylinderInitSetup[i][j].Position<<std::endl;
		}
	}

	m_DropCount = 0;

	is_frozen = std::vector<bool>(NUM_FOOTBOTS,false);
	freeze_timer = std::vector<int>(NUM_FOOTBOTS,0);

}

/****************************************/
/****************************************/

int GETaskSpecialisationLoopFunctions::Performance() {
   /* The performance is the number of cylinders gathered by all robots during a trial */
   int numCylinders = 0;

   for(int i=0; i<NUM_FOOTBOTS; i++) {
		numCylinders += m_pcControllers[i]->GetCylindersGathered();
   }

   return numCylinders;
}

float GETaskSpecialisationLoopFunctions::DegreeOfTaskSpecialisation() {
	if(numTotalCylindersRetrieved > 0)
   		return (numCylindersRetrievedWithTS/numTotalCylindersRetrieved);
    else
   		return 0;
}

size_t GETaskSpecialisationLoopFunctions::GetNumTrials() {
	return m_numTrials;
}

UInt32 GETaskSpecialisationLoopFunctions::GetTrial() {
      return m_unCurrentTrial;
}

void GETaskSpecialisationLoopFunctions::SetLogging(bool loggingFlag) {
	if(loggingFlag == true) {
		log_performance = true;
		log_trajectory = true;
		log_cache = true;
	}
	else{
		log_performance = false;
		log_trajectory = false;
		log_cache = false;
	}

	log_degree_of_TS = false;//DEPRECATED
}

void GETaskSpecialisationLoopFunctions::AssignRules(std::string rulesString) {

	//std::cout<<"Loop Functions: Assigning Rules"<<std::endl;

	for(int i=0; i<NUM_FOOTBOTS; i++) {
		m_pcControllers[i]->SetRulesFromInput(rulesString);
	}
}

void GETaskSpecialisationLoopFunctions::SetNumTrials(int numTrials){
	m_numTrials = numTrials;
}

void GETaskSpecialisationLoopFunctions::SetSlopeAngle(float angle) {
	m_slopeAngle = CRadians(angle/CRadians::RADIANS_TO_DEGREES);
	//std::cout<<"Slope angle is "<<m_slopeAngle<<std::endl;
}

void GETaskSpecialisationLoopFunctions::SpawnNewCylinder(int cylinderID, CEmbodiedEntity& cylinder){
   SInitSetup entitySetup;
   int numAttempts = 0;
   bool entityPlaced = false;

   while(numAttempts < ENTITY_PLACEMENT_ATTEMPTS && !entityPlaced) {
	   entitySetup = GenerateEntitySetup("CYLINDER");
	   std::cout<<"Spawning Cylinder "<<cylinderID<<std::endl;
	   if(!MoveEntity(
			cylinder,
			entitySetup.Position,    // to this position
			entitySetup.Orientation, // with this orientation
			false
			)) {
				entitySetup = GenerateEntitySetup("CYLINDER");
				numAttempts++;
			}
		else {
			entityPlaced = true;
			}
	}

	if(numAttempts >= ENTITY_PLACEMENT_ATTEMPTS){
		LOGERR << "Can't move entity in <"
					 << entitySetup.Position
					 << ">, <"
					 << entitySetup.Orientation
					 << ">"
					 << std::endl;
	}
}

void GETaskSpecialisationLoopFunctions::DropCylinder(CEmbodiedEntity& cylinder, CVector2 robotPosition){
	SInitSetup entitySetup;
	int numAttempts = 0;
	bool entityPlaced = false;

	while(numAttempts < ENTITY_PLACEMENT_ATTEMPTS && !entityPlaced) {
	   entitySetup = GenerateCylinderSetupNearRobot(robotPosition);
	   std::cout<<"Droping a cylinder"<<std::endl;
	   if(!MoveEntity(
			cylinder,
			entitySetup.Position,    // to this position
			entitySetup.Orientation, // with this orientation
			false
			)) {
				entitySetup = GenerateCylinderSetupNearRobot(robotPosition);
				numAttempts++;
			}
		else {
			entityPlaced = true;
			}
	}

	if(numAttempts >= ENTITY_PLACEMENT_ATTEMPTS){
		LOGERR << "Can't move entity in <"
					 << entitySetup.Position
					 << ">, <"
					 << entitySetup.Orientation
					 << ">"
					 << std::endl;
	}
}

bool GETaskSpecialisationLoopFunctions::IsCylinderOnSource(CEmbodiedEntity& cylinder){
	CVector2 cylinderPosition;
	cylinder.GetOriginAnchor().Position.ProjectOntoXY(cylinderPosition);

	if(cylinderPosition.GetX() >= ARENA_START_X &&
		cylinderPosition.GetX() < (ARENA_START_X+ARENA_WIDTH) &&
		cylinderPosition.GetY() >= ARENA_LENGTH-SOURCE_LENGTH &&
		cylinderPosition.GetY() < ARENA_LENGTH) {
      return true;
    }

    else
		return false;
}

bool GETaskSpecialisationLoopFunctions::IsCylinderOnSlope(CEmbodiedEntity& cylinder){
	CVector2 cylinderPosition;
	cylinder.GetOriginAnchor().Position.ProjectOntoXY(cylinderPosition);

	if(cylinderPosition.GetX() >= ARENA_START_X &&
		cylinderPosition.GetX() < (ARENA_START_X+ARENA_WIDTH) &&
		cylinderPosition.GetY() >= ARENA_START_Y+NEST_LENGTH+CACHE_LENGTH &&
		cylinderPosition.GetY() < ARENA_LENGTH-SOURCE_LENGTH) {
      return true;
    }

    else
		return false;
}

bool GETaskSpecialisationLoopFunctions::IsCylinderOnCache(CEmbodiedEntity& cylinder){
	CVector2 cylinderPosition;
	cylinder.GetOriginAnchor().Position.ProjectOntoXY(cylinderPosition);

	if(cylinderPosition.GetX() >= ARENA_START_X &&
		cylinderPosition.GetX() < (ARENA_START_X+ARENA_WIDTH) &&
		cylinderPosition.GetY() >= ARENA_START_Y+NEST_LENGTH &&
		cylinderPosition.GetY() < ARENA_START_Y+NEST_LENGTH+CACHE_LENGTH) {
      return true;
    }

    else
		return false;
}

bool GETaskSpecialisationLoopFunctions::IsCylinderOnNest(CEmbodiedEntity& cylinder){
	CVector2 cylinderPosition;
	cylinder.GetOriginAnchor().Position.ProjectOntoXY(cylinderPosition);

	if(cylinderPosition.GetX() >= ARENA_START_X &&
		cylinderPosition.GetX() < (ARENA_START_X+ARENA_WIDTH) &&
		cylinderPosition.GetY() >= ARENA_START_Y &&
		cylinderPosition.GetY() < ARENA_START_Y+NEST_LENGTH) {
      return true;
    }

    else
		return false;
}

Real GETaskSpecialisationLoopFunctions::CalculateMaxRobotSpeed(CRadians robotOrientation){
	float fgNormal = ROBOT_MASS * GRAVITY * Cos(m_slopeAngle);
	float fFriction = COEFFICIENT_OF_FRICTION * fgNormal;
	float work = ROBOT_MASS * GRAVITY;
	float power = fFriction*FLAT_SPEED;

	CRadians adjustmentAngle(90.0f/CRadians::RADIANS_TO_DEGREES);
	float maxVelocity = power / (fFriction + work*Sin(m_slopeAngle)*Cos(robotOrientation-adjustmentAngle));

	return maxVelocity;
}

void GETaskSpecialisationLoopFunctions::SlideCylinder(CEmbodiedEntity& cylinder, CVector2 newPosition){
	std::cout<<"Cylinder is sliding"<<std::endl;
	SInitSetup entitySetup;
	int numAttempts = 0;
	bool entityPlaced = false;
	CVector3 position = CVector3();
	position.SetX(newPosition.GetX());
	position.SetY(newPosition.GetY());
	position.SetZ(0.0);
	CQuaternion orientation = CQuaternion(CRadians::ZERO, CVector3());
	entitySetup.Position = position;
	entitySetup.Orientation = orientation;

	while(numAttempts < ENTITY_PLACEMENT_ATTEMPTS && !entityPlaced) {
	   if(!MoveEntity(
			cylinder,
			entitySetup.Position,    // to this position
			entitySetup.Orientation, // with this orientation
			false
			)) {
				numAttempts++;
				newPosition.SetY(newPosition.GetY()-CYLINDER_RADIUS);
				entitySetup = GenerateCylinderSetupNearCylinder(newPosition);
			}
		else {
			entityPlaced = true;
			}
	}

	if(numAttempts >= ENTITY_PLACEMENT_ATTEMPTS){
		LOGERR << "Can't move entity in <"
					 << position
					 << ">, <"
					 << orientation
					 << ">"
					 << std::endl;
	}
}

GETaskSpecialisationLoopFunctions::SInitSetup GETaskSpecialisationLoopFunctions::GenerateCylinderSetupNearCylinder(CVector2 cylinderPosition){
	SInitSetup entitySetup;
	entitySetup.Position = CVector3(m_pcRNG->Uniform(CRange<Real>(cylinderPosition.GetX() - 0.2f, cylinderPosition.GetX() + 0.2f)),
												m_pcRNG->Uniform(CRange<Real>(cylinderPosition.GetY() - 0.2f, cylinderPosition.GetY() + 0.2f)),
												0.0);

	entitySetup.Orientation.FromEulerAngles(
         CRadians::ZERO,        // rotation around Z
         CRadians::ZERO, // rotation around Y
         CRadians::ZERO  // rotation around X
         );

    return entitySetup;
}

void GETaskSpecialisationLoopFunctions::LogBehaviourCharacterisation(){
	for(int i=0; i<NUM_FOOTBOTS; i++){
		CVector2 robotPosition;
		//CRadians robotOrientation,y,z;
		m_pcFootBots[i]->GetEmbodiedEntity().GetOriginAnchor().Position.ProjectOntoXY(robotPosition);
		//m_pcFootBots[i]->GetEmbodiedEntity().GetOriginAnchor().Orientation.ToEulerAngles(robotOrientation,y,z);
		behaviourCharacterisation.push_back(robotPosition.GetX());
		behaviourCharacterisation.push_back(robotPosition.GetY());
		//behaviourCharacterisation.push_back(robotOrientation.GetValue());
	}
}

void GETaskSpecialisationLoopFunctions::LogBCFile(){
	std::ostringstream cOSS;
   cOSS << "BC_LOGS.csv";
   std::ofstream cOFS(cOSS.str().c_str(), std::ios::out | std::ios::trunc);
   /* Then dump the genome */
   for(int i=0; i<behaviourCharacterisation.size(); i++){
      cOFS << behaviourCharacterisation[i] <<",";
   }
   /* End line */
   cOFS << std::endl;
}

void GETaskSpecialisationLoopFunctions::LogPerformance(){
	performanceLog.push_back(Performance());
}

void GETaskSpecialisationLoopFunctions::LogPerformanceFile(){
	std::ostringstream cOSS;
   cOSS << "performance_logs.csv";
   std::ofstream cOFS(cOSS.str().c_str(), std::ios::out | std::ios::app);

   for(int i=0; i<performanceLog.size(); i++){
   		cOFS << performanceLog[i] <<std::endl;
   }
}

void GETaskSpecialisationLoopFunctions::LogCache(){
	int currentCacheCount = 0;
	for(int i=0; i<m_pcCylinders.size(); i++){
		if(m_pcCylinders[i] != NULL){
			if(IsCylinderOnCache(m_pcCylinders[i]->GetEmbodiedEntity())){
				currentCacheCount++;
			}
		}
	}
	cacheCount.push_back(currentCacheCount);
}

void GETaskSpecialisationLoopFunctions::LogCacheFile(){
	std::ostringstream cOSS;
   cOSS << "cache_logs.csv";
   std::ofstream cOFS(cOSS.str().c_str(), std::ios::out | std::ios::app);

   /*
   cOFS << "Time";

   for(int i=0; i<cacheCount.size(); i++){
   		cOFS << i <<",";
   }

   cOFS << std::endl;*/

   for(int i=0; i<cacheCount.size(); i++){
   		cOFS << cacheCount[i] <<",";
   }
   cOFS << std::endl;
}

void GETaskSpecialisationLoopFunctions::LogTrajectory(){
	for(int i=0; i<NUM_FOOTBOTS; i++){
		CVector2 robotPosition;
		//CRadians robotOrientation,y,z;
		m_pcFootBots[i]->GetEmbodiedEntity().GetOriginAnchor().Position.ProjectOntoXY(robotPosition);
		swarmTrajectory[i].push_back(robotPosition.GetY());
	}
}

void GETaskSpecialisationLoopFunctions::LogTrajectoryFile(){
	std::ostringstream cOSS;
   cOSS << "trajectory_logs.csv";
   std::ofstream cOFS(cOSS.str().c_str(), std::ios::out | std::ios::app);
   /*
   for(int i=0; i<swarmTrajectory[0].size(); i++){
   	cOFS << i*10 <<",";
   }
   cOFS << std::endl;*/
   for(int i=0; i<swarmTrajectory.size(); i++){
   	for(int j=0; j<swarmTrajectory[0].size(); j++){
      cOFS << swarmTrajectory[i][j] <<",";
   	}
   	cOFS << std::endl;
   }
}

void GETaskSpecialisationLoopFunctions::LogDegreeOfTSFile(){
	std::ostringstream cOSS;
   cOSS << "degree_of_TS_logs.csv";
   std::ofstream cOFS(cOSS.str().c_str(), std::ios::out | std::ios::trunc);
   if(numTotalCylindersRetrieved > 0)
   	cOFS << numCylindersRetrievedWithTS/numTotalCylindersRetrieved << std::endl;
   else
   	cOFS << 0 << std::endl;
}

/****************************************/
/****************************************/

REGISTER_LOOP_FUNCTIONS(GETaskSpecialisationLoopFunctions, "ge_task_specialisation_loop_functions")
