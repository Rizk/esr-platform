#ifndef GE_TASK_SPECIALISATION_LOOP_FUNCTIONS_H
#define GE_TASK_SPECIALISATION_LOOP_FUNCTIONS_H

/* ARGoS-related headers */
#include <argos3/core/simulator/loop_functions.h>
#include <argos3/core/utility/math/rng.h>
#include <argos3/plugins/robots/foot-bot/simulator/footbot_entity.h>
#include <argos3/plugins/simulator/entities/cylinder_entity.h>

#include <argos3/core/simulator/entity/floor_entity.h>

#include <controllers/footbot_ge_task_specialisation/footbot_ge_task_specialisation.h>
/****************************************/
/****************************************/


static const size_t NUM_FOOTBOTS = 4;
static const size_t NUM_CYLINDERS = 5;
//static const size_t NUM_TRIALS = 3;
static const size_t ENTITY_PLACEMENT_ATTEMPTS = 100;
static const float_t COMMUNICATION_RANGE = 1.0f;
static const size_t SIMULATION_LENGTH = 50000;

static const float_t CYLINDER_RADIUS = 0.1f;
static const float_t CYLINDER_HEIGHT = 0.2f;
static const float_t CYLINDER_MASS = 0.1f;
static const float_t FOOTBOT_DIAMETER = 0.17f;
static const float_t CYLINDER_PICKUP_DISTANCE = FOOTBOT_DIAMETER + CYLINDER_RADIUS;//Same as foot-bot diameter

static const float_t ARENA_START_X = 4.125f;
static const float_t ARENA_START_Y = 0.0f;
static const float_t ARENA_WIDTH = 1.75f;
static const float_t ARENA_LENGTH = 9.75f;

static const float_t NEST_LENGTH = 0.5f;
static const float_t CACHE_LENGTH = 2.5f;
static const float_t SOURCE_LENGTH = 2.0f;

static const CColor NEST_COLOUR = CColor::WHITE;
static const CColor CACHE_COLOUR = CColor::GRAY50;
static const CColor NO_MANS_COLOUR = CColor::RED;
static const CColor SOURCE_COLOUR = CColor::BLACK;
static const CColor OUTER_ARENA_COLOUR = CColor::GRAY90;

/* Constants for calculating maximum robot speed */
static const float_t ROBOT_MASS = 1.6; //According to mass in ARGoS code found in argos3/src/plugins/robots/foot-bot/simulator/dynamics2d_footbot_model.cpp
static const float_t GRAVITY = 9.81;
static const float_t FLAT_SPEED = 20; //As opposed to 0.15 from the paper
//static const float_t COEFFICIENT_OF_FRICTION = 0.5; //According to coefficient of dynamic friction in ARGoS code found in argos3/src/plugins/simulator/physics_engines/physx/physx_engine.cpp
static const float_t COEFFICIENT_OF_FRICTION = 0.0195;//1; //Removes friction

/* Constants for freezing robots after picking up a cylinder */
static const size_t FREEZE_LIMIT = 20;//Number of time steps robot freezes after picking up a cylinder


//DEPRECATED
//Variables for teleporting cylinder
//static const float_t SLIDING_SPEED = 0.1f;

/****************************************/
/****************************************/

using namespace argos;

class GETaskSpecialisationLoopFunctions : public CLoopFunctions {

public:

   GETaskSpecialisationLoopFunctions();
   virtual ~GETaskSpecialisationLoopFunctions();

   virtual void Init(TConfigurationNode& t_node);
   virtual void PreStep();
   virtual void PostStep();
   virtual void PostExperiment();
   virtual CColor GetFloorColor(const CVector2& c_position_on_plane);
   
   virtual void Reset();

   /* Returns the performance of the robots in the current trial */
   int Performance();

   /* Returns the degree of task specialisation */
   float DegreeOfTaskSpecialisation();   
   
   size_t GetNumTrials();
   
   inline void SetTrial(size_t un_trial) {
      m_unCurrentTrial = un_trial;
   }
   
   UInt32 GetTrial();

   void SetLogging(bool loggingFlag);
   
   void AssignRules(std::string rulesString);

   void SetNumTrials(int numTrials);

   /* Set the angle of the slope. Argument is in degrees */
   void SetSlopeAngle(float angle);
   
   void SpawnNewCylinder(int cylinderID, CEmbodiedEntity& cylinder);
   
   void DropCylinder(CEmbodiedEntity& cylinder, CVector2 robotPosition);

   argos::Real CalculateMaxRobotSpeed(CRadians orientation);

   bool IsCylinderOnSource(CEmbodiedEntity& cylinder);

   bool IsCylinderOnSlope(CEmbodiedEntity& cylinder);

   bool IsCylinderOnCache(CEmbodiedEntity& cylinder);

   bool IsCylinderOnNest(CEmbodiedEntity& cylinder);

   void SlideCylinder(CEmbodiedEntity& cylinder, CVector2 newPosition);

   void LogBehaviourCharacterisation();

   void LogBCFile();

   void LogPerformance();

   void LogPerformanceFile();

   void LogTrajectory();

   void LogTrajectoryFile();

   void LogCache();

   void LogCacheFile();

   void LogDegreeOfTSFile();

private:

   /* The initial setup of a trial */
   struct SInitSetup {
      CVector3 Position;
      CQuaternion Orientation;
   };

   SInitSetup GenerateEntitySetup(std::string entityType);
   void PlaceEntity(std::string entityType, CEmbodiedEntity& entityToPlace, int entityID);
   CCylinderEntity* CreateCylinder(int cylinderID);
   SInitSetup GenerateCylinderSetupNearRobot(CVector2 robotPosition);
   SInitSetup GenerateCylinderSetupNearCylinder(CVector2 cylinderPosition);
   
   std::vector<std::vector<SInitSetup>> m_vecRobotInitSetup;
   std::vector<std::vector<SInitSetup>> m_vecCylinderInitSetup;
   std::vector<CFootBotEntity*> m_pcFootBots;
   std::vector<CCylinderEntity*> m_pcCylinders;
   std::vector<std::vector<int>> m_pcCylinderPickupRecord;
   std::vector<CFootBotGETaskSpecialisationController*> m_pcControllers;
   Real* m_pfControllerParams;
   CRandom::CRNG* m_pcRNG;
   std::vector<int> m_CylinderWithRobot;//ID of cylinder each robot is carrying. -1 if none
   std::vector<CVector3> m_CylinderDropSpots;
   int m_DropCount;
   
   UInt32 m_unCurrentTrial;

   Real m_originalMaxSpeed;

   /* Robot freezing variables */
   std::vector<bool> is_frozen;
   std::vector<int> freeze_timer;

   int m_numTrials;

   /* Slope angle */
   CRadians m_slopeAngle;

   std::vector<Real> behaviourCharacterisation;
   int BCV_LENGTH = 3*NUM_FOOTBOTS*SIMULATION_LENGTH/5;//12000;//x,y,theta * 4 robots * 1000 time intervals (every 5th time step)
   int m_time_steps;

   std::vector<Real> performanceLog;

   std::vector<std::vector<Real>> swarmTrajectory;
   int TRAJECTORY_LENGTH = SIMULATION_LENGTH/10;

   std::vector<int> cacheCount;
   int CACHE_COUNT_LENGTH = SIMULATION_LENGTH/10;

   Real numCylindersRetrievedWithTS = 0;
   Real numTotalCylindersRetrieved = 0;

   bool log_BC = false;
   bool log_performance = false;
   bool log_trajectory = false;
   bool log_cache = false;
   bool log_degree_of_TS = false;
   
   //std::vector<Real> m_distanceTravelledByRobot;

};

#endif
