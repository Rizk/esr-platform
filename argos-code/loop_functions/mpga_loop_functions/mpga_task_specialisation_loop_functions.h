#ifndef MPGA_TASK_SPECIALISATION_LOOP_FUNCTIONS_H
#define MPGA_TASK_SPECIALISATION_LOOP_FUNCTIONS_H

/* The NN controller */
#include <controllers/footbot_nn/footbot_nn_controller.h>

/* ARGoS-related headers */
#include <argos3/core/utility/math/rng.h>
#include <argos3/plugins/robots/foot-bot/simulator/footbot_entity.h>
#include <argos3/plugins/simulator/entities/cylinder_entity.h>
#include <argos3/core/simulator/entity/floor_entity.h>

#include <loop_functions/mpga_loop_functions/mpga_loop_functions.h>

/****************************************/
/****************************************/

static const size_t NUM_FOOTBOTS = 4;
static const size_t NUM_CYLINDERS = 5;
static const size_t NUM_TRIALS = 5;
static const size_t ENTITY_PLACEMENT_ATTEMPTS = 100;
static const float_t COMMUNICATION_RANGE = 1.0f;

static const float_t CYLINDER_RADIUS = 0.1f;
static const float_t CYLINDER_HEIGHT = 0.2f;
static const float_t CYLINDER_MASS = 0.1f;
static const float_t FOOTBOT_DIAMETER = 0.17f;
static const float_t CYLINDER_PICKUP_DISTANCE = FOOTBOT_DIAMETER + CYLINDER_RADIUS;//Same as foot-bot diameter

static const float_t ARENA_START_X = 4.125f;
static const float_t ARENA_START_Y = 0.0f;
static const float_t ARENA_WIDTH = 1.75f;
static const float_t ARENA_LENGTH = 9.75f;

static const float_t NEST_LENGTH = 0.5f;
static const float_t CACHE_LENGTH = 2.5f;
static const float_t SOURCE_LENGTH = 2.0f;

static const CColor NEST_COLOUR = CColor::WHITE;
static const CColor CACHE_COLOUR = CColor::GRAY50;
static const CColor NO_MANS_COLOUR = CColor::RED;
static const CColor SOURCE_COLOUR = CColor::BLACK;
static const CColor OUTER_ARENA_COLOUR = CColor::GRAY90;

//Variables for calculating maximum robot speed
static const float_t ROBOT_MASS = 1.6; //From footbot model in ARGoS code
static const float_t GRAVITY = 9.81;
static const float_t FLAT_SPEED = 20; //As opposed to 0.15 from the paper
static const float_t COEFFICIENT_OF_FRICTION = 0.7; //According to footbot model in ARGoS code
static const CRadians SLOPE_ANGLE(8.0f/CRadians::RADIANS_TO_DEGREES); //In degrees

//Variables for teleporting cylinder
static const float_t SLIDING_SPEED = 0.1f;

/****************************************/
/****************************************/

/*
 * The size of the genome.
 * 
 * The genome is the set of NN weights. The NN is a simple
 * 2-layer perceptron. The inputs are 24 proximity readings,
 * 24 light readings, 4 ground readings and 5 aggregated RAB inputs. The outputs are 2 wheel speeds. The total
 * number of weights is therefore:
 *
 * W = (I + 1) * O = (24 + 24 + 4 + 5 + 1) * 3 = 174
 *
 * where:
 *   W = number of weights
 *   I = number of inputs
 *   O = number of outputs
 */
static const size_t GENOME_SIZE = 174;

/****************************************/
/****************************************/

using namespace argos;

class CMPGATaskSpecialisationLoopFunctions : public CMPGALoopFunctions {

public:

   CMPGATaskSpecialisationLoopFunctions();
   virtual ~CMPGATaskSpecialisationLoopFunctions();

   virtual void Init(TConfigurationNode& t_node);
   virtual void Reset();

   /* Configures the robot controller from the genome */
   virtual void ConfigureFromGenome(const Real* pf_genome);

   /* Calculates the performance of the robot in a trial */
   virtual Real Score();
   
   

   virtual void PreStep();
   virtual void PostStep();
   virtual void PostExperiment();
   virtual CColor GetFloorColor(const CVector2& c_position_on_plane);
   
   void SpawnNewCylinder(int cylinderID, CEmbodiedEntity& cylinder);
   
   void DropCylinder(CEmbodiedEntity& cylinder, CVector2 robotPosition);
   
   bool IsCylinderOnNest(CEmbodiedEntity& cylinder);
   
   argos::Real CalculateMaxRobotSpeed(CRadians orientation);

   bool IsCylinderOnSlope(CEmbodiedEntity& cylinder);

   bool IsRobotOnSlope(CEmbodiedEntity& robot);

   void SlideCylinder(CEmbodiedEntity& cylinder, CVector2 newPosition);

   bool IsCylinderOnSource(CEmbodiedEntity& cylinder);

   void LogBehaviourCharacterisation();

   void LogBCFile();

private:

   /* The initial setup of a trial */
   struct SInitSetup {
      CVector3 Position;
      CQuaternion Orientation;
   };

   /*
   std::vector<SInitSetup> m_vecInitSetup;
   CFootBotEntity* m_pcFootBot;
   CFootBotNNController* m_pcController;
   Real* m_pfControllerParams;
   CRandom::CRNG* m_pcRNG;
   */
   
   
   SInitSetup GenerateEntitySetup(std::string entityType);
   void PlaceEntity(std::string entityType, CEmbodiedEntity& entityToPlace, int entityID);
   CCylinderEntity* CreateCylinder(int cylinderID);
   SInitSetup GenerateCylinderSetupNearRobot(CVector2 robotPosition);
   SInitSetup GenerateCylinderSetupNearCylinder(CVector2 cylinderPosition);
   
   std::vector<std::vector<SInitSetup>> m_vecRobotInitSetup;
   std::vector<std::vector<SInitSetup>> m_vecCylinderInitSetup;
   std::vector<CFootBotEntity*> m_pcFootBots;
   std::vector<CCylinderEntity*> m_pcCylinders;
   std::vector<CFootBotNNController*> m_pcControllers;
   Real* m_pfControllerParams;
   CRandom::CRNG* m_pcRNG;
   std::vector<int> m_CylinderWithRobot;//ID of cylinder each robot is carrying. -1 if none
   std::vector<CVector3> m_CylinderDropSpots;
   int m_DropCount;
   
   //UInt32 m_unCurrentTrial;
   
   Real m_originalMaxSpeed;

   std::vector<Real> behaviourCharacterisation;
   int BCV_LENGTH = 12000;//x,y,theta * 4 robots * 1000 time intervals (every 5th time step)
   int m_time_steps;


};

#endif
