#include <../embedding/mpga/main_foraging.cpp>

//Tests fitness flushing with values bestscore and average score ranging 1-101
//Output should be of the form:
//best_fitness  avg_fitness
//      1           1
//      2           2
//      3           3
//      .           .
//      .           .
//      .           .
//      101         101
//      File name will be results/BEST_LOGS_300/foraging_stats.tsv
void FlushFitnessTest(){
  argos::LOG<<"Testing"<<std::endl;
  CMPGA::SIndividual individual;
  UInt32 generation = 0;
  Real averageScore;
  bool lastRun = false;
  char* seed = "300";

  for(int i=0; i<100; i++){
    individual.Score = i;
    averageScore = i;
    FlushFitness(individual,generation,averageScore,lastRun,seed);
  }

  individual.Score = 101;
  averageScore = 101;
  lastRun = true;
  FlushFitness(individual,generation,averageScore,lastRun,seed);
}

int main() {
	FlushFitnessTest();
}
