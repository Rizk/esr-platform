import numpy as np
from os import path, getcwd, pardir, chdir
import subprocess

def flush_individual_test():
	##Run command: build/embedding/mpga/mpga_foraging 3 0.05 3 10 202
	'''
	originalDir = getcwd()
	chdir('../../argos-code/')
	process = subprocess.Popen(['build/embedding/mpga/mpga_foraging 3 0.05 3 1 202'], stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
	output = process.communicate()
	output_string = str(output[0])
	error_string = str(output[1])
	'''
	
	#Read comparison file and convert to list of list of floats
	f = open('gen_output.txt', 'r')
	population = [0]
	population= f.read().strip().split('\n')
	for i in range(len(population)):
		population[i] = population[i].split(' ')
		for j in range(len(population[i])):
			population[i][j] = float(population[i][j]) 
	
	#Get best and average fitnesses (according to template)
	best_fitness = []
	avg_fitness = []
	for i in range(len(population)):
		best_fitness += [population[i][0]]
		avg_fitness += [np.mean(population[i])]
	
	#Read output from running code and compare to the sample output file
	g = open('/home/mostafa/Code/esr-benchmarks/argos-code/results/BEST_LOGS_202/foraging_stats.tsv','r')
	fitnesses = g.read().strip().split('\n')[1:]
	best_fitness_code = []
	avg_fitness_code = []
	for i in range(len(fitnesses)):
		best_fitness_code += [float(fitnesses[i].split('\t')[0])]
		avg_fitness_code += [float(fitnesses[i].split('\t')[1])]
	
	print(best_fitness)
	print(best_fitness_code)
	print(avg_fitness)
	print(avg_fitness_code)
	assert best_fitness == best_fitness_code, "Best fitnesses don't match"
	assert avg_fitness == avg_fitness_code, "Average fitnesses don't match"

flush_individual_test()
