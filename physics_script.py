#This script allows the user to set the parameters of the slope and generates
#the variables that should be used in the simulation

import math

#Known values
ROBOT_MASS = 1.6 #From footbot model in ARGoS code
GRAVITY = 9.8 #Unless the robots are in outer space
FLAT_SPEED = 0.15 #From the paper

#Variable parameters
COEFFICIENT_OF_FRICTION = 0.7 #According to footbot model in ARGoS code
SLOPE_ANGLE = math.radians(8) #In degrees

#Derived values
f_g_normal = ROBOT_MASS * GRAVITY * math.cos(SLOPE_ANGLE)
f_friction = COEFFICIENT_OF_FRICTION * f_g_normal
work = ROBOT_MASS * GRAVITY

#Power
power = f_friction*FLAT_SPEED #kilowatts (?)

def calculate_max_velocity(robot_orientation):
	robot_orientation = math.radians(robot_orientation)
	max_velocity = power / (f_friction + work*math.sin(SLOPE_ANGLE)*math.cos(robot_orientation))
	return max_velocity

"""
print(calculate_max_velocity(0))
print(calculate_max_velocity(45))
print(calculate_max_velocity(90))
print(calculate_max_velocity(180))
"""

#Up
vel = power / (f_friction + work*math.sin(SLOPE_ANGLE))
print("Up: " + str(vel))

#Down
vel = power / (f_friction - work*math.sin(SLOPE_ANGLE))
print("Down: " + str(vel))


print("0: " + str(calculate_max_velocity(0)))
print("45: " + str(calculate_max_velocity(45)))
print("90: " + str(calculate_max_velocity(90)))
print("180: " + str(calculate_max_velocity(180)))
print("210: " + str(calculate_max_velocity(210)))
print("270: " + str(calculate_max_velocity(270)))
print("315: " + str(calculate_max_velocity(315)))